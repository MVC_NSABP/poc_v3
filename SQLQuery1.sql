﻿USE [POC_V2]
GO

DECLARE	@return_value Int

EXEC	@return_value = [dbo].[sp_GetAddressDetails]
		@AddressId = 1

SELECT	@return_value as 'Return Value'

GO
