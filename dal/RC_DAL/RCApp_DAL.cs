﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Validation;
using dal.RC_BO;
using System.Data.Entity;

namespace dal.RC_DAL
{
    public class RCApp_DAL
    {
        public Boolean SaveSiteAddendumDetails(RCAppAddendumSave obj, bool isresubmit)
        {
            Boolean _result = false;
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        if (isresubmit)
                        {
                                                     
                            obj._T006_Research_Site.Resubmited = true;
                            entity.T006_Research_Site.Attach(obj._T006_Research_Site);
                            entity.Entry(obj._T006_Research_Site).Property(r => r.Resubmited).IsModified = true;
                            entity.Entry(obj._T006_Research_Site).Property(r => r.ModifiedBy).IsModified = true;
                            entity.Entry(obj._T006_Research_Site).Property(r => r.ModifiedOn).IsModified = true;
                            entity.SaveChanges();
                            transaction.Commit();
                            _result = true;
                            return true;
                        }
                        obj._T006_Research_Site.Application_Site_Status = 1;

                        obj._T006_Research_Site.Record_status = "Active";
                        entity.T006_Research_Site.Add(obj._T006_Research_Site);
                        entity.SaveChanges();
                        var id = obj._T006_Research_Site.ID;

                        T012_Map_RCSite_RegularActivities objT012;
                        if (obj.lst_T206_RC_RegularActivities != null)
                            foreach (var item in obj.lst_T206_RC_RegularActivities)
                            {
                                objT012 = new T012_Map_RCSite_RegularActivities();
                                objT012.T006_RCSiteID = id;
                                objT012.T206_RegularActivityID = item.ID;
                                objT012.CreatedBy = obj._T006_Research_Site.CreatedBy;
                                objT012.CreatedOn = DateTime.Now.Date;
                                objT012.ModifiedBy = obj._T006_Research_Site.CreatedBy;
                                objT012.ModifiedOn = DateTime.Now.Date;
                                objT012.Record_status = "Active";
                                if (objT012.T206_RegularActivityID == 5)
                                {
                                    objT012.RegularActivity_Comment = item.RegularActivity_Comment;
                                }
                                entity.T012_Map_RCSite_RegularActivities.Add(objT012);
                                entity.SaveChanges();
                            }
                        T014_Map_RCSite_DataManagement objT014;
                        if (obj.lst_T204_RC_DataManagementActivities != null)
                            foreach (var item in obj.lst_T204_RC_DataManagementActivities)
                            {
                                objT014 = new T014_Map_RCSite_DataManagement();
                                objT014.T006_RCSiteID = id;
                                objT014.T204_DataManagementActivityID = item.ID;
                                objT014.CreatedBy = obj._T006_Research_Site.CreatedBy;
                                objT014.CreatedOn = DateTime.Now.Date;
                                objT014.ModifiedBy = obj._T006_Research_Site.CreatedBy;
                                objT014.ModifiedOn = DateTime.Now.Date;
                                objT014.Record_status = "Active";
                                entity.T014_Map_RCSite_DataManagement.Add(objT014);
                                entity.SaveChanges();
                            }

                        T013_Map_RCSite_StudyParticipant objT013;
                        if (obj.lst_T203_RC_StudyPacrticipantActivities != null)
                            foreach (var item in obj.lst_T203_RC_StudyPacrticipantActivities)
                            {
                                objT013 = new T013_Map_RCSite_StudyParticipant();
                                objT013.T006_RCSiteID = id;
                                objT013.T203_StudyParticipantActivityID = item.ID;
                                objT013.CreatedBy = obj._T006_Research_Site.CreatedBy;
                                objT013.CreatedOn = DateTime.Now.Date;
                                objT013.ModifiedBy = obj._T006_Research_Site.CreatedBy;
                                objT013.ModifiedOn = DateTime.Now.Date;
                                objT013.Record_status = "Active";
                                entity.T013_Map_RCSite_StudyParticipant.Add(objT013);
                                entity.SaveChanges();
                            }

                        T015_Map_RCSite_Pharmacy objT015;
                        if (obj.lst_T205_RC_PharmacyActivities != null)
                            foreach (var item in obj.lst_T205_RC_PharmacyActivities)
                            {
                                objT015 = new T015_Map_RCSite_Pharmacy();
                                objT015.T006_RCSiteID = id;
                                objT015.T205_PharmacyActivityID = item.ID;
                                objT015.CreatedBy = obj._T006_Research_Site.CreatedBy;
                                objT015.CreatedOn = DateTime.Now.Date;
                                objT015.ModifiedBy = obj._T006_Research_Site.CreatedBy;
                                objT015.ModifiedOn = DateTime.Now.Date;
                                objT015.Record_status = "Active";
                                entity.T015_Map_RCSite_Pharmacy.Add(objT015);
                                entity.SaveChanges();
                            }
                        obj._T019_Research_Site_History.T006_Research_Site_ID = id;
                        obj._T019_Research_Site_History.StartDate = DateTime.Today.Date;
                        obj._T019_Research_Site_History.Comments = "Creation";

                        entity.T019_Research_Site_History.Add(obj._T019_Research_Site_History);
                        entity.SaveChanges();

                        transaction.Commit();
                        _result = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _result = false;

                    }
                }
            }

            return _result;
        }

        public int SaveRcApp(RCAppSave obj)
        {
            int _result = 0;
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        if (obj._T001_Research_Collaborator.IsAffirmation == true)
                        {
                            obj._T001_Research_Collaborator.SID = GetNxtRCID();
                        }
                        entity.T001_Research_Collaborator.Add(obj._T001_Research_Collaborator);
                        entity.SaveChanges();
                        var id = obj._T001_Research_Collaborator.ID;

                        T005_Map_RC_RegularActivities objT005;
                        if (obj.lst_T206_RC_RegularActivities != null)
                            foreach (var item in obj.lst_T206_RC_RegularActivities)
                            {
                                objT005 = new T005_Map_RC_RegularActivities();
                                objT005.T001_RCID = id;
                                objT005.T206_RegularActivityID = item.ID;
                                objT005.CreatedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT005.CreatedOn = DateTime.Now.Date;
                                objT005.ModifiedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT005.ModifiedOn = DateTime.Now.Date;
                                objT005.Record_status = "Active";
                                entity.T005_Map_RC_RegularActivities.Add(objT005);
                                entity.SaveChanges();
                            }
                        T003_Map_RC_DataManagement objT003;
                        if (obj.lst_T204_RC_DataManagementActivities != null)
                            foreach (var item in obj.lst_T204_RC_DataManagementActivities)
                            {
                                objT003 = new T003_Map_RC_DataManagement();
                                objT003.T001_RCID = id;
                                objT003.T204_DataManagementActivityID = item.ID;
                                objT003.CreatedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT003.CreatedOn = DateTime.Now.Date;
                                objT003.ModifiedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT003.ModifiedOn = DateTime.Now.Date;
                                objT003.Record_status = "Active";
                                entity.T003_Map_RC_DataManagement.Add(objT003);
                                entity.SaveChanges();
                            }

                        T004_Map_RC_StudyParticipant objT004;
                        if (obj.lst_T203_RC_StudyPacrticipantActivities != null)
                            foreach (var item in obj.lst_T203_RC_StudyPacrticipantActivities)
                            {
                                objT004 = new T004_Map_RC_StudyParticipant();
                                objT004.T001_RCID = id;
                                objT004.T203_StudyParticipantActivityID = item.ID;
                                objT004.CreatedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT004.CreatedOn = DateTime.Now.Date;
                                objT004.ModifiedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT004.ModifiedOn = DateTime.Now.Date;
                                objT004.Record_status = "Active";
                                entity.T004_Map_RC_StudyParticipant.Add(objT004);
                                entity.SaveChanges();
                            }

                        T002_Map_RC_Pharmacy objT002;
                        if (obj.lst_T205_RC_PharmacyActivities != null)
                            foreach (var item in obj.lst_T205_RC_PharmacyActivities)
                            {
                                objT002 = new T002_Map_RC_Pharmacy();
                                objT002.T001_RCID = id;
                                objT002.T205_PharmacyActivityID = item.ID;
                                objT002.CreatedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT002.CreatedOn = DateTime.Now.Date;
                                objT002.ModifiedBy = obj._T001_Research_Collaborator.CreatedBy;
                                objT002.ModifiedOn = DateTime.Now.Date;
                                objT002.Record_status = "Active";
                                entity.T002_Map_RC_Pharmacy.Add(objT002);
                                entity.SaveChanges();
                            }
                        transaction.Commit();
                        _result = id;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _result = 0;

                    }
                }
            }

            return _result;
        }
        public Boolean UpdateRCApp(RCAppSave obj)
        {
            Boolean _result = false;
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        entity.T001_Research_Collaborator.Attach(obj._T001_Research_Collaborator);
                        entity.Entry(obj._T001_Research_Collaborator).State = EntityState.Modified;
                        entity.SaveChanges();
                        transaction.Commit();
                        _result = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }
            return _result;
        }
        public List<sp_GetRCDetails_Result> getRCDetails()
        {
            using (var db = new POC_V2Entities())
            {
                List<sp_GetRCDetails_Result> sp_GetRCDetails_Result = new List<sp_GetRCDetails_Result>();
                try
                {
                    var result = db.sp_GetRCDetails();
                    sp_GetRCDetails_Result = result.ToList();
                }
                catch (Exception ex)
                {
                }
                return sp_GetRCDetails_Result;
            }
        }

        public RCAppSave ViewRCDetails(int RCID)
        {
            RCAppSave obj = new RCAppSave();

            using (var db = new POC_V2Entities())
            {
                try
                {
                    obj._T001_Research_Collaborator = (from T001 in db.T001_Research_Collaborator
                                                       where T001.Record_status == "Active" && T001.ID == RCID
                                                       select T001).FirstOrDefault();
                    obj.lst_T206_RC_RegularActivities = (from T005 in db.T005_Map_RC_RegularActivities
                                                         where T005.Record_status == "Active" && T005.T001_RCID == RCID
                                                         orderby T005.ID ascending
                                                         select T005).ToList();

                    obj.lst_T204_RC_DataManagementActivities = (from T005 in db.T003_Map_RC_DataManagement
                                                                where T005.Record_status == "Active" && T005.T001_RCID == RCID
                                                                orderby T005.ID ascending
                                                                select T005).ToList();


                    obj.lst_T203_RC_StudyPacrticipantActivities = (from T005 in db.T004_Map_RC_StudyParticipant
                                                                   where T005.Record_status == "Active" && T005.T001_RCID == RCID
                                                                   orderby T005.ID ascending
                                                                   select T005).ToList();

                    obj.lst_T205_RC_PharmacyActivities = (from T005 in db.T002_Map_RC_Pharmacy
                                                          where T005.Record_status == "Active" && T005.T001_RCID == RCID
                                                          orderby T005.ID ascending
                                                          select T005).ToList();


                }
                catch (Exception ex)
                {
                    throw;
                }
                return obj;
            }
        }

        public List<T206_RC_RegularActivities> GetRegularActivities()
        {
            using (var entity = new POC_V2Entities())
            {
                var ord = 0;
                List<T206_RC_RegularActivities> ListRC_Reg = new List<T206_RC_RegularActivities>();
                try
                {
                    var res = (from T206 in entity.T206_RC_RegularActivities
                               where T206.Record_status == "Active"
                               orderby ord ascending, T206.ID ascending
                               select new
                               {
                                   T206.ID,
                                   T206.Name,
                                   ord = T206.ID == 4 ? 1 : 0
                               }
                             ).ToList();

                    foreach (var item in res)
                    {
                        T206_RC_RegularActivities ob = new T206_RC_RegularActivities();
                        ob.Name = item.Name;
                        ob.ID = item.ID;
                        ListRC_Reg.Add(ob);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ListRC_Reg;
            }
        }
        public List<T203_RC_StudyPacrticipantActivities> GetStudyActivities()
        {
            using (var entity = new POC_V2Entities())
            {

                List<T203_RC_StudyPacrticipantActivities> ListStudyPart = new List<T203_RC_StudyPacrticipantActivities>();
                try
                {
                    var res = (from TM203 in entity.T203_RC_StudyPacrticipantActivities
                               where TM203.Record_status == "Active"
                               orderby TM203.ID ascending
                               select TM203);
                    ListStudyPart = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ListStudyPart;
            }


        }
        public List<T204_RC_DataManagementActivities> GetDataManagementActivities()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T204_RC_DataManagementActivities> ListRC_Data = new List<T204_RC_DataManagementActivities>();
                try
                {
                    var res = (from TM204 in entity.T204_RC_DataManagementActivities
                               where TM204.Record_status == "Active"
                               orderby TM204.ID ascending
                               select TM204);
                    ListRC_Data = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ListRC_Data;
            }


        }
        public List<T205_RC_PharmacyActivities> GetPharmacyActivities()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T205_RC_PharmacyActivities> PharmacyActivities = new List<T205_RC_PharmacyActivities>();
                try
                {
                    var res = (from TM205 in entity.T205_RC_PharmacyActivities
                               where TM205.Record_status == "Active"
                               orderby TM205.ID ascending
                               select TM205);
                    PharmacyActivities = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return PharmacyActivities;
            }
        }
        public List<T201_RC_MedicalSpecialities> GetMedicalSpecialties()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T201_RC_MedicalSpecialities> PharmacyActivities = new List<T201_RC_MedicalSpecialities>();
                try
                {
                    var res = (from TM201 in entity.T201_RC_MedicalSpecialities
                               where TM201.Record_status == "Active"
                               orderby TM201.ID ascending
                               select TM201);
                    PharmacyActivities = res.ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return PharmacyActivities;
            }
        }

        public List<T209_Countries> getCountries()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T209_Countries> lstCountries = new List<T209_Countries>();
                try
                {
                    var res = (from TM002 in entity.T209_Countries
                               select new
                               {
                                   TM002.Country_Code,
                                   TM002.Country_Name
                               }
                             ).ToList();

                    foreach (var item in res)
                    {
                        T209_Countries ob = new T209_Countries();
                        ob.Country_Code = item.Country_Code.Trim();
                        ob.Country_Name = item.Country_Name.Trim();
                        lstCountries.Add(ob);
                    }
                }
                catch (Exception ex)
                {

                }
                return lstCountries;
            }
        }
        public List<T210_States> getStates(string Country_Code)
        {
            using (var entity = new POC_V2Entities())
            {
                List<T210_States> lstStates = new List<T210_States>();
                try
                {
                    var States = (from TM003 in entity.T210_States
                                  where TM003.T209__Country_Code.Equals(Country_Code)
                                  select new
                                  {
                                      TM003.State_Code,
                                      TM003.State_Name
                                  }
                             ).ToList();
                    foreach (var item in States)
                    {
                        T210_States ob = new T210_States();
                        ob.State_Code = item.State_Code.Trim();
                        ob.State_Name = item.State_Name.Trim();
                        lstStates.Add(ob);
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

                return lstStates;
            }
        }

        public string GetNxtRCID()
        {
            string rc_id = "";
            try
            {
                using (var entity = new POC_V2Entities())
                {

                    rc_id = entity.Database.SqlQuery<string>("select dbo.[getMaxSequentialID_RC]() as NextSID")
                            .FirstOrDefault();

                    //select dbo.[getMaxSequentialID_RC]() as NextSID
                }
            }
            catch (Exception ex)
            {


                throw;
            }
            return rc_id;
        }
        public Boolean IsRCIDExists(string rcID)
        {
            Boolean _result = false;
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    if (entity.T001_Research_Collaborator.Where(s => s.RCID.Contains(rcID) && s.Record_status == "Active").Count() > 0)
                        _result = true;
                    else
                        _result = false;
                }
            }
            catch (Exception ex)
            {
            }
            return _result;
        }
        public Boolean roaster_IsRCIDExists(string rcID)
        {
            Boolean _result = false;
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    if (entity.T007_Roaster_T001_Collaborator_Site.Where(s => s.Collaborator_Site_ID.Contains(rcID) && s.Record_status == "Active").Count() > 0)
                        _result = true;
                    else
                        _result = false;
                }
            }
            catch (Exception ex)
            {
            }
            return _result;
        }
        public Boolean IsRCSiteExists(string rcID)
        {
            Boolean _result = false;
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    if (entity.T006_Research_Site.Where(s => s.RCSiteID.Contains(rcID) && s.Record_status == "Active").Count() > 0)
                        _result = true;
                    else
                        _result = false;
                }
            }
            catch (Exception ex)
            {
            }
            return _result;
        }

        public List<T202_Suffix_TYPES> GetSuffix()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T202_Suffix_TYPES> objlst = new List<T202_Suffix_TYPES>();
                try
                {
                    var res = from objsuffix in entity.T202_Suffix_TYPES
                              where objsuffix.Record_Status == "Active"
                              orderby objsuffix.ID ascending
                              select objsuffix;
                    objlst = res.ToList();
                }
                catch (Exception ex)
                {
                }
                return objlst;
            }
        }

        public List<T207_RC_Degree> GetDegree()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T207_RC_Degree> objlst = new List<T207_RC_Degree>();
                try
                {
                    var res = from objDegree in entity.T207_RC_Degree
                              where objDegree.Record_status == "Active"
                              orderby objDegree.ID ascending
                              select objDegree;
                    objlst = res.ToList();
                }
                catch (Exception ex)
                {
                }
                return objlst;
            }
        }

        public List<sp_GetSiteAddendumDetails_RCID_Result> GetSiteAddendumDetails(string SID)
        {
            List<sp_GetSiteAddendumDetails_RCID_Result> obj = new List<sp_GetSiteAddendumDetails_RCID_Result>();
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    obj = entity.sp_GetSiteAddendumDetails_RCID(SID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return obj;
        }
        public List<sp_getApplicationHistoryStatus_Result> GetApplicationHistoryStatus(string SID)
        {
            List<sp_getApplicationHistoryStatus_Result> obj = new List<sp_getApplicationHistoryStatus_Result>();
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    obj = entity.sp_getApplicationHistoryStatus(SID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return obj;
        }
        public List<sp_GetSiteAddendumDetailsWithAssessment_Result> GetSiteAddendumDetailsWithAssessment(string SID)
        {
            List<sp_GetSiteAddendumDetailsWithAssessment_Result> obj = new List<sp_GetSiteAddendumDetailsWithAssessment_Result>();
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    obj = entity.sp_GetSiteAddendumDetailsWithAssessment(SID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return obj;
        }
        public List<T214_RC_Application_Statuses> getApplicationStatuses()
        {
            List<T214_RC_Application_Statuses> obj = new List<T214_RC_Application_Statuses>();
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    obj = (from t214 in entity.T214_RC_Application_Statuses where t214.Record_Status == "Active" select t214).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return obj;
        }
        public Boolean UpdateApplicationStatus(T001_Research_Collaborator t001, List<T006_Research_Site> t006_table)
        {
            
               Boolean _result = false;
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                    {
                        entity.T001_Research_Collaborator.Attach(t001);
                        entity.Entry(t001).Property(x => x.RC_Application_Status).IsModified = true;
                        entity.Entry(t001).Property(x => x.RC_Application_Status_Date).IsModified = true;
                        if (t001.RC_Application_Status == 1)
                        {
                            t001.RC_Application_Status_Date = DateTime.Now.Date;//set value
                        }
                        else if (t001.RC_Application_Status != 7)
                        {
                            entity.Entry(t001).Property(x => x.RC_Application_Status_Reason).IsModified = true;
                            if (t001.RC_Application_Status == 2)
                            {
                                entity.Entry(t001).Property(x => x.SectionsToBeUnlock).IsModified = true;
                            }
                            if (t001.RC_Application_Status == 3)
                            {
                                entity.Entry(t001).Property(x => x.assignOldNSABPSiteID).IsModified = true;
                                entity.Entry(t001).Property(x => x.RCID).IsModified = true;
                            }
                        }
                        entity.Entry(t001).Property(x => x.ModifiedBy).IsModified = true;
                        entity.Entry(t001).Property(x => x.ModifiedOn).IsModified = true;

                        entity.SaveChanges();


                        var lst_t016A = (from qtnr in entity.T016_Questionnaire
                                         join t011 in entity.T011_Site_Assesment on qtnr.HeaderID equals t011.ID
                                         where qtnr.Record_Status == "Active" && t011.SiteID == t001.SID && t011.T001_Research_Collabotator_ID == t001.ID
                                         && t011.Type == "Assesment"
                                         select qtnr).ToList();

                        var lst_t016_B = (from qtnr in entity.T016_Questionnaire
                                          join t011 in entity.T011_Site_Assesment on qtnr.HeaderID equals t011.ID
                                          where qtnr.Record_Status == "Active" && t011.SiteID == t001.SID && t011.T001_Research_Collabotator_ID == t001.ID

                                          select qtnr).ToList();

                        if (t001.RC_Application_Status == 2 && !t001.SectionsToBeUnlock.Contains("NoChange"))
                        {
                            foreach (var t016A in lst_t016A)
                            {
                                entity.Entry(t016A).Property(x => x.NSABPHeadQt_HasSiteMetCriteria).IsModified = true;
                                entity.Entry(t016A).Property(x => x.NSABPHeadQt_DtofApproval).IsModified = true;
                                entity.Entry(t016A).Property(x => x.NSABPHeadQt_AdminApprovalSign).IsModified = true;
                                entity.Entry(t016A).Property(x => x.Unsatisfactory_Comments).IsModified = true;
                                entity.Entry(t016A).Property(x => x.ModifiedBy).IsModified = true;
                                entity.Entry(t016A).Property(x => x.ModifiedOn).IsModified = true;
                                entity.Entry(t016A).Property(x => x.SQA_Form_Status).IsModified = true;
                                entity.Entry(t016A).Property(x => x.QCCompleteStatus).IsModified = true;



                                t016A.NSABPHeadQt_HasSiteMetCriteria = false;
                                t016A.NSABPHeadQt_DtofApproval = DateTime.Now.Date;
                                t016A.ModifiedOn = DateTime.Now.Date;
                                t016A.NSABPHeadQt_AdminApprovalSign = "system";
                                t016A.Unsatisfactory_Comments = "system:" + t001.SID + "status changed to Approved Pending";
                                t016A.QCCompleteStatus = false;
                                entity.T016_Questionnaire.Attach(t016A);

                            }
                        }
                        else if (t001.RC_Application_Status == 4)
                        {
                            foreach (var t016B in lst_t016_B)
                            {
                                entity.Entry(t016B).Property(x => x.NSABPHeadQt_HasSiteMetCriteria).IsModified = true;
                                entity.Entry(t016B).Property(x => x.NSABPHeadQt_DtofApproval).IsModified = true;
                                entity.Entry(t016B).Property(x => x.NSABPHeadQt_AdminApprovalSign).IsModified = true;
                                entity.Entry(t016B).Property(x => x.Unsatisfactory_Comments).IsModified = true;
                                entity.Entry(t016B).Property(x => x.ModifiedBy).IsModified = true;
                                entity.Entry(t016B).Property(x => x.ModifiedOn).IsModified = true;
                                entity.Entry(t016B).Property(x => x.SQA_Form_Status).IsModified = true;
                                entity.Entry(t016B).Property(x => x.QCCompleteStatus).IsModified = true;


                                t016B.NSABPHeadQt_HasSiteMetCriteria = false;
                                t016B.NSABPHeadQt_DtofApproval = DateTime.Now.Date;
                                t016B.ModifiedOn = DateTime.Now.Date;
                                t016B.NSABPHeadQt_AdminApprovalSign = "system";
                                t016B.Unsatisfactory_Comments = "system:" + t001.SID + "status changed to Rejected";
                                entity.T016_Questionnaire.Attach(t016B);

                            }
                        }
                        foreach (var t006item in t006_table)
                        {
                            entity.Entry(t006item).Property(x => x.Application_Site_Status_Date).IsModified = true;
                            entity.Entry(t006item).Property(x => x.Application_Site_Status).IsModified = true;
                            entity.Entry(t006item).Property(x => x.ModifiedBy).IsModified = true;
                            entity.Entry(t006item).Property(x => x.ModifiedOn).IsModified = true;

                            if (t006item.Application_Site_Status == 1)
                            {
                                t006item.Application_Site_Status_Date = DateTime.Now.Date;
                            }
                            else if (t006item.Application_Site_Status != 7)
                            {
                                entity.Entry(t006item).Property(x => x.Application_Site_Status_Reason).IsModified = true;
                                if (t006item.Application_Site_Status == 2)
                                {
                                    entity.Entry(t006item).Property(x => x.SectionsToBeUnlock).IsModified = true;
                                }
                                if (t006item.Application_Site_Status == 3)
                                {
                                    entity.Entry(t006item).Property(x => x.RCSiteID).IsModified = true;
                                }
                            }

                            entity.T006_Research_Site.Attach(t006item);

                            if (t006item.Application_Site_Status == 2 && t006item.SectionsToBeUnlock.Equals("NoChange"))
                            {                                
                                string sql = "update T11 set T11.NSABPHeadQt_HasSiteMetCriteria = '0',T11.NSABPHeadQt_DtofApproval = getDate()," +
" T11.NSABPHeadQt_AdminApprovalSign = 'System',T11.Unsatisfactory_Comments = 'System:" + t006item.SID + " status changed to Approved Pending'," +
                                            " T11.ModifiedOn = getdate(),T11.ModifiedBy = '',T11.SQA_Form_Status = 1," +
                                            " T11.QCCompleteStatus = 0 from [dbo].[T016_Questionnaire] T11" +
                                             " join[dbo].[T011_Site_Assesment] T6 on(T11.HeaderID= T6.ID and T6.Record_Status= 'Active')" +
                    " where T11.Record_Status='Active' and T6.SiteID='" + t006item.SID + "' and  T6.T001_Research_Collabotator_ID=" +
                                            "   '" + t006item.ID + "' and T6.[Type]='Assessment'; ";

                                int noOfRowUpdated = entity.Database.ExecuteSqlCommand(sql);
                            }
                            if (t006item.Application_Site_Status == 4)
                            {
                                string sqlA = "update T11 set T11.NSABPHeadQt_HasSiteMetCriteria = '0',T11.NSABPHeadQt_DtofApproval = getDate(),T11.NSABPHeadQt_AdminApprovalSign = 'System', " +
" T11.Unsatisfactory_Comments = 'System:" + t006item.SID + " status changed to Rejected',T11.ModifiedOn = getdate(), " +
" T11.ModifiedBy = '',T11.SQA_Form_Status = 4 from[dbo].[T016_Questionnaire] T11 join[dbo].[T011_Site_Assesment] " +
      "  T6 on(T11.HeaderID= T6.ID and T6.Record_Status= 'Active') where T11.Record_Status='Active' and T6.SiteID='" + t006item.SID + "'  " +

  " and T6.T001_Research_Collabotator_ID='" + t006item.ID + "' ";
                                int res = entity.Database.ExecuteSqlCommand(sqlA);
                            }
                        }


                        entity.SaveChanges();
                        transaction.Commit();
                        _result = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return _result;
        }



        public List<sp_GetAddressDetails_Result> lstGetAddresses()
        {
            using (var entity = new POC_V2Entities())
            {
                List<sp_GetAddressDetails_Result> lstAddress = new List<sp_GetAddressDetails_Result>();
                try
                {
                    //var res = (from T213_Address in entity.T213_Address where T213_Address.Record_status=="Active"
                    //           select T213_Address );
                    var res = entity.sp_GetAddressDetails(0);//for get alladdress details we need to pass zero
                    lstAddress = res.ToList();
                }
                catch (Exception ex)
                {

                }
                return lstAddress;
            }
        }
        public List<USP4_GetAddressesAssociationsByAddressId_Result> GetAddressMappingPersonSiteByAddressID(int AddressID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<USP4_GetAddressesAssociationsByAddressId_Result> lstobj = new List<USP4_GetAddressesAssociationsByAddressId_Result>();
                try
                {
                    var result = entity.USP4_GetAddressesAssociationsByAddressId(AddressID);

                    lstobj = result.ToList();


                }
                catch (Exception ex)
                {
                }
                return lstobj;
            }
        }
        public static T212_Personnel ISPersonnelNameExist(string Fname,string Lanme)
        {
            T212_Personnel res = new T212_Personnel();
            using (var db = new POC_V2Entities())
            {
                try
                {
                    res = db.T212_Personnel.Where(x => x.F_Name == Fname && x.L_Name == Lanme).FirstOrDefault();
                   
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return res;
        }

        public List<RC_RejectedApplications_Result> getRejectedApplications()
        {
            using (var entity = new POC_V2Entities())
            {
                List<RC_RejectedApplications_Result> lst = new List<RC_RejectedApplications_Result>();
                try
                {

                    lst = entity.RC_RejectedApplications().ToList();
                  
                }
                catch (Exception ex)
                {

                }
                return lst;
            }
        }

    }
}
