﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using System.Data.Entity;
namespace dal.RC_DAL
{
    public class HeaderDAL
    {
        public List<sp_GetStatesbyCountryID_Result> GetStates(string Countr_Code)
        {
            using (var entity = new POC_V2Entities())
            {
                List<sp_GetStatesbyCountryID_Result> listStates = new List<sp_GetStatesbyCountryID_Result>();
                try
                {
                    var result = entity.sp_GetStatesbyCountryID(Countr_Code);
                    listStates = result.ToList();
                }
                catch (Exception ex)
                {

                }
                return listStates;
            }
        }


        public Boolean SaveAddresses(T213_Address objAddress)
        {
            Boolean _result = false;
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    entity.T213_Address.Add(objAddress);
                    entity.SaveChanges();
                    _result = true;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return _result;
        }

        public Boolean isAddressExist(T213_Address obj)
        {
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    var res = entity.sp_IsAddressExist(obj.Address_Line1, obj.Address_Line2, obj.Country_ID, obj.State_ID, obj.City, obj.ZIP, obj.Attention, obj.Department, obj.InternalOffice).ToList();
                    if (res.Count > 0)
                    {
                        return true;
                    }
                    res.Clear();

                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool UpdateAddress(T213_Address obj)
        {
            try
            {
                using (var entity = new POC_V2Entities())
                {
                    var res = entity.T213_Address.SingleOrDefault(s => s.Address_ID == obj.Address_ID);
                    if (res != null)
                    {
                        entity.T213_Address.AddOrUpdate(obj); //requires using System.Data.Entity.Migrations;
                        entity.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        //assesments

        //int count = HeaderDAL.GetHeaderCount(header.SiteID);
        //header.HeaderID = "Assessment - " + (count + 1) + ": " + header.DateofCall.ToString("MM/dd/yyyy"); 
        public static List<sp_GetUserDetails_Result> GetUserDetails()
        {
            using (var entity = new POC_V2Entities())
            {
                List<sp_GetUserDetails_Result> listusers = new List<sp_GetUserDetails_Result>();
                try
                {
                    var res = entity.sp_GetUserDetails();
                    listusers = res.ToList();
                }
                catch (Exception ex)
                {

                }
                return listusers;
            }
        }

        public static bool IsAssessmentExists(T011_Site_Assesment header)
        {
            using (var entity = new POC_V2Entities())
            {
                try
                {

                    var res = (from Assessmet in entity.T011_Site_Assesment
                               where Assessmet.SiteID == header.SiteID && Assessmet.Date == header.Date && Assessmet.Person_conducting_Assesment_Name == header.Person_conducting_Assesment_Name && Assessmet.Record_status == "Active"
                               select Assessmet).Count();
                    return (res > 0);
                }
                catch (Exception ex)
                {
                }
                return false;
            }
        }

        public static DateTime getMaxHeaderDate_ForSelectedSite(string siteNumber)
        {
            using (var entity = new POC_V2Entities())
            {
                DateTime MaxDateForSite = new DateTime();
                try
                {
                    // var res = entity.sp
                }
                catch (Exception ex)
                {
                }
                return DateTime.Now;
            }
        }

        public static Boolean AddHeader(T011_Site_Assesment header, T020_Site_Address objadd)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        var count = entity.sp_GetHeaderCount(header.SiteID).FirstOrDefault();
                        header.HeaderID = "Assessment - " + (Convert.ToInt32(count) + 1) + ": " + header.Date.ToString("MM/dd/yyyy");
                        entity.T011_Site_Assesment.Add(header);
                        entity.SaveChanges();
                        objadd.Assessment_ID = header.ID;
                        entity.T020_Site_Address.Add(objadd);
                        entity.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)

                    {
                        transaction.Rollback();
                    }
                    return false;
                }
            }

        }
        public static sp_getApplicationOrSiteDetails_Result getApplicationOrSiteDetails(string site)
        {
            sp_getApplicationOrSiteDetails_Result obj = new sp_getApplicationOrSiteDetails_Result();
            using (var entity = new POC_V2Entities())
            {

                try
                {
                    obj = entity.sp_getApplicationOrSiteDetails(site).FirstOrDefault();
                   // Id = Convert.ToString(res.Select(a => a.ID).FirstOrDefault());

                }
                catch (Exception ex)
                {

                }
                return obj;
            }
        }
        
        public static List<String> getSiteNumbers()
        {
            using (var entity = new POC_V2Entities())
            {
                List<String> lst = new List<String>();

                try
                {
                    lst = (from obj in entity.T007_Roaster_T001_Collaborator_Site
                           where obj.Record_status == "Active"
                           orderby obj.Collaborator_Site_ID
                           select obj.Collaborator_Site_ID).ToList();


                }
                catch (Exception ex)
                {

                }
                return lst;
            }
        }

        public static List<sp_getAffirmationSiteDetails_Result> getAffirmationSiteDetails(string strSiteID)
        {
            List<sp_getAffirmationSiteDetails_Result> lstsite = new List<sp_getAffirmationSiteDetails_Result>();
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    lstsite = entity.sp_getAffirmationSiteDetails(strSiteID).ToList();


                }
                catch (Exception)
                {

                }
            }
            return lstsite;
        }
        #region HeaderDetails
        public static List<sp_GetAllHeadersList_Result> GetAllHeaderDetails(string userRole, int userID)
        {
            List<sp_GetAllHeadersList_Result> listHeaders = new List<sp_GetAllHeadersList_Result>();
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    listHeaders = entity.sp_GetAllHeadersList(userRole, userID).ToList();
                }
                catch (Exception ex)
                {
                }
                return listHeaders;
            }
        }
        public static sp_GetHeaderDetailsbyHeaderId_Result GetHeaderDetails(int Id)
        {
            using (var entity = new POC_V2Entities())
            {
                sp_GetHeaderDetailsbyHeaderId_Result objHeader = new sp_GetHeaderDetailsbyHeaderId_Result();
                try
                {
                    objHeader = entity.sp_GetHeaderDetailsbyHeaderId(Id).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }
                return objHeader;
            }
        }

        public static bool IsHeaderUser(int UserID)
        {
            using (var entity = new POC_V2Entities())
            {
                bool _isExists = false;
                try
                {
                    var res = entity.sp_IsHeaderUser(UserID);
                    if(res!=null)
                    {
                        if (res.Count()> 0)
                        {
                            _isExists = true;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                return _isExists;
            }
        }

        public static bool DeleteHeader(int headerID)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                         entity.T020_Site_Address.RemoveRange(entity.T020_Site_Address.Where(a => a.Assessment_ID == headerID));
                         entity.T011_Site_Assesment.RemoveRange(entity.T011_Site_Assesment.Where(a => a.ID == headerID));
                        //  Delete from T020_Site_Address where Assessment_ID = " + headerID + "
                        //Delete from T011_Site_Assesment where ID = " + headerID + "                     
                        entity.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public static bool DeleteAllHeaderDetails(int headerID)
        {

            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        entity.T016_Questionnaire.RemoveRange(entity.T016_Questionnaire.Where(a => a.HeaderID == headerID));
                        entity.T011_Site_Assesment.RemoveRange(entity.T011_Site_Assesment.Where(a => a.ID == headerID));
                                        
                        entity.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }
        }

        #endregion
        public static as_getApplicaonOrAddendumStatus_Result getApplicaonOrAddendumStatus(int id)
        {
            using (var entity = new POC_V2Entities())
            {
                as_getApplicaonOrAddendumStatus_Result res = new as_getApplicaonOrAddendumStatus_Result();
                try
                {
                    res = entity.as_getApplicaonOrAddendumStatus(id).FirstOrDefault();
                }
                catch (Exception ex)
                {

                }
                return res;
            }
        }
    }
}
