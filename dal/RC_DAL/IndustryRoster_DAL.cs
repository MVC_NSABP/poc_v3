﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Validation;
using System.Data.Entity;
using dal.RC_BO;

namespace dal.RC_DAL
{
    public class IndustryRoster_DAL
    {
        public static List<T007_Roaster_T001_Collaborator_Site> lst_Roaster_T001_Collaborator_Site()
        {

            using (var db = new POC_V2Entities())
            {
                List<T007_Roaster_T001_Collaborator_Site> lst_T007 = new List<T007_Roaster_T001_Collaborator_Site>();
                try
                {
                    lst_T007 = (from t007 in db.T007_Roaster_T001_Collaborator_Site
                                where
                            t007.Record_status == "Active"
                                select t007
                             ).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_T007;
            }
        }
        public static List<sp_getPersonnelByNetworkID_Result> lst_getPersonnelByNetworkID_Result(string NetworkID)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_getPersonnelByNetworkID_Result> lst_res = new List<sp_getPersonnelByNetworkID_Result>();
                try
                {
                    lst_res = db.sp_getPersonnelByNetworkID(NetworkID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<T022_Roaster_Roles> T022_Roaster_Roles()
        {

            using (var db = new POC_V2Entities())
            {
                List<T022_Roaster_Roles> lst_res = new List<T022_Roaster_Roles>();
                try
                {
                    lst_res = (from T022 in db.T022_Roaster_Roles where T022.Record_Status == "Active" select T022).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_PersonnelByNetwork_Result> sp_T212_Personnel()
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_PersonnelByNetwork_Result> lst_res = new List<sp_PersonnelByNetwork_Result>();
                try
                {
                    lst_res = db.sp_PersonnelByNetwork().ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<T213_Address> lst_Address()
        {

            using (var db = new POC_V2Entities())
            {
                List<T213_Address> lst_res = new List<T213_Address>();
                try
                {
                    lst_res = (from T212 in db.T213_Address select T212).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_GetAddressDetails_Result> GetAddressDetailsByAddressID(int AddresID)
        {
            using (var db = new POC_V2Entities())
            {
                List<sp_GetAddressDetails_Result> lst_res = new List<sp_GetAddressDetails_Result>();
                try
                {
                    lst_res = db.sp_GetAddressDetails(AddresID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_GetAddressDetailsByNetworkId_Result> GetAddressDetailsByNetworkId(String NetworkID)
        {
            using (var db = new POC_V2Entities())
            {
                List<sp_GetAddressDetailsByNetworkId_Result> lst_res = new List<sp_GetAddressDetailsByNetworkId_Result>();
                try
                {
                    lst_res = db.sp_GetAddressDetailsByNetworkId(NetworkID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_getRoleHistoryBySiteID_Result> lst_getRoleHistoryBySiteID(int SiteId)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_getRoleHistoryBySiteID_Result> lst_res = new List<sp_getRoleHistoryBySiteID_Result>();
                try
                {
                    lst_res = db.sp_getRoleHistoryBySiteID(SiteId.ToString()).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_getNetworkDeailsByNetworkID_Result> lst_getNetworkDeailsByNetworkID(string NetworkID)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_getNetworkDeailsByNetworkID_Result> lst_res = new List<sp_getNetworkDeailsByNetworkID_Result>();
                try
                {
                    lst_res = db.sp_getNetworkDeailsByNetworkID(NetworkID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }

        public static List<sp_GetPersonnelToNetworkDetails_Result> GetPersonnelToNetworkDetails_Result(int NetworkID)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_GetPersonnelToNetworkDetails_Result> lst_res = new List<sp_GetPersonnelToNetworkDetails_Result>();
                try
                {
                    lst_res = db.sp_GetPersonnelToNetworkDetails(NetworkID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_GetIndustryPersonnelByNetwork_Result> GetIndustryPersonnelByNetwork(string NetworkID)
        {
            using (var db = new POC_V2Entities())
            {
                List<sp_GetIndustryPersonnelByNetwork_Result> lst_res = new List<sp_GetIndustryPersonnelByNetwork_Result>();
                try
                {
                    lst_res = db.sp_GetIndustryPersonnelByNetwork(NetworkID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }

        public static List<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result> GetAddendumDetailsFortreatingSiteByNetworkID(string NetworkID)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result> lst_res = new List<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result>();
                try
                {
                    lst_res = db.sp_GetAddendumDetailsFortreatingSiteByNetworkID(NetworkID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }

        public static List<sp_getContractsHistoryBySiteID_Result> getContractsHistoryBySiteID(int siteID)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_getContractsHistoryBySiteID_Result> lst_res = new List<sp_getContractsHistoryBySiteID_Result>();
                try
                {
                    lst_res = db.sp_getContractsHistoryBySiteID(siteID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static List<sp_getPayments_HistoryBySiteID_Result> getPayments_HistoryBySiteID(int siteID)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_getPayments_HistoryBySiteID_Result> lst_res = new List<sp_getPayments_HistoryBySiteID_Result>();
                try
                {
                    lst_res = db.sp_getPayments_HistoryBySiteID(siteID).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static Boolean USP5_Update_PrimaryRole(string network, Nullable<int> role, Nullable<int> person, Nullable<int> modifiedBy)
        {

            using (var db = new POC_V2Entities())
            {
                bool _bool = false;
                try
                {
                    var res = db.USP5_Update_PrimaryRole(network, role, person, modifiedBy);

                    _bool = true;
                }
                catch (Exception ex)
                {
                }
                return _bool;
            }
        }

        public static List<sp_CAAddressDetailsByNetworkID_Result> getCAAddressDetailsByNetworkID(string NetworkId)
        {

            using (var db = new POC_V2Entities())
            {
                List<sp_CAAddressDetailsByNetworkID_Result> lst_res = new List<sp_CAAddressDetailsByNetworkID_Result>();
                try
                {
                    lst_res = db.sp_CAAddressDetailsByNetworkID(NetworkId).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst_res;
            }
        }
        public static bool Update_Roaster_Collaborator_Legal_Details(T007_Roaster_T001_Collaborator_Site obj)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                try
                {
                    T007_Roaster_T001_Collaborator_Site t007 = db.T007_Roaster_T001_Collaborator_Site.FirstOrDefault(x => x.ID == obj.ID);
                    t007.LeagalName = obj.LeagalName;
                    if (obj.LeagalAddressID != 0 && obj.LeagalAddressID != null)
                        t007.LeagalAddressID = obj.LeagalAddressID;
                    if (obj.Affiliation != "" && obj.Affiliation != null)
                        t007.Affiliation = obj.Affiliation;
                    t007.ModifiedBy = obj.ModifiedBy;
                    t007.ModifiedOn = obj.ModifiedOn;

                    db.SaveChanges();
                    issaved = true;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                return issaved;
            }
        }

        public static bool Update_Roaster_Collaborator_Details(T007_Roaster_T001_Collaborator_Site obj)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                try
                {
                    T007_Roaster_T001_Collaborator_Site t007 = db.T007_Roaster_T001_Collaborator_Site.FirstOrDefault(x => x.ID == obj.ID);
                    t007.TaxIdentificationNo = obj.TaxIdentificationNo;
                    t007.CheckAddressee = obj.CheckAddressee;
                    t007.CAAddress = obj.CAAddress;
                    t007.TeachingHospital = obj.TeachingHospital;
                    t007.PayeeName = obj.PayeeName;
                    t007.ModifiedBy = obj.ModifiedBy;
                    t007.ModifiedOn = obj.ModifiedOn;
                    t007.TeachingHospital = obj.TeachingHospital;
                    t007.TeachingHospital = obj.TeachingHospital;
                    T023_Roaster_Map_Collaborator_Personnel t023 = (from T023 in db.T023_Roaster_Map_Collaborator_Personnel
                                                                    where T023.T022_Roaster_Roles_ID == 5 && T023.Record_Status == "Active"
&& T023.T007_Roaster_T001_Collaborator_Site_ID == obj.ID
                                                                    select T023).FirstOrDefault();
                    if (t023.T007_Roaster_T001_Collaborator_Site_ID == obj.ID)
                    {
                        t023.AddressID = obj.CAAddress;
                        t023.ModifiedBy = obj.ModifiedBy;
                        t023.ModifiedOn = obj.ModifiedOn;
                    }
                    else
                    {
                        t023.Record_Status = "Active";
                        t023.T022_Roaster_Roles_ID = 5;
                        t023.ModifiedBy = obj.ModifiedBy;
                        t023.ModifiedOn = obj.ModifiedOn;
                        t023.CreatedBy = obj.ModifiedBy;
                        t023.CreatedOn = obj.ModifiedOn;
                        t023.T007_Roaster_T001_Collaborator_Site_ID = obj.ID;
                        db.T023_Roaster_Map_Collaborator_Personnel.Add(t023);
                    }


                    db.SaveChanges();
                    issaved = true;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                return issaved;
            }
        }
        public static bool SaveAddendumDetailsForTreatingSite(T007_Roaster_T001_Collaborator_Site obj)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                try
                {
                    db.T007_Roaster_T001_Collaborator_Site.Add(obj);
                    db.SaveChanges();
                    issaved = true;
                }
                catch (DbEntityValidationException e)
                {
                }
                return issaved;
            }
        }

        public static bool UpdatePersonnelStatus(T023_Roaster_Map_Collaborator_Personnel obj)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                try
                {
                    T023_Roaster_Map_Collaborator_Personnel T023 = (from t023 in db.T023_Roaster_Map_Collaborator_Personnel
                                                                    where t023.T007_Roaster_T001_Collaborator_Site_ID == obj.T007_Roaster_T001_Collaborator_Site_ID
                                                                    && t023.Map_Collaborator_Personnel_ID == obj.Map_Collaborator_Personnel_ID
                                                                    select t023).SingleOrDefault();
                    T023.StatusID = obj.StatusID;
                    T023.StatusDate = obj.StatusDate;
                    T023.ModifiedBy = obj.ModifiedBy;
                    T023.ModifiedOn = obj.ModifiedOn;
                    db.SaveChanges();
                    issaved = true;
                }
                catch (DbEntityValidationException e)
                {
                }
                return issaved;
            }
        }


        public static IND_getSiteDetailsBySiteID_Result getSiteDetailsBySiteID(string SiteId)
        {
            using (var db = new POC_V2Entities())
            {
                IND_getSiteDetailsBySiteID_Result res = new IND_getSiteDetailsBySiteID_Result();
                try
                {
                    res = db.IND_getSiteDetailsBySiteID(SiteId).SingleOrDefault();
                }
                catch (Exception ex)
                {
                }
                return res;
            }
        }

        public static bool UpdateSiteDetails(T007_Roaster_T001_Collaborator_Site obj)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                try
                {
                    T007_Roaster_T001_Collaborator_Site T023 = (from T007 in db.T007_Roaster_T001_Collaborator_Site
                                                                where T007.Collaborator_Site_ID == obj.Collaborator_Site_ID
                                                                select T007).SingleOrDefault();
                    T023.Collaborator_Site_Name = obj.Collaborator_Site_Name;
                    T023.AddressID = obj.AddressID;
                    T023.ModifiedBy = obj.ModifiedBy;
                    T023.ModifiedOn = obj.ModifiedOn;
                    db.SaveChanges();
                    issaved = true;
                }
                catch (DbEntityValidationException e)
                {
                }
                return issaved;
            }
        }
        public static bool UpdateSitePharmacyDetails(T007_Roaster_T001_Collaborator_Site obj)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                try
                {
                    T007_Roaster_T001_Collaborator_Site T023 = (from T007 in db.T007_Roaster_T001_Collaborator_Site
                                                                where T007.Collaborator_Site_ID == obj.Collaborator_Site_ID
                                                                select T007).SingleOrDefault();
                    T023.Pharmacy_Address_ID = obj.Pharmacy_Address_ID;
                    T023.PharmacyType = obj.PharmacyType;
                    T023.AddressID = obj.AddressID;
                    T023.ModifiedBy = obj.ModifiedBy;
                    T023.ModifiedOn = obj.ModifiedOn;
                    db.SaveChanges();
                    issaved = true;
                }
                catch (DbEntityValidationException e)
                {
                }
                return issaved;
            }
        }

        public static List<IND_getSiteAssociations_Result> getSiteAssociations(string SiteId)
        {
            using (var db = new POC_V2Entities())
            {
                List<IND_getSiteAssociations_Result> res = new List<IND_getSiteAssociations_Result>();
                try
                {
                    res = db.IND_getSiteAssociations(SiteId).ToList();
                }
                catch (Exception ex)
                {
                }
                return res;
            }
        }

        public static IND_PersonnelDetailsByPersonnelID_Result PersonnelDetailsByPersonnelID(string Personnel_ID)
        {
            using (var db = new POC_V2Entities())
            {
                IND_PersonnelDetailsByPersonnelID_Result res = new IND_PersonnelDetailsByPersonnelID_Result();
                try
                {
                    res = db.IND_PersonnelDetailsByPersonnelID(Personnel_ID).SingleOrDefault();
                }
                catch (Exception ex)
                {
                }
                return res;
            }
        }
        public static List<T009_Map_Personnel_Degree> getPersonnelDegreesByT212_ID(int T212_ID)
        {
            using (var db = new POC_V2Entities())
            {
                List<T009_Map_Personnel_Degree> res = new List<T009_Map_Personnel_Degree>();
                try
                {
                    res = (from t009 in db.T009_Map_Personnel_Degree where t009.T212_Personnel_ID == T212_ID select t009).ToList();
                }
                catch (Exception ex)
                {
                }
                return res;
            }
        }
        public static bool Update_Roaster_PersonnelDetails(T212_Personnel obj, List<T009_Map_Personnel_Degree> lst_T009)
        {
            bool issaved = false;
            using (var db = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        T212_Personnel T212 = (from t212 in db.T212_Personnel
                                               where t212.ID == obj.ID
                                               select t212).SingleOrDefault();
                        T212.Salutation = obj.Salutation;
                        T212.F_Name = obj.F_Name;
                        T212.L_Name = obj.L_Name;
                        T212.M_Initials = obj.M_Initials;
                        T212.Phone = obj.Phone;
                        T212.Phone_Ext = obj.Phone_Ext;
                        //T212.Fax = obj.Fax;
                        T212.Email = obj.Email;
                        T212.T201_MedicalSpl_ID = obj.T201_MedicalSpl_ID;
                        T212.T202_Suffix_ID = obj.T202_Suffix_ID;
                        T212.AddressID = obj.AddressID;
                        T212.Personnel_StatusID = obj.Personnel_StatusID;
                        T212.StatusDate = obj.StatusDate;
                        T212.MODIFIED_BY = obj.MODIFIED_BY;
                        T212.MODIFIED_ON = obj.MODIFIED_ON;


                        db.T009_Map_Personnel_Degree.RemoveRange(db.T009_Map_Personnel_Degree.Where(x=>x.T212_Personnel_ID== obj.ID));

                        db.T009_Map_Personnel_Degree.AddRange(lst_T009);

                        db.SaveChanges();
                        issaved = true;
                        transaction.Commit();
                    }
                    catch (DbEntityValidationException e)
                    {
                        transaction.Rollback();
                    }
                    return issaved;
                }
            }
        }
        public static List<IND_GetAddressRolesBYPersonID_Result> GetAddressRolesBYPersonID(string Personnel_ID)
        {
            using (var db = new POC_V2Entities())
            {
                List<IND_GetAddressRolesBYPersonID_Result> res = new List<IND_GetAddressRolesBYPersonID_Result>();
                try
                {
                    res = db.IND_GetAddressRolesBYPersonID(Personnel_ID).ToList();
                }
                catch (Exception ex)
                {
                }
                return res;
            }
        }

        //rcrc && Change status
        public static bool UpdateSentToRCRC(int _id, string _SID, DateTime _DateOfRCSubmittedToRCBoard, int _ModifiedBy)
        {
            using (var db = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {

                    try
                    {
                        T001_Research_Collaborator res = (from t in db.T001_Research_Collaborator
                                                          where t.SID == _SID && t.ID == _id
                                                          select t).SingleOrDefault();
                        if (res != null)
                        {
                            res.RC_Application_Status = 7;
                            res.DateOfRCSubmittedToRCBoard = _DateOfRCSubmittedToRCBoard;
                            res.RC_Application_Status_Date = res.ModifiedOn = DateTime.Now;
                            res.ModifiedBy = _ModifiedBy;
                            res.RC_Application_Status_Reason = null;
                            res.Record_status = "Active";
                            res.ModifiedOn = DateTime.Now;
                        }

                        var _res = (from t in db.T006_Research_Site
                                    where t.RCID == _SID && (t.Resubmited == false || t.Resubmited == null)
                                    && (t.Application_Site_Status == 6 || (t.Application_Site_Status == 2 && t.SectionsToBeUnlock.Contains("NoChange")))
                                   && t.Record_status == "Active"
                                    select t
                                                 ).ToList();
                        _res.ForEach(a => {
                            a.Application_Site_Status = 7;
                            a.Application_Site_Status = 7;
                            a.Application_Site_Status_Date = DateTime.Now;
                            a.DateOfRCSubmittedToRCBoard = _DateOfRCSubmittedToRCBoard;
                            a.Application_Site_Status_Reason = null;
                            a.ModifiedBy = _ModifiedBy;
                            a.ModifiedOn = DateTime.Now;
                        });
                        db.SaveChanges();
                        transaction.Commit();
                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
                return false;
            }
        }


        public static bool UpdateApplicationStatus(IndustryRoasterBO objIndustryRoaster, List<IndustryRoasterBO> lstSiteReviewDetails)
        {
            bool _IsUpdated = false;
            using (var db = new POC_V2Entities())
            {

                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    List<string> lstQuery = new List<string>();
                    try
                    {
                        var today = DateTime.Now.Date;
                        string updateQuery = "";
                        updateQuery = "update T001_Research_Collaborator set RC_Application_Status='" + objIndustryRoaster.RC_Application_Status + "',";
                        if (objIndustryRoaster.RC_Application_Status == 1)
                        {
                            updateQuery = updateQuery + "RC_Application_Status_Date=getDate(),";
                        }
                        else if (objIndustryRoaster.RC_Application_Status != 7) //DateTime.ParseExact(objIndustryRoaster.RC_Application_Status_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        {
                            updateQuery = updateQuery + "RC_Application_Status_Date='" + objIndustryRoaster.RC_Application_Status_Date + "',";
                            updateQuery = updateQuery + "RC_Application_Status_Reason='" + objIndustryRoaster.RC_Application_Status_Reason + "',";
                            if (objIndustryRoaster.RC_Application_Status == 2)
                            {
                                updateQuery = updateQuery + "SectionsToBeUnlock='" + objIndustryRoaster.SectionsToBeUnlock + "',";
                            }
                            if (objIndustryRoaster.RC_Application_Status == 3)
                            {
                                updateQuery = updateQuery + "assignOldNSABPSiteID='" + objIndustryRoaster.assignOldNSABPSiteID + "',RCID='" + objIndustryRoaster.RCID + "',";
                            }
                        }

                        updateQuery = updateQuery + "ModifiedBy='" + objIndustryRoaster.ModifiedBy + "',ModifiedOn=getDate() where ID='" + objIndustryRoaster.ID + "' and Record_Status='Active'";
                        lstQuery.Add(updateQuery);
                        if (objIndustryRoaster.RC_Application_Status == 2 && !objIndustryRoaster.SectionsToBeUnlock.Contains("NoChange"))
                        {
                            updateQuery = "update T016 set T016.NSABPHeadQt_HasSiteMetCriteria='0',T016.NSABPHeadQt_DtofApproval=getDate(),T016.NSABPHeadQt_AdminApprovalSign='System',T016.Unsatisfactory_Comments='System:" + objIndustryRoaster.SID + " status changed to Approved Pending',T016.ModifiedOn=getDate(),T016.ModifiedBy='" + objIndustryRoaster.ModifiedBy + "',T016.SQA_Form_Status=1,T016.QCCompleteStatus=0 from T016_Questionnaire T016 join T011_Site_Assesment T011 on (T016.HeaderID=T011.ID and T011.Record_Status='Active') where T016.Record_Status='Active' and T011.SiteID='" + objIndustryRoaster.SID + "' and T011.T001_Research_Collabotator_ID='" + objIndustryRoaster.ID + "' and T011.[Type]='Assessment'";
                            lstQuery.Add(updateQuery);
                        }
                        else if (objIndustryRoaster.RC_Application_Status == 4)
                        {
                            updateQuery = "update T016 set T016.NSABPHeadQt_HasSiteMetCriteria='0',T016.NSABPHeadQt_DtofApproval=getDate(),T016.NSABPHeadQt_AdminApprovalSign='System',T016.Unsatisfactory_Comments='System:" + objIndustryRoaster.SID + " status changed to Rejected',T016.ModifiedOn=getDate(),T016.ModifiedBy='" + objIndustryRoaster.ModifiedBy + "',T016.SQA_Form_Status=4 from T016_Questionnaire T016 join T011_Site_Assesment T011 on (T016.HeaderID=T011.ID and T011.Record_Status='Active') where T016.Record_Status='Active' and T011.SiteID='" + objIndustryRoaster.SID + "' and T011.T001_Research_Collabotator_ID='" + objIndustryRoaster.ID + "'";
                            lstQuery.Add(updateQuery);
                        }
                        if (lstSiteReviewDetails != null)
                        {

                            foreach (var item in lstSiteReviewDetails)
                            {

                                updateQuery = "update T006_Research_Site set Application_Site_Status=" + item.Application_Site_Status + ",";
                                if (item.Application_Site_Status.ToString() == "1")
                                {
                                    updateQuery = updateQuery + "Application_Site_Status_Date=getDate(),";
                                }
                                else if (item.Application_Site_Status.ToString() != "7")
                                {
                                    //DateTime.ParseExact(this.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    updateQuery = updateQuery + "Application_Site_Status_Date='" + item.Application_Site_Status_Date + "',";
                                    updateQuery = updateQuery + "Application_Site_Status_Reason='" + item.Application_Site_Status_Reason + "',";
                                    if (item.Application_Site_Status.ToString() == "2")
                                    {
                                        updateQuery = updateQuery + "SectionsToBeUnlock='" + item.SectionsToBeUnlock.ToString() + "',";
                                    }
                                    if (item.Application_Site_Status.ToString() == "3")
                                    {
                                        updateQuery = updateQuery + "RCSiteID='" + item.RCSiteID.ToString() + "',";
                                    }
                                }
                                updateQuery = updateQuery + "ModifiedBy='" + objIndustryRoaster.ModifiedBy + "',ModifiedOn=getDate() where ID='" + item.ID.ToString() + "' and Record_Status='Active'";
                                lstQuery.Add(updateQuery);
                                if (item.Application_Site_Status.ToString() == "2" && !item.SectionsToBeUnlock.ToString().Contains("NoChange"))
                                {
                                    updateQuery = "update T016 set T016.NSABPHeadQt_HasSiteMetCriteria='0',T016.NSABPHeadQt_DtofApproval=getDate(),T016.NSABPHeadQt_AdminApprovalSign='System',T016.Unsatisfactory_Comments='System:" + item.SID.ToString() + " status changed to Approved Pending',T016.ModifiedOn=getDate(),T016.ModifiedBy='" + objIndustryRoaster.ModifiedBy + "',T016.SQA_Form_Status=1,T016.QCCompleteStatus=0 from T016_Questionnaire T016 join T011_Site_Assesment T011 on (T016.HeaderID=T011.ID and T011.Record_Status='Active') where T016.Record_Status='Active' and T011.SiteID='" + item.SID.ToString() + "' and T011.T001_Research_Collabotator_ID='" + item.ID.ToString() + "' and T011.[Type]='Assessment'";
                                    lstQuery.Add(updateQuery);
                                }
                                if (item.Application_Site_Status.ToString() == "4")
                                {
                                    updateQuery = "update T016 set T016.NSABPHeadQt_HasSiteMetCriteria='0',T016.NSABPHeadQt_DtofApproval=getDate(),T016.NSABPHeadQt_AdminApprovalSign='System',T016.Unsatisfactory_Comments='System:" + item.SID.ToString() + " status changed to Rejected',T016.ModifiedOn=getDate(),T016.ModifiedBy='" + objIndustryRoaster.ModifiedBy + "',T016.SQA_Form_Status=4 from T016_Questionnaire T016 join T011_Site_Assesment T011 on (T016.HeaderID=T011.ID and T011.Record_Status='Active') where T016.Record_Status='Active' and T011.SiteID='" + item.SID.ToString() + "' and T011.T001_Research_Collabotator_ID='" + item.ID.ToString() + "'";
                                    lstQuery.Add(updateQuery);
                                }
                            }
                        }
                        foreach (var query in lstQuery)
                        {
                            db.Database.ExecuteSqlCommand(query);
                        }
                        //  db.SaveChanges();
                        transaction.Commit();
                        _IsUpdated = true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                    }
                    return _IsUpdated;
                }
            }


        }
        public static List<sp_GetSiteAddendumDetailsWithAssessment_Result> GetSiteAddendumDetailsWithAssessment(string SID)
        {
            using (var db = new POC_V2Entities())
            {
                List<sp_GetSiteAddendumDetailsWithAssessment_Result> objlist = new List<sp_GetSiteAddendumDetailsWithAssessment_Result>();
                try
                {
                    objlist = db.sp_GetSiteAddendumDetailsWithAssessment(SID).ToList();

                }
                catch (Exception ex)
                {

                }
                return objlist;
            }
        }
       
        public static bool UpdateSentToRCRC_Addendum(int _id, string _SID, DateTime _DateOfRCSubmittedToRCBoard, int _ModifiedBy)
        {
            using (var db = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {

                    try
                    {
                        T006_Research_Site _res = (from s in db.T006_Research_Site where s.SID == _SID && s.ID == _id && s.Record_status == "Active" select s
                                                 ).SingleOrDefault();
                        _res.Application_Site_Status = 7;
                        _res.Application_Site_Status_Date = DateTime.Now;
                        _res.DateOfRCSubmittedToRCBoard = _DateOfRCSubmittedToRCBoard;
                        _res.Application_Site_Status_Reason = null;
                        _res.ModifiedBy = _ModifiedBy;
                        _res.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                        transaction.Commit();
                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                    return false;
                }
            }
        }
    }
}
