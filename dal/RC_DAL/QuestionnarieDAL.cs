﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using System.Data.Entity;
namespace dal.RC_DAL
{
    public class QuestionnarieDAL
    {

        public static List<getSequentialIDs_Application_Result> getSequentialIDs()
        {
            using (var entity = new POC_V2Entities())
            {
                List<getSequentialIDs_Application_Result> listSids = new List<getSequentialIDs_Application_Result>();
                try
                {
                    var result = entity.getSequentialIDs_Application();
                    listSids = result.ToList();
                }
                catch (Exception ex)
                {

                }
                return listSids;
            }
        }

        public static bool IsQuestionnaireExists(int AssessmentID)
        {
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    var res = (from objquest in entity.T016_Questionnaire
                               where objquest.HeaderID == AssessmentID
                               select objquest
                               ).Count();
                    return (res > 0);
                }
                catch (Exception ex)
                {
                }
            }
            return false;
        }


        public static Boolean InsertEval_DAL(T016_Questionnaire BO_Qst)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        entity.T016_Questionnaire.Add(BO_Qst);
                        entity.SaveChanges();
                        transaction.Commit();
                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                    }
                    return false;
                }
            }
        }
        public static Boolean updateGenEvalDal(T016_Questionnaire BO_Qst)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        T016_Questionnaire res = entity.T016_Questionnaire.First(a => a.HeaderID == BO_Qst.HeaderID);
                        if (res != null)
                        {
                            res.is_GenEval_inserted = true;
                            res.GenEval_NoOfSubInv = BO_Qst.GenEval_NoOfSubInv;
                            res.GenEval_NoOfCoOrdinators = BO_Qst.GenEval_NoOfCoOrdinators;
                            if (BO_Qst.GenEval_HavePriorExpWithIndustryTrials == true)
                            {
                                res.GenEval_HavePriorExpWithIndustryTrials = BO_Qst.GenEval_HavePriorExpWithIndustryTrials;
                                res.GenEval_YrsOfExpInIndustryTrials = BO_Qst.GenEval_YrsOfExpInIndustryTrials;
                            }
                            else
                            {
                                res.GenEval_HavePriorExpWithIndustryTrials = BO_Qst.GenEval_HavePriorExpWithIndustryTrials;
                                res.GenEval_YrsOfExpInIndustryTrials = null;
                            }
                            res.GenEval_CanUrSiteHavePriorExpWithPhaseI = BO_Qst.GenEval_CanUrSiteHavePriorExpWithPhaseI;
                            res.GenEval_ISUrSiteInterestedInConductingNSABPPhaseI = BO_Qst.GenEval_ISUrSiteInterestedInConductingNSABPPhaseI;
                            res.GenEval_HaveExpInEDCsystem = BO_Qst.GenEval_HaveExpInEDCsystem;
                            res.GenEval_CanUrSiteConductPharmokeneticsStudies = BO_Qst.GenEval_CanUrSiteConductPharmokeneticsStudies;
                            if (BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens == false)
                            {
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens = BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens;
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens_ReasonNo = BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens_ReasonNo;
                            }
                            else
                            {
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens = BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens;
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens_ReasonNo = "";
                            }
                            res.GenEval_WillUrSitePathologyDeptAllowFreshTumorSample = BO_Qst.GenEval_WillUrSitePathologyDeptAllowFreshTumorSample;
                            if (BO_Qst.GenEval_HaveFDAinspectedUrSite == true)
                            {
                                res.GenEval_HaveFDAinspectedUrSite = BO_Qst.GenEval_HaveFDAinspectedUrSite;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFile = BO_Qst.GenEval_HaveFDAinspectedUrSiteUploadReportFile;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName = BO_Qst.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName;
                                res.GenEval_HaveFDAinspectedUrSiteComments = BO_Qst.GenEval_HaveFDAinspectedUrSiteComments;
                            }
                            else
                            {
                                res.GenEval_HaveFDAinspectedUrSite = BO_Qst.GenEval_HaveFDAinspectedUrSite;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFile = null;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName = null;
                                res.GenEval_HaveFDAinspectedUrSiteComments = null;
                            }
                            if (BO_Qst.GenEval_AreInvReq4HumanSubProdTraining == true)
                            {
                                res.GenEval_AreInvReq4HumanSubProdTraining = BO_Qst.GenEval_AreInvReq4HumanSubProdTraining;
                                res.GenEval_AreInvReq4HumanSubProdTrainingHowOften = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingHowOften;
                                res.GenEval_AreInvReq4HumanSubProdTrainingComments = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingComments;
                            }
                            else
                            {

                                res.GenEval_AreInvReq4HumanSubProdTraining = BO_Qst.GenEval_AreInvReq4HumanSubProdTraining;
                                res.GenEval_AreInvReq4HumanSubProdTrainingHowOften = null;
                                res.GenEval_AreInvReq4HumanSubProdTrainingComments = "";
                            }
                            if (BO_Qst.GenEval_AreInvReq4GCPTtraining == true)
                            {
                                res.GenEval_AreInvReq4GCPTtraining = BO_Qst.GenEval_AreInvReq4GCPTtraining;
                                res.GenEval_AreInvReq4GCPTtrainingHowOfter = BO_Qst.GenEval_AreInvReq4GCPTtrainingHowOfter;
                                res.GenEval_AreInvReq4GCPTtrainingComments = BO_Qst.GenEval_AreInvReq4GCPTtrainingComments;
                            }
                            else
                            {
                                res.GenEval_AreInvReq4GCPTtraining = BO_Qst.GenEval_AreInvReq4GCPTtraining;
                                res.GenEval_AreInvReq4GCPTtrainingHowOfter = null;
                                res.GenEval_AreInvReq4GCPTtrainingComments = "";
                            }
                            if (BO_Qst.GenEval_AreInvReq4GCPTtrainingUploadCref != null)
                            {
                                res.GenEval_AreInvReq4GCPTtrainingUploadCref = BO_Qst.GenEval_AreInvReq4GCPTtrainingUploadCref;
                                res.GenEval_AreInvReq4GCPTtrainingUploadCrefFileName = BO_Qst.GenEval_AreInvReq4GCPTtrainingUploadCrefFileName;
                            }
                            //res.GenEval_AreInvReq4HumanSubProdTraining = BO_Qst.GenEval_AreInvReq4HumanSubProdTraining;
                            //res.GenEval_AreInvReq4HumanSubProdTrainingHowOften = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingHowOften;
                            //res.GenEval_AreInvReq4HumanSubProdTrainingUploadCerf = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingUploadCerf;
                            //res.GenEval_AreInvReq4HumanSubProdTrainingUploadCerfFileName = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingUploadCerfFileName;
                            //res.GenEval_AreInvReq4HumanSubProdTrainingComments = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingComments;
                            entity.SaveChanges();
                            transaction.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
                return false;
            }
        }
        public static Boolean UpdateFacEvalDal(T016_Questionnaire BO_Qst)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        T016_Questionnaire res = entity.T016_Questionnaire.First(a => a.HeaderID == BO_Qst.HeaderID);
                        if (res != null)
                        {
                            res.FacltEval_20Freezer = BO_Qst.FacltEval_20Freezer;
                            res.FacltEval_70cFreezer = BO_Qst.FacltEval_70cFreezer;
                            res.FacltEval_DryIce = BO_Qst.FacltEval_DryIce;
                            res.FacltEval_Centrifuge = BO_Qst.FacltEval_Centrifuge;
                            if (BO_Qst.FacltEval_DoesSiteHavBckEmergencySys.ToLower() == "yes")
                            {
                                res.FacltEval_DoesSiteHavBckEmergencySys = BO_Qst.FacltEval_DoesSiteHavBckEmergencySys;
                                res.FacltEval_DoesSiteHavBckEmergencySys_FreezerDegree = BO_Qst.FacltEval_DoesSiteHavBckEmergencySys_FreezerDegree;
                            }
                            else
                            {
                                res.FacltEval_DoesSiteHavBckEmergencySys = BO_Qst.FacltEval_DoesSiteHavBckEmergencySys;
                                res.FacltEval_DoesSiteHavBckEmergencySys_FreezerDegree = null;
                            }
                            if (BO_Qst.FacltEval_DoesSiteHavAlermSys.ToLower() == "yes")
                            {
                                res.FacltEval_DoesSiteHavAlermSys = BO_Qst.FacltEval_DoesSiteHavAlermSys;
                                res.FacltEval_DoesSiteHavAlermSys_FreezerDegree = BO_Qst.FacltEval_DoesSiteHavAlermSys_FreezerDegree;
                            }
                            else
                            {
                                res.FacltEval_DoesSiteHavAlermSys = BO_Qst.FacltEval_DoesSiteHavAlermSys;
                                res.FacltEval_DoesSiteHavAlermSys_FreezerDegree = null;
                            }
                            if (BO_Qst.FacltEval_HasAccess2EmergencyEquipment == true)
                            {
                                res.FacltEval_HasAccess2EmergencyEquipment = BO_Qst.FacltEval_HasAccess2EmergencyEquipment;
                                res.FacltEval_HasAccess2EmergencyEquipmentResponsePlan = BO_Qst.FacltEval_HasAccess2EmergencyEquipmentResponsePlan;
                            }
                            else
                            {
                                res.FacltEval_HasAccess2EmergencyEquipment = BO_Qst.FacltEval_HasAccess2EmergencyEquipment;
                                res.FacltEval_HasAccess2EmergencyEquipmentResponsePlan = BO_Qst.FacltEval_HasAccess2EmergencyEquipmentResponsePlan;
                            }
                            if (BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased.ToLower() == "electronic"|| BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased.ToLower() == "both")
                            {
                                res.FacltEval_AreUrMedicalRcdPaperorElectronicBased = BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased;
                                res.FacltEval_IfElectronicIsEMRsys21CFRPart11Compliant = BO_Qst.FacltEval_IfElectronicIsEMRsys21CFRPart11Compliant;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMR = BO_Qst.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMR;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMRComment = BO_Qst.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMRComment;
                            }
                            else
                            {
                                res.FacltEval_AreUrMedicalRcdPaperorElectronicBased = BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased;
                                res.FacltEval_IfElectronicIsEMRsys21CFRPart11Compliant = null;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMR = null;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMRComment = "";

                            }
                            if (BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds == true)
                            {
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdTimeFrame = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdTimeFrame;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdComment = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdComment;
                            }
                            else
                            {
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdTimeFrame = null;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdComment = "";
                            }
                            res.FacltEval_IsStudyDocFilingLockedArea = BO_Qst.FacltEval_IsStudyDocFilingLockedArea;
                            res.FacltEval_IsWrittenFileDRplanMaintained = BO_Qst.FacltEval_IsWrittenFileDRplanMaintained;
                            res.FacltEval_RLabFacDeemedAccreditedByLocalAuth = BO_Qst.FacltEval_RLabFacDeemedAccreditedByLocalAuth;
                            res.FacltEval_CurrLabCerf = BO_Qst.FacltEval_CurrLabCerf;
                            res.FacltEval_CurrLabNormRanges = BO_Qst.FacltEval_CurrLabNormRanges;
                            res.FacltEval_IsUrSitePartOfaNtwk = BO_Qst.FacltEval_IsUrSitePartOfaNtwk;
                            res.ModifiedOn = DateTime.Now.Date;
                            res.is_FaciEval_inserted = true;
                            res.Record_Status = "Active";
                            entity.SaveChanges();
                            transaction.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                    return false;
                }
            }
        }

        public static Boolean UpdateIRBEvalDal(T016_Questionnaire BO_Qst)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        T016_Questionnaire res = entity.T016_Questionnaire.First(a => a.HeaderID == BO_Qst.HeaderID);
                        if (res != null)
                        {
                            res.IRBEval_LocalOrCentral = BO_Qst.IRBEval_LocalOrCentral;
                            res.IRBEval_IRBName = BO_Qst.IRBEval_IRBName;
                            res.IRBEval_AddressLine1 = BO_Qst.IRBEval_AddressLine1;
                            res.IRBEval_AddressLine2 = BO_Qst.IRBEval_AddressLine2;
                            res.IRBEval_County = BO_Qst.IRBEval_County;
                            res.IRBEval_City = BO_Qst.IRBEval_City;
                            res.IRBEval_State = BO_Qst.IRBEval_State;
                            res.IRBEval_ZipCode = BO_Qst.IRBEval_ZipCode;
                            res.IRBEval_OHRP_IRBRegNo = BO_Qst.IRBEval_OHRP_IRBRegNo;
                            res.IRBEval_HowFrequentlyIRBmeets = BO_Qst.IRBEval_HowFrequentlyIRBmeets;
                            res.IRBEval_AreWillingtoWaiveOversighttoNSABP = BO_Qst.IRBEval_AreWillingtoWaiveOversighttoNSABP;
                            res.IRBEval_AreWillingtoWaiveOversighttoNSABPComment = BO_Qst.IRBEval_AreWillingtoWaiveOversighttoNSABPComment;
                            res.IRBEval_HowFrequentlyIRBmeetsComments = BO_Qst.IRBEval_HowFrequentlyIRBmeetsComments;
                            res.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReview = BO_Qst.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReview;
                            res.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReviewComments = BO_Qst.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReviewComments;
                            res.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApproval = BO_Qst.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApproval;
                            res.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApprovalComments = BO_Qst.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApprovalComments;
                            if (BO_Qst.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub == true)
                            {
                                res.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub = BO_Qst.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub;
                                res.IRBEval_HwOfterCommitteMeet = BO_Qst.IRBEval_HwOfterCommitteMeet;
                                res.IRBEval_HwOfterCommitteMeetComment = BO_Qst.IRBEval_HwOfterCommitteMeetComment;
                            }
                            else
                            {
                                res.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub = BO_Qst.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub;
                                res.IRBEval_HwOfterCommitteMeet = null;
                                res.IRBEval_HwOfterCommitteMeetComment = "";
                            }
                            res.IRBEval_AnticipatedTime4Approval = BO_Qst.IRBEval_AnticipatedTime4Approval;
                            res.IRBEval_AnticipatedTime4ApprovalComment = BO_Qst.IRBEval_AnticipatedTime4ApprovalComment;
                            res.ModifiedOn = DateTime.Now;
                            res.is_IRBEval_inserted = true;
                            entity.SaveChanges();
                            transaction.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                    return false;
                }
            }


        }
        public static Boolean UpdatePharmaEvalDal(T016_Questionnaire BO_Qst)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        T016_Questionnaire res = entity.T016_Questionnaire.First(a => a.HeaderID == BO_Qst.HeaderID);
                        if (res != null)
                        {
                            res.PhrmEval_SecurityMeasures4InvestigationalProdStorage = BO_Qst.PhrmEval_SecurityMeasures4InvestigationalProdStorage;
                            res.PhrmEval_InvestigationalProdStorageEqup = BO_Qst.PhrmEval_InvestigationalProdStorageEqup;
                            res.PhrmEval_Mechanism4TemperatureMonitoringBKPPOwerSupp = BO_Qst.PhrmEval_Mechanism4TemperatureMonitoringBKPPOwerSupp;
                            res.PhrmEval_DistanceBwnInvProdStorageLoc = BO_Qst.PhrmEval_DistanceBwnInvProdStorageLoc;

                            if (BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites == true)
                            {
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName;
                            }
                            else
                            {
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = null;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName = "";

                            }
                            if (BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction == true)
                            {

                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopy = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopy;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName;
                            }
                            else
                            {
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopy = null;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName = "";

                            }

                            res.is_PhrmEval_inserted = true;
                            res.ModifiedOn = DateTime.Now.Date;
                            entity.SaveChanges();
                            transaction.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {

                        transaction.Rollback();
                    }
                    return false;
                }
            }

        }

        public static Boolean SubmitQuestionnarie(T016_Questionnaire BO_Qst, string userRole)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        T016_Questionnaire res = entity.T016_Questionnaire.First(a => a.HeaderID == BO_Qst.HeaderID);
                        if (res != null)
                        {
                            //geneval

                            res.GenEval_NoOfSubInv = BO_Qst.GenEval_NoOfSubInv;
                            res.GenEval_NoOfCoOrdinators = BO_Qst.GenEval_NoOfCoOrdinators;
                            if (BO_Qst.GenEval_HavePriorExpWithIndustryTrials == true)
                            {
                                res.GenEval_HavePriorExpWithIndustryTrials = BO_Qst.GenEval_HavePriorExpWithIndustryTrials;
                                res.GenEval_YrsOfExpInIndustryTrials = BO_Qst.GenEval_YrsOfExpInIndustryTrials;
                            }
                            else
                            {
                                res.GenEval_HavePriorExpWithIndustryTrials = BO_Qst.GenEval_HavePriorExpWithIndustryTrials;
                                res.GenEval_YrsOfExpInIndustryTrials = null;
                            }
                            res.GenEval_CanUrSiteHavePriorExpWithPhaseI = BO_Qst.GenEval_CanUrSiteHavePriorExpWithPhaseI;
                            res.GenEval_ISUrSiteInterestedInConductingNSABPPhaseI = BO_Qst.GenEval_ISUrSiteInterestedInConductingNSABPPhaseI;
                            res.GenEval_HaveExpInEDCsystem = BO_Qst.GenEval_HaveExpInEDCsystem;
                            res.GenEval_CanUrSiteConductPharmokeneticsStudies = BO_Qst.GenEval_CanUrSiteConductPharmokeneticsStudies;
                            if (BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens == false)
                            {
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens = BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens;
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens_ReasonNo = BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens_ReasonNo;
                            }
                            else
                            {
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens = BO_Qst.GenEval_CanUrSiteProvidePathologicalSpecimens;
                                res.GenEval_CanUrSiteProvidePathologicalSpecimens_ReasonNo = "";
                            }
                            res.GenEval_WillUrSitePathologyDeptAllowFreshTumorSample = BO_Qst.GenEval_WillUrSitePathologyDeptAllowFreshTumorSample;
                            if (BO_Qst.GenEval_HaveFDAinspectedUrSite == true)
                            {
                                res.GenEval_HaveFDAinspectedUrSite = BO_Qst.GenEval_HaveFDAinspectedUrSite;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFile = BO_Qst.GenEval_HaveFDAinspectedUrSiteUploadReportFile;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName = BO_Qst.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName;
                                res.GenEval_HaveFDAinspectedUrSiteComments = BO_Qst.GenEval_HaveFDAinspectedUrSiteComments;
                            }
                            else
                            {
                                res.GenEval_HaveFDAinspectedUrSite = BO_Qst.GenEval_HaveFDAinspectedUrSite;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFile = null;
                                res.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName = null;
                                res.GenEval_HaveFDAinspectedUrSiteComments = null;
                            }
                            if (BO_Qst.GenEval_AreInvReq4HumanSubProdTraining == true)
                            {
                                res.GenEval_AreInvReq4HumanSubProdTraining = BO_Qst.GenEval_AreInvReq4HumanSubProdTraining;
                                res.GenEval_AreInvReq4HumanSubProdTrainingHowOften = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingHowOften;
                                res.GenEval_AreInvReq4HumanSubProdTrainingComments = BO_Qst.GenEval_AreInvReq4HumanSubProdTrainingComments;
                            }
                            else
                            {

                                res.GenEval_AreInvReq4HumanSubProdTraining = BO_Qst.GenEval_AreInvReq4HumanSubProdTraining;
                                res.GenEval_AreInvReq4HumanSubProdTrainingHowOften = null;
                                res.GenEval_AreInvReq4HumanSubProdTrainingComments = "";
                            }
                            if (BO_Qst.GenEval_AreInvReq4GCPTtraining == true)
                            {
                                res.GenEval_AreInvReq4GCPTtraining = BO_Qst.GenEval_AreInvReq4GCPTtraining;
                                res.GenEval_AreInvReq4GCPTtrainingHowOfter = BO_Qst.GenEval_AreInvReq4GCPTtrainingHowOfter;
                                res.GenEval_AreInvReq4GCPTtrainingComments = BO_Qst.GenEval_AreInvReq4GCPTtrainingComments;
                            }
                            else
                            {
                                res.GenEval_AreInvReq4GCPTtraining = BO_Qst.GenEval_AreInvReq4GCPTtraining;
                                res.GenEval_AreInvReq4GCPTtrainingHowOfter = null;
                                res.GenEval_AreInvReq4GCPTtrainingComments = "";
                            }
                            res.FacltEval_20Freezer = BO_Qst.FacltEval_20Freezer;
                            res.FacltEval_70cFreezer = BO_Qst.FacltEval_70cFreezer;
                            res.FacltEval_DryIce = BO_Qst.FacltEval_DryIce;
                            res.FacltEval_Centrifuge = BO_Qst.FacltEval_Centrifuge;
                            if (BO_Qst.FacltEval_DoesSiteHavBckEmergencySys.ToLower() == "yes")
                            {
                                res.FacltEval_DoesSiteHavBckEmergencySys = BO_Qst.FacltEval_DoesSiteHavBckEmergencySys;
                                res.FacltEval_DoesSiteHavBckEmergencySys_FreezerDegree = BO_Qst.FacltEval_DoesSiteHavBckEmergencySys_FreezerDegree;
                            }
                            else
                            {
                                res.FacltEval_DoesSiteHavBckEmergencySys = BO_Qst.FacltEval_DoesSiteHavBckEmergencySys;
                                res.FacltEval_DoesSiteHavBckEmergencySys_FreezerDegree = null;
                            }
                            if (BO_Qst.FacltEval_DoesSiteHavAlermSys.ToLower() == "yes")
                            {
                                res.FacltEval_DoesSiteHavAlermSys = BO_Qst.FacltEval_DoesSiteHavAlermSys;
                                res.FacltEval_DoesSiteHavAlermSys_FreezerDegree = BO_Qst.FacltEval_DoesSiteHavAlermSys_FreezerDegree;
                            }
                            else
                            {
                                res.FacltEval_DoesSiteHavAlermSys = BO_Qst.FacltEval_DoesSiteHavAlermSys;
                                res.FacltEval_DoesSiteHavAlermSys_FreezerDegree = null;
                            }
                            if (BO_Qst.FacltEval_HasAccess2EmergencyEquipment == true)
                            {
                                res.FacltEval_HasAccess2EmergencyEquipment = BO_Qst.FacltEval_HasAccess2EmergencyEquipment;
                                res.FacltEval_HasAccess2EmergencyEquipmentResponsePlan = BO_Qst.FacltEval_HasAccess2EmergencyEquipmentResponsePlan;
                            }
                            else
                            {
                                res.FacltEval_HasAccess2EmergencyEquipment = BO_Qst.FacltEval_HasAccess2EmergencyEquipment;
                                res.FacltEval_HasAccess2EmergencyEquipmentResponsePlan = BO_Qst.FacltEval_HasAccess2EmergencyEquipmentResponsePlan;
                            }
                            if (BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased.ToLower() == "electronic" || BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased.ToLower() == "both")
                            {
                                res.FacltEval_AreUrMedicalRcdPaperorElectronicBased = BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased;
                                res.FacltEval_IfElectronicIsEMRsys21CFRPart11Compliant = BO_Qst.FacltEval_IfElectronicIsEMRsys21CFRPart11Compliant;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMR = BO_Qst.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMR;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMRComment = BO_Qst.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMRComment;
                            }
                            else
                            {
                                res.FacltEval_AreUrMedicalRcdPaperorElectronicBased = BO_Qst.FacltEval_AreUrMedicalRcdPaperorElectronicBased;
                                res.FacltEval_IfElectronicIsEMRsys21CFRPart11Compliant = null;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMR = null;
                                res.FacltEval_IfElectronicIsOnsiteMonitorReq2ObtnUsernamePswd2ReviewEMRComment = "";

                            }
                            if (BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds == true)
                            {
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdTimeFrame = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdTimeFrame;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdComment = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdComment;
                            }
                            else
                            {
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds = BO_Qst.FacltEval_HasWrittenRcdRetentionPolc4StudyRcds;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdTimeFrame = null;
                                res.FacltEval_HasWrittenRcdRetentionPolc4StudyRcdComment = "";
                            }
                            res.FacltEval_IsStudyDocFilingLockedArea = BO_Qst.FacltEval_IsStudyDocFilingLockedArea;
                            res.FacltEval_IsWrittenFileDRplanMaintained = BO_Qst.FacltEval_IsWrittenFileDRplanMaintained;
                            res.FacltEval_RLabFacDeemedAccreditedByLocalAuth = BO_Qst.FacltEval_RLabFacDeemedAccreditedByLocalAuth;
                            res.FacltEval_CurrLabCerf = BO_Qst.FacltEval_CurrLabCerf;
                            res.FacltEval_CurrLabNormRanges = BO_Qst.FacltEval_CurrLabNormRanges;
                            res.FacltEval_IsUrSitePartOfaNtwk = BO_Qst.FacltEval_IsUrSitePartOfaNtwk;



                            //IRB
                            res.IRBEval_LocalOrCentral = BO_Qst.IRBEval_LocalOrCentral;
                            res.IRBEval_IRBName = BO_Qst.IRBEval_IRBName;
                            res.IRBEval_AddressLine1 = BO_Qst.IRBEval_AddressLine1;
                            res.IRBEval_AddressLine2 = BO_Qst.IRBEval_AddressLine2;
                            res.IRBEval_County = BO_Qst.IRBEval_County;
                            res.IRBEval_City = BO_Qst.IRBEval_City;
                            res.IRBEval_State = BO_Qst.IRBEval_State;
                            res.IRBEval_ZipCode = BO_Qst.IRBEval_ZipCode;
                            res.IRBEval_OHRP_IRBRegNo = BO_Qst.IRBEval_OHRP_IRBRegNo;
                            res.IRBEval_AreWillingtoWaiveOversighttoNSABP = BO_Qst.IRBEval_AreWillingtoWaiveOversighttoNSABP;
                            res.IRBEval_AreWillingtoWaiveOversighttoNSABPComment = BO_Qst.IRBEval_AreWillingtoWaiveOversighttoNSABPComment;
                            res.IRBEval_HowFrequentlyIRBmeets = BO_Qst.IRBEval_HowFrequentlyIRBmeets;
                            res.IRBEval_HowFrequentlyIRBmeetsComments = BO_Qst.IRBEval_HowFrequentlyIRBmeetsComments;
                            res.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReview = BO_Qst.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReview;
                            res.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReviewComments = BO_Qst.IRBEval_HowFarInAdvProtocolmustBsubmitted4IRBReviewComments;
                            res.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApproval = BO_Qst.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApproval;
                            res.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApprovalComments = BO_Qst.IRBEval_AvgTime4mIRBSubmission2ReceiptofIRBApprovalComments;
                            if (BO_Qst.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub == true)
                            {
                                res.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub = BO_Qst.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub;
                                res.IRBEval_HwOfterCommitteMeet = BO_Qst.IRBEval_HwOfterCommitteMeet;
                                res.IRBEval_HwOfterCommitteMeetComment = BO_Qst.IRBEval_HwOfterCommitteMeetComment;
                            }
                            else
                            {
                                res.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub = BO_Qst.IRBEval_DoesInstUtilizeProtocolRevwComtePriorUrIRBSub;
                                res.IRBEval_HwOfterCommitteMeet = null;
                                res.IRBEval_HwOfterCommitteMeetComment = "";
                            }
                            res.IRBEval_AnticipatedTime4Approval = BO_Qst.IRBEval_AnticipatedTime4Approval;
                            res.IRBEval_AnticipatedTime4ApprovalComment = BO_Qst.IRBEval_AnticipatedTime4ApprovalComment;
                            //pharma
                            res.PhrmEval_SecurityMeasures4InvestigationalProdStorage = BO_Qst.PhrmEval_SecurityMeasures4InvestigationalProdStorage;
                            res.PhrmEval_InvestigationalProdStorageEqup = BO_Qst.PhrmEval_InvestigationalProdStorageEqup;
                            res.PhrmEval_Mechanism4TemperatureMonitoringBKPPOwerSupp = BO_Qst.PhrmEval_Mechanism4TemperatureMonitoringBKPPOwerSupp;
                            res.PhrmEval_DistanceBwnInvProdStorageLoc = BO_Qst.PhrmEval_DistanceBwnInvProdStorageLoc;

                            if (BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites == true)
                            {
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName;
                            }
                            else
                            {
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites = BO_Qst.PhrmEval_IsThrSOPDetailsDrugTransBwnSites;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = null;
                                res.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName = "";

                            }
                            if (BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction == true)
                            {

                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopy = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopy;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName;
                            }
                            else
                            {
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction = BO_Qst.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopy = null;
                                res.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName = "";

                            }
                            res.is_GenEval_inserted = true;
                            res.is_IRBEval_inserted = true;
                            res.is_PhrmEval_inserted = true;
                            res.is_FaciEval_inserted = true;
                            res.Record_Status = "Active";
                            res.ModifiedBy = BO_Qst.ModifiedBy;
                            res.ModifiedOn = DateTime.Now.Date;
                            if (userRole == "NSABP-Monitor" || userRole.ToLower() == "both")
                            {
                                res.QCCompleteStatus = BO_Qst.QCCompleteStatus;
                                res.SQA_Form_Status = "2";
                                res.NSABPHeadQt_HasSiteMetCriteria = null;
                                res.NSABPHeadQt_DtofApproval = null;
                                res.NSABPHeadQt_AdminApprovalSign = null;
                                //  where HeaderID = '" + BO_Qst.HeaderID + "'";
                            }
                            else if (userRole == "OnSite-Monitor")
                            {
                                res.QCCompleteStatus = BO_Qst.QCCompleteStatus;
                                res.SQA_Form_Status = "1";
                                res.NSABPHeadQt_HasSiteMetCriteria = null;
                                res.NSABPHeadQt_DtofApproval = null;
                                res.NSABPHeadQt_AdminApprovalSign = null;
                            }
                            entity.SaveChanges();
                            transaction.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                    return false;
                }

            }
        }
        public static List<T215_TimeFrequency> getTimeFrequency_WEEKSandMONTHSWithOthers_DAL()
        {
            List<T215_TimeFrequency> obj = new List<dal.T215_TimeFrequency>();
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    var res = (from objtime in entity.T215_TimeFrequency
                               where objtime.Record_Status == "Active" && objtime.TimeFrequency != "Less than a Month"
                               select objtime).ToList();
                    //{ objtime.id, objtime.TimeFrequency }).ToList();
                    obj = res;

                }
                catch (Exception ex)
                {
                }
                return obj;
            }

        }

        public static List<sp_GetIRBTimes_Result> getTimeFrequency_WeeksWithOthers_DAL()
        {
            List<sp_GetIRBTimes_Result> lstobj = new List<dal.sp_GetIRBTimes_Result>();
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    lstobj = entity.sp_GetIRBTimes().ToList();
                }
                catch (Exception ex)
                {
                }
                return lstobj;
            }
        }

        #region headerdetails
        public static Boolean is_QuestionaryExists(int HeaderID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<T016_Questionnaire> listQue = new List<T016_Questionnaire>();
                try
                {
                    var res = (from objQuest in entity.T016_Questionnaire
                               where objQuest.Record_Status == "Active" && objQuest.HeaderID == HeaderID
                               select objQuest).Take(1).Count();
                    return (res > 0);
                }
                catch (Exception ex)
                {
                    return false;
                }
                //  return false;
            }

        }
        public static Boolean InsertNSABPHeadQt_DAL(T016_Questionnaire BO_Qst)
        {
            return InsertEval_DAL(BO_Qst);

        }

        public static Boolean UpdateNSABPHeadQt_DAL(T016_Questionnaire BO_Qst, string _SQAStatus, string ApplicationorSiteStatus, int applicationorSiteId)
        {
            Boolean _isResult = false;
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {

                        //,IsApplication,[Type]
                        string sqlquery = "select T011.* from  T011_Site_Assesment T011 join T016_Questionnaire T016 on(T011.ID= T016.HeaderID and T016.Record_Status= 'Active') where T011.Record_Status= 'Active' and T011.ID = '" + BO_Qst.HeaderID + "'";
                        var res = entity.Database.SqlQuery<T011_Site_Assesment>(sqlquery);
                        string type = res.Select(A => A.Type).FirstOrDefault();
                        bool IsApplication = Convert.ToBoolean(res.Select(A => A.IsApplication).FirstOrDefault());
                        sqlquery = " select T001.RC_Application_Status as [RC Status] from T001_Research_Collaborator T001 where T001.record_status = 'Active' and T001.[SID]='" + BO_Qst.SiteID + "' and T001.ID='" + applicationorSiteId + "' and T001.RC_Application_Status= 2";//id -->application or siteid
                        sqlquery += " UNION (select T006.Application_Site_Status as [RC Status] from T006_Research_Site T006 where T006.record_status= 'Active' and T006.[SID]= '" + BO_Qst.SiteID + "' and T006.ID= '" + applicationorSiteId + "' and T006.Application_Site_Status= 2) ";
                        var res1 = entity.Database.SqlQuery<int>(sqlquery).FirstOrDefault();
                        string rcstatus = res.ToString();

                        List<string> lstStrQury = new List<string>();
                        sqlquery = "update T016_Questionnaire set NSABPHeadQt_HasSiteMetCriteria = '" + BO_Qst.NSABPHeadQt_HasSiteMetCriteria + "',";
                        sqlquery += " NSABPHeadQt_DtofApproval = '" + BO_Qst.NSABPHeadQt_DtofApproval + "',NSABPHeadQt_AdminApprovalSign = '" + BO_Qst.NSABPHeadQt_AdminApprovalSign + "',Unsatisfactory_Comments = '" + BO_Qst.Unsatisfactory_Comments + "',ModifiedOn = getdate(),ModifiedBy = '" + BO_Qst.ModifiedBy + "',SQA_Form_Status = '" + _SQAStatus + "' where HeaderID = " + BO_Qst.HeaderID + "";
                        lstStrQury.Add(sqlquery);

                        if (IsApplication && type == "Assessment" && res1 != 0)
                        {
                            if (_SQAStatus == "3")
                            {
                                sqlquery = " update T016 set T016.NSABPHeadQt_HasSiteMetCriteria = '" + BO_Qst.NSABPHeadQt_HasSiteMetCriteria + "',";
                                sqlquery += " T016.NSABPHeadQt_DtofApproval = '" + BO_Qst.NSABPHeadQt_DtofApproval + "',";
                                sqlquery += " T016.NSABPHeadQt_AdminApprovalSign = '" + BO_Qst.NSABPHeadQt_AdminApprovalSign + "', ";
                                sqlquery += " T016.Unsatisfactory_Comments = 'System:Status changed to Signed due to Assessment status change', ";
                                sqlquery += " T016.ModifiedOn = getdate(),T016.ModifiedBy = '" + BO_Qst.ModifiedBy + "',T016.SQA_Form_Status = '" + _SQAStatus + "' ";
                                sqlquery += " from T016_Questionnaire T016 join T011_Site_Assesment T011  on (T016.HeaderID = T011.ID and T011.Record_Status = 'Active') ";
                                sqlquery += " where T016.Record_Status = 'Active' and T011.[SiteID]='" + BO_Qst.SiteID + "' and ";
                                sqlquery += "T011.T001_Research_Collabotator_ID='" + BO_Qst.HeaderID + "' and T011.[Type]= 'Qualification' and T016.SQA_Form_Status= 1 ";
                                lstStrQury.Add(sqlquery);
                            }
                            if (_SQAStatus == "4")
                            {
                                sqlquery = "update T016 set T016.NSABPHeadQt_HasSiteMetCriteria = '" + BO_Qst.NSABPHeadQt_HasSiteMetCriteria + "', ";
                                sqlquery += " T016.NSABPHeadQt_DtofApproval = '" + BO_Qst.NSABPHeadQt_DtofApproval + "', ";
                                sqlquery += " T016.NSABPHeadQt_AdminApprovalSign = '" + BO_Qst.NSABPHeadQt_AdminApprovalSign + "', ";
                                sqlquery += " T016.Unsatisfactory_Comments = 'System:Status changed to Unsatisfactory due to Assessment status change', ";
                                sqlquery += " T016.ModifiedOn = getdate(),T016.ModifiedBy = '" + BO_Qst.ModifiedBy + "',T016.SQA_Form_Status = '" + _SQAStatus + "' ";
                                sqlquery += " from T016_Questionnaire T016 join T011_Site_Assesment T011  on (T016.HeaderID = T011.ID and T011.Record_Status = 'Active') ";
                                sqlquery += "  where T016.Record_Status = 'Active' and T011.[SiteID]='" + BO_Qst.SiteID + "' ";
                                sqlquery += "  and T011.T001_Research_Collabotator_ID=" + applicationorSiteId + " and T011.[Type]= 'Qualification' and T016.SQA_Form_Status= 1 ";
                            }
                        }

                        if (IsApplication && type == "Qualification" && res1 != 0)
                        {
                            if (_SQAStatus == "4")
                            {
                                sqlquery = "  update T016 set T016.NSABPHeadQt_HasSiteMetCriteria = '" + BO_Qst.NSABPHeadQt_HasSiteMetCriteria + "',T016.NSABPHeadQt_DtofApproval = '" + BO_Qst.NSABPHeadQt_DtofApproval + "',T016.NSABPHeadQt_AdminApprovalSign = '" + BO_Qst.NSABPHeadQt_AdminApprovalSign + "',T016.Unsatisfactory_Comments = 'System:Status changed to Unsatisfactory due to Qualification status change',T016.ModifiedOn = getdate(),T016.ModifiedBy = '" + BO_Qst.ModifiedBy + "',T016.SQA_Form_Status = '" + _SQAStatus + "'";
                                sqlquery += " from T016_Questionnaire T016 join T011_Site_Assesment T011  on (T016.HeaderID = T011.ID and T011.Record_Status = 'Active')";
                                sqlquery += " where T016.Record_Status = 'Active' and T011.[SiteID]='" + BO_Qst.SiteID + "' and T011.T001_Research_Collabotator_ID=" + applicationorSiteId + " and ";
                                sqlquery += " T011.[Type]= 'Assessment' and T016.SQA_Form_Status= 1";
                                lstStrQury.Add(sqlquery);
                                //sqlquery = " update T1_Research_Collaborator set  RC_Application_Status = '" + ApplicationorSiteStatus + "',";
                                //                                sqlquery += " ModifiedBy = '" + BO_Qst.ModifiedBy + "', ModifiedOn = getDate() ";
                                //                                sqlquery += " where ID = " + applicationorSiteId + " and [SID] = '" + BO_Qst.SiteID + "' and Record_Status = 'Active' ";
                                //                                lstStrQury.Add(sqlquery);
                            }
                        }

                        if (_SQAStatus == "3" && IsApplication)
                        {
                            sqlquery = " update T001_Research_Collaborator set  RC_Application_Status = '" + ApplicationorSiteStatus + "',";
                            sqlquery += " ModifiedBy = '" + BO_Qst.ModifiedBy + "', ModifiedOn = getDate() ";
                            sqlquery += " where ID = " + applicationorSiteId + " and [SID] = '" + BO_Qst.SiteID + "' and Record_Status = 'Active' ";
                            lstStrQury.Add(sqlquery);
                            sqlquery = "update  T006_Research_Site set Application_Site_Status='" + ApplicationorSiteStatus + "',";
                            sqlquery += " ModifiedBy = '" + BO_Qst.ModifiedBy + "',ModifiedOn = getDate() where ID = '" + applicationorSiteId + "'";
                            sqlquery += " and [SID] = '" + BO_Qst.SiteID + "' and Record_Status = 'Active' ";
                            lstStrQury.Add(sqlquery);

                        }

                        foreach (var s in lstStrQury)
                        {
                            entity.Database.ExecuteSqlCommand(s);
                        }
                        transaction.Commit();
                        _isResult = true;


                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return _isResult;
        }

        public static sp_GetQuestionaryDetailsbyHeaderId_Result GetQuestionnarieDetails(int _HeaderId)
        {
            using (var entity = new POC_V2Entities())
            {
                sp_GetQuestionaryDetailsbyHeaderId_Result objQuestionary = new sp_GetQuestionaryDetailsbyHeaderId_Result();
                try
                {
                    objQuestionary = entity.sp_GetQuestionaryDetailsbyHeaderId(_HeaderId).FirstOrDefault();

                }
                catch (Exception ex)
                {
                }
                return objQuestionary;
            }
        }

            
        #endregion
    }
}
