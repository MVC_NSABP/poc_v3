﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations; //for update
using System.Data.Entity;

namespace dal.RC_DAL
{
    public class Personal_DAL
    {
        public List<SP_GetRCPersonnelDetails_Result> GetRCPersonnelDetails()
        {
            using (var db = new POC_V2Entities())
            {
                List<SP_GetRCPersonnelDetails_Result> lstPersonaldetails = new List<SP_GetRCPersonnelDetails_Result>();
                try
                {
                    var result = db.SP_GetRCPersonnelDetails();
                    lstPersonaldetails = result.ToList();
                }
                catch (Exception ex)
                {
                }
                return lstPersonaldetails;
            }
        }


        public List<USP3_GetPersonRoleAddressesByPersonId_Result> GetPersonnelMappingAddressByPersonID(int PersonnelID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<USP3_GetPersonRoleAddressesByPersonId_Result> lstobj = new List<USP3_GetPersonRoleAddressesByPersonId_Result>();
                try
                {
                    var result = entity.USP3_GetPersonRoleAddressesByPersonId(PersonnelID);

                    lstobj = result.ToList();

                }
                catch (Exception ex)
                {
                }
                return lstobj;
            }
        }


        public List<T201_RC_MedicalSpecialities> GetMedicalSpecialties(int T200_Person_Type_ID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<T201_RC_MedicalSpecialities> objlst = new List<T201_RC_MedicalSpecialities>();
                try
                {
                    var res = from objMedicalSpecialities in entity.T201_RC_MedicalSpecialities
                              where objMedicalSpecialities.T200_PersonType == T200_Person_Type_ID && objMedicalSpecialities.Record_status == "Active"
                              orderby objMedicalSpecialities.ID ascending
                              select objMedicalSpecialities;
                    objlst = res.ToList();

                }
                catch (Exception ex)
                {
                }
                return objlst;
            }
        }
        public List<T200_Person_Types> GetPerson_Types()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T200_Person_Types> objlst = new List<T200_Person_Types>();
                try
                {
                    var res = from objPersonTypes in entity.T200_Person_Types
                              where objPersonTypes.Record_status == "Active"
                              //&& objPersonTypes.ID  Not in (3, 4)
                              orderby objPersonTypes.ID ascending
                              select objPersonTypes;
                    objlst = res.ToList();
                }
                catch (Exception ex)
                {
                }
                return objlst;
            }
        }

        public List<T010_Personnel_Statuses> GetPersonnelStatus()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T010_Personnel_Statuses> objlst = new List<T010_Personnel_Statuses>();
                try
                {
                    var res = from objPersonalstatus in entity.T010_Personnel_Statuses
                              where objPersonalstatus.Record_Status == "Active"
                              //&& objPersonTypes.ID  Not in (3, 4)
                              orderby objPersonalstatus.StatusID ascending
                              select objPersonalstatus;
                    objlst = res.ToList();

                }
                catch (Exception ex)
                {
                }
                return objlst;
            }
        }
  
        public string SavePersonnelDetails(T212_Personnel obj, List<T009_Map_Personnel_Degree> degrees)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        obj.Personnel_ID = entity.Database.SqlQuery<string>("select dbo.getNextPersonnelId(" + obj.T200_Person_Type_ID + ") as Personnel_ID")
                               .FirstOrDefault();
                        entity.T212_Personnel.Add(obj);
                        entity.SaveChanges();
                        var personalId = obj.ID;

                        foreach (var i in degrees)
                        {
                            i.T212_Personnel_ID = personalId;
                            entity.T009_Map_Personnel_Degree.Add(i);
                            entity.SaveChanges();
                        }

                        transaction.Commit();
                        //return obj.Personnel_ID;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                    return obj.Personnel_ID;
                }
            }
        }
        public bool UpdatePersonnelDetails(T212_Personnel obj, List<T009_Map_Personnel_Degree> degrees)
        {
            using (var entity = new POC_V2Entities())
            {
                using (DbContextTransaction transaction = entity.Database.BeginTransaction())
                {
                    try
                    {
                        var res = entity.T212_Personnel.SingleOrDefault(s => s.ID == obj.ID);
                        if (res != null)
                        {
                            entity.T212_Personnel.AddOrUpdate(obj); //requires using System.Data.Entity.Migrations;
                            entity.SaveChanges();
                            var personalId = obj.ID;
                            entity.T009_Map_Personnel_Degree.RemoveRange(entity.T009_Map_Personnel_Degree.Where(c => c.T212_Personnel_ID == obj.ID));
                            //db.ProRel.RemoveRange(db.ProRel.Where(c => c.ProjectId == Project_id));
                            entity.SaveChanges();
                            foreach (var i in degrees)
                            {
                                i.T212_Personnel_ID = personalId;
                                entity.T009_Map_Personnel_Degree.AddOrUpdate(i);
                                entity.SaveChanges();
                            }
                            transaction.Commit();
                            return true;
                        }

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                    return false;
                }
            }

        }
    }
}
