﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using System.Data.Entity;

namespace dal.PRSM_DAL
{
    public class PRSM_DAL
    {
        public static List<T032_SPAM_Protocols> GetProtocolSetupDetails()
        {
            using (var entity = new POC_V2Entities())
            {
                List<T032_SPAM_Protocols> listProtocolSetupDetails = new List<T032_SPAM_Protocols>();
                try
                {
                    listProtocolSetupDetails = (from T032 in entity.T032_SPAM_Protocols where T032.Record_status == "Active" select T032).ToList();

                }
                catch (Exception ex)
                {

                }
                return listProtocolSetupDetails;
            }
        }
        public static int Save_Protocol_Setup_info(T032_SPAM_Protocols T032)
        {
            int ID = 0;
            using (var entity = new POC_V2Entities())
            {

                try
                {
                    entity.T032_SPAM_Protocols.Add(T032);
                    entity.SaveChanges();
                    ID = T032.ID;

                }
                catch (Exception ex)
                {

                }
                return ID;
            }
        }
        public static List<PRSM_getDealBreaker_Questions_Result> getDealBreaker_Questions(int ProtocolID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<PRSM_getDealBreaker_Questions_Result> listPRSM_getDealBreaker_Questions_Results = new List<PRSM_getDealBreaker_Questions_Result>();
                try
                {
                    listPRSM_getDealBreaker_Questions_Results = entity.PRSM_getDealBreaker_Questions(ProtocolID).ToList();

                }
                catch (Exception ex)
                {

                }
                return listPRSM_getDealBreaker_Questions_Results;
            }
        }
        public static List<PRSM_Get_RoleDetailsbyProtocol_Result> Get_RoleDetailsbyProtocol(int ProtocolID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<PRSM_Get_RoleDetailsbyProtocol_Result> lstrols = new List<PRSM_Get_RoleDetailsbyProtocol_Result>();
                try
                {
                    lstrols = entity.PRSM_Get_RoleDetailsbyProtocol(ProtocolID).ToList();

                }
                catch (Exception ex)
                {

                }
                return lstrols;
            }
        }
        public static Boolean is_External_Partners_exists(T041_SPAM_Protocol_external_partners T041)
        {
            bool isExist = false;
            using (var entity = new POC_V2Entities())
            {
                var count = 0;

                try
                {
                    if (T041.ID > 0)
                    {
                        count = (from t041 in entity.T041_SPAM_Protocol_external_partners
                                 where t041.ExternalPartnerName == T041.ExternalPartnerName.Trim() && t041.ProtocolID == T041.ProtocolID &&
t041.Record_status == "Active" && t041.ID != T041.ID
                                 select t041).ToList().Count;
                    }
                    else
                    {
                        count = (from t041 in entity.T041_SPAM_Protocol_external_partners
                                 where t041.ExternalPartnerName == T041.ExternalPartnerName.Trim() && t041.ProtocolID == T041.ProtocolID &&
t041.Record_status == "Active"
                                 select t041).ToList().Count;
                    }

                    if (count > 0)
                        isExist = true;
                }
                catch (Exception ex)
                {

                }
                return isExist;
            }
        }
        public static Boolean is_External_Partners_Email_exists(T044_SPAM_ExternalPartner_Emails T044)
        {
            bool isExist = false;
            using (var entity = new POC_V2Entities())
            {
                var count = 0;

                try
                {
                    if (T044.ID > 0)
                    {
                        count = (from t044 in entity.T044_SPAM_ExternalPartner_Emails
                                 where t044.Email == T044.Email && t044.ProtocolExternalPartnerID == T044.ProtocolExternalPartnerID &&
t044.Record_status == "Active" && t044.ID != T044.ID
                                 select t044).ToList().Count;
                    }
                    else
                    {
                        count = (from t044 in entity.T044_SPAM_ExternalPartner_Emails
                                 where t044.Email == T044.Email && t044.ProtocolExternalPartnerID == T044.ProtocolExternalPartnerID &&
t044.Record_status == "Active"
                                 select t044).ToList().Count;
                    }

                    if (count > 0)
                        isExist = true;
                }
                catch (Exception ex)
                {

                }
                return isExist;
            }
        }

        public static Boolean Save_ExternalPartnerName_Email(T041_SPAM_Protocol_external_partners T041, T044_SPAM_ExternalPartner_Emails T044, out int externalpartnerid)
        {
            bool isExist = false;
            externalpartnerid = 0;
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    if (T041.ID > 0)
                    {
                        T041_SPAM_Protocol_external_partners res = (from t041 in entity.T041_SPAM_Protocol_external_partners where t041.ID == T041.ID select t041).SingleOrDefault();
                        res.ExternalPartnerName = T041.ExternalPartnerName;
                        res.ModifiedBy = T041.ModifiedBy;
                        res.ModifiedOn = T041.ModifiedOn;
                        entity.SaveChanges();

                    }
                    else
                    {
                        T041.Record_status = "Active";
                        entity.T041_SPAM_Protocol_external_partners.Add(T041);


                    }
                    entity.SaveChanges();
                    if (!string.IsNullOrEmpty(T044.Email))
                    {
                        T044.ProtocolExternalPartnerID = T041.ID;
                        T044.ModifiedBy = T041.ModifiedBy;
                        T044.CreatedBy = T041.ModifiedBy;
                        T044.CreatedOn = T041.CreatedOn;
                        T044.ModifiedOn = T041.CreatedOn;
                        T044.Record_status = "Active";
                        entity.T044_SPAM_ExternalPartner_Emails.Add(T044);

                    }
                    isExist = true;
                    entity.SaveChanges();
                    externalpartnerid = T041.ID;
                }
                catch (Exception ex)
                {

                }
                return isExist;
            }
        }
        public static Boolean Update_ExternalPartnerName_Email(T041_SPAM_Protocol_external_partners T041, T044_SPAM_ExternalPartner_Emails T044)
        {
            bool isExist = false;
            using (var entity = new POC_V2Entities())
            {

                try
                {
                    T041_SPAM_Protocol_external_partners res = (from t041 in entity.T041_SPAM_Protocol_external_partners where t041.ID == T041.ID select t041).SingleOrDefault();
                    res.ExternalPartnerName = T041.ExternalPartnerName;
                    res.ModifiedBy = T041.ModifiedBy;
                    res.ModifiedOn = T041.ModifiedOn;

                    if (!string.IsNullOrEmpty(T044.Email))
                    {
                        T044_SPAM_ExternalPartner_Emails obj = (from t044 in entity.T044_SPAM_ExternalPartner_Emails where t044.ID == T044.ID select t044).SingleOrDefault();
                        obj.Email = T044.Email;
                        obj.ModifiedBy = T041.ModifiedBy;
                        obj.ModifiedOn = T041.CreatedOn;

                    }
                    entity.SaveChanges();
                    isExist = true;
                }
                catch (Exception)
                {

                    throw;
                }

            }
            return isExist;
        }
        public static List<T041_SPAM_Protocol_external_partners> get_External_Partners(int _ProtocolID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<T041_SPAM_Protocol_external_partners> list = new List<T041_SPAM_Protocol_external_partners>();
                try
                {
                    list = (from T041 in entity.T041_SPAM_Protocol_external_partners where T041.Record_status == "Active" && T041.ProtocolID == _ProtocolID select T041).ToList();

                }
                catch (Exception ex)
                {

                }
                return list;
            }
        }
        public static T045_SPAM_Protocol_Amendment IsAmendment_Exists(string Amendment_Number, int ProtocolID)
        {
            T045_SPAM_Protocol_Amendment ob = new T045_SPAM_Protocol_Amendment();
            using (var entity = new POC_V2Entities())
            {
                try
                {
                    ob = (from t045 in entity.T045_SPAM_Protocol_Amendment where t045.ProtocolID == ProtocolID && t045.Amendment_Number == Amendment_Number && t045.Record_status == "Active" select t045).SingleOrDefault();
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return ob;
        }
        public static Boolean Save_Protocol_Amendment(T045_SPAM_Protocol_Amendment T045)
        {
            bool res = false;
            using (var entity = new POC_V2Entities())
            {

                try
                {
                    T045_SPAM_Protocol_Amendment T045_count = (from t045 in entity.T045_SPAM_Protocol_Amendment where t045.ProtocolID == T045.ProtocolID && t045.Amendment_Number == T045.Amendment_Number && t045.Record_status == "Active" select t045).SingleOrDefault();
                    if (T045_count == null)
                    {
                        entity.T045_SPAM_Protocol_Amendment.Add(T045);
                    }
                    else
                    {
                        T045_count.Amendment_Number = T045.Amendment_Number;
                        T045_count.Amendment_Date = T045.Amendment_Date;
                        T045_count.ModifiedBy = T045.ModifiedBy;
                        T045_count.ModifiedOn = T045.ModifiedOn;
                    }
                    entity.SaveChanges();
                    res = true;
                }
                catch (Exception)
                {

                    throw;
                }

            }
            return res;
        }
        public static List<T044_SPAM_ExternalPartner_Emails> get_External_Partners_Emails_by_Name(int _ProtocolExternalPartnerID)
        {
            using (var entity = new POC_V2Entities())
            {
                List<T044_SPAM_ExternalPartner_Emails> list = new List<T044_SPAM_ExternalPartner_Emails>();
                try
                {
                    list = (from T044 in entity.T044_SPAM_ExternalPartner_Emails where T044.Record_status == "Active" && T044.ProtocolExternalPartnerID == _ProtocolExternalPartnerID select T044).ToList();

                }
                catch (Exception ex)
                {

                }
                return list;
            }
        }
        public static List<Dictionary<T041_SPAM_Protocol_external_partners, string>> get_External_Partners_Name_Emails(int _protocol)
        {
            List<Dictionary<T041_SPAM_Protocol_external_partners, string>> lstDic = new List<Dictionary<T041_SPAM_Protocol_external_partners, string>>();

            using (var entity = new POC_V2Entities())
            {
                T041_SPAM_Protocol_external_partners T041 = new T041_SPAM_Protocol_external_partners();
                string Eamails = "";
                var query = (from t041 in entity.T041_SPAM_Protocol_external_partners
                           join t044 in entity.T044_SPAM_ExternalPartner_Emails
  on t041.ID equals t044.ProtocolExternalPartnerID into ProtocolExternalPartnerID
                           from m in ProtocolExternalPartnerID.DefaultIfEmpty()
                           where t041.ProtocolID == _protocol && t041.Record_status == "Active"
                           select new
                           {
                               t041.ID,
                               t041.ExternalPartnerName,
                               t041.ProtocolID,
                               m.Email
                           }).ToList();
                var res = (from q in query group q by q.Email into g select new { g.Key, Emails = string.Join(", ", g.Select(x => x.Email)) });

                var kkllk= res.ToDictionary(x => x.Key.ToString(), y => y.Emails);
            }
            return lstDic;
        }



    }
}
