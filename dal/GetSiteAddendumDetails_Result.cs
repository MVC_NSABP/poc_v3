//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    
    public partial class GetSiteAddendumDetails_Result
    {
        public int ID { get; set; }
        public string sid { get; set; }
        public string RCSiteID { get; set; }
        public string ResearchSiteName { get; set; }
        public string RCID { get; set; }
        public Nullable<int> Application_Site_Status { get; set; }
        public string Research_Site_Status { get; set; }
        public Nullable<System.DateTime> Application_Site_Status_Date { get; set; }
        public Nullable<bool> IsResubmit { get; set; }
        public Nullable<bool> Resubmited { get; set; }
        public Nullable<bool> assignOldNSABPSiteID { get; set; }
        public string Application_Site_Status_Reason { get; set; }
        public string SectionsToBeUnlock { get; set; }
    }
}
