//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    
    public partial class sp_IsAddressExist_Result
    {
        public int Address_ID { get; set; }
        public string Address_Line1 { get; set; }
        public string Address_Line2 { get; set; }
        public string Country_ID { get; set; }
        public string State_ID { get; set; }
        public string City { get; set; }
        public string ZIP { get; set; }
        public string Attention { get; set; }
        public string Department { get; set; }
        public string InternalOffice { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string Record_status { get; set; }
    }
}
