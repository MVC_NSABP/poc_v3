﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dal.RC_BO
{
  public  class RCAppSave
    {
        public T001_Research_Collaborator _T001_Research_Collaborator { get; set; }
        public List<dal.T002_Map_RC_Pharmacy> lst_T205_RC_PharmacyActivities { get; set; }
        public List<dal.T005_Map_RC_RegularActivities> lst_T206_RC_RegularActivities { get; set; }
        public List<dal.T004_Map_RC_StudyParticipant> lst_T203_RC_StudyPacrticipantActivities { get; set; }
        public List<dal.T003_Map_RC_DataManagement> lst_T204_RC_DataManagementActivities { get; set; }

    }
    public class RCAppAddendumSave
    {
        public T006_Research_Site _T006_Research_Site { get; set; }
        public T019_Research_Site_History _T019_Research_Site_History { get; set; }
        public List<dal.T015_Map_RCSite_Pharmacy> lst_T205_RC_PharmacyActivities { get; set; }
        public List<dal.T012_Map_RCSite_RegularActivities> lst_T206_RC_RegularActivities { get; set; }
        public List<dal.T013_Map_RCSite_StudyParticipant> lst_T203_RC_StudyPacrticipantActivities { get; set; }
        public List<dal.T014_Map_RCSite_DataManagement> lst_T204_RC_DataManagementActivities { get; set; }

    }
    public class UpdateApplicationStatus
    {
        public List<T006_Research_Site> lst_T006_Research_Site { get; set; }
        public T001_Research_Collaborator _T001_Research_Collaborator { get; set; }

    }
}
