﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dal.RC_BO
{
    public class IndustryRoasterBO
    {
        public int ID { get; set; }
        public string SID { get; set; }
        public string RCID { get; set; }
        public Nullable<int> RC_Application_Status { get; set; }
        public string RC_Application_Status_Date { get; set; }
        public string RC_Application_Status_Reason { get; set; }
        public string SectionsToBeUnlock { get; set; }
        public bool assignOldNSABPSiteID { get; set; }
        public int ModifiedBy { get; set; }
        //for grid of change status popup
       public int Application_Site_Status { get; set; }
        public string Application_Site_Status_Date { get; set; }
        public string RCSiteID { get; set; }
        public string Application_Site_Status_Reason { get; set; }
        
    }
}
