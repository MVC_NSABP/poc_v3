//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class T026_Site_Main
    {
        public string NSABP_Site_Satellite_ID { get; set; }
        public string NCI_ID { get; set; }
        public string Site_Satellite_Name { get; set; }
        public int T402_Site_Satellite_Status_ID { get; set; }
        public string ParentSite_ID { get; set; }
        public bool Nucleus { get; set; }
        public bool Nucleus_Tier { get; set; }
        public Nullable<int> Level { get; set; }
        public Nullable<int> CREATED_BY { get; set; }
        public Nullable<int> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<System.DateTime> MODIFIED_ON { get; set; }
        public string Record_status { get; set; }
        public Nullable<int> MaxAssessmentID { get; set; }
    }
}
