//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    
    public partial class USP3_GetPersonRoleAddressesByPersonId_Result
    {
        public string RCID { get; set; }
        public string Role { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Attention { get; set; }
        public string Department { get; set; }
        public string Internal_Office { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZIP { get; set; }
    }
}
