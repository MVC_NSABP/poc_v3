//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    
    public partial class SP_GetRCPersonnelDetails_Result
    {
        public int ID { get; set; }
        public string Personnel_ID { get; set; }
        public string NSABP_Site_ID { get; set; }
        public string Salutation { get; set; }
        public string F_Name { get; set; }
        public string M_Initials { get; set; }
        public string L_Name { get; set; }
        public string Phone { get; set; }
        public string Phone_Ext { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string PersonType { get; set; }
        public string JobTitle { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<System.DateTime> MODIFIED_ON { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Suffix_Name { get; set; }
        public Nullable<int> PersontypeId { get; set; }
        public Nullable<int> SuffixID { get; set; }
        public string DegreeNames { get; set; }
        public string DegreeIDs { get; set; }
        public Nullable<int> MedicalspecialityID { get; set; }
        public string Medicalspecialityname { get; set; }
        public Nullable<int> Personnel_StatusID { get; set; }
        public string StatusName { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
    }
}
