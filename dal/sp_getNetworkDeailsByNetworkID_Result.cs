//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    
    public partial class sp_getNetworkDeailsByNetworkID_Result
    {
        public string Collaborator_Site_Name { get; set; }
        public string Collaborator_Site_ID { get; set; }
        public string TaxIdentificationNo { get; set; }
        public Nullable<bool> TeachingHospital { get; set; }
        public string LeagalName { get; set; }
        public string PayeeName { get; set; }
        public Nullable<int> LeagalAddressID { get; set; }
        public string LA_Address_Line1 { get; set; }
        public string LA_Address_Line2 { get; set; }
        public string LA_Country_ID { get; set; }
        public string LA_State_ID { get; set; }
        public string LA_City { get; set; }
        public string LA_ZIP { get; set; }
        public string LA_Attention { get; set; }
        public string LA_Department { get; set; }
        public string LA_InternalOffice { get; set; }
        public string LA_T3_State_Name { get; set; }
        public string LA_T3_CountryName { get; set; }
        public Nullable<int> Roaster_Status_ID { get; set; }
        public string Status_Name { get; set; }
        public int ID { get; set; }
        public string CheckAddressee { get; set; }
        public Nullable<int> CAAddress { get; set; }
        public string CA_Address { get; set; }
        public string SID { get; set; }
        public string Affiliation { get; set; }
    }
}
