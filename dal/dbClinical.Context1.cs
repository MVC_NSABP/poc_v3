﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class POC_V2Entities : DbContext
    {
        public POC_V2Entities()
            : base("name=POC_V2Entities")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<T001_Research_Collaborator> T001_Research_Collaborator { get; set; }
        public virtual DbSet<T002_Map_RC_Pharmacy> T002_Map_RC_Pharmacy { get; set; }
        public virtual DbSet<T003_Map_RC_DataManagement> T003_Map_RC_DataManagement { get; set; }
        public virtual DbSet<T004_Map_RC_StudyParticipant> T004_Map_RC_StudyParticipant { get; set; }
        public virtual DbSet<T005_Map_RC_RegularActivities> T005_Map_RC_RegularActivities { get; set; }
        public virtual DbSet<T006_Research_Site> T006_Research_Site { get; set; }
        public virtual DbSet<T007_Roaster_T001_Collaborator_Site> T007_Roaster_T001_Collaborator_Site { get; set; }
        public virtual DbSet<T009_Map_Personnel_Degree> T009_Map_Personnel_Degree { get; set; }
        public virtual DbSet<T011_Site_Assesment> T011_Site_Assesment { get; set; }
        public virtual DbSet<T012_Map_RCSite_RegularActivities> T012_Map_RCSite_RegularActivities { get; set; }
        public virtual DbSet<T013_Map_RCSite_StudyParticipant> T013_Map_RCSite_StudyParticipant { get; set; }
        public virtual DbSet<T014_Map_RCSite_DataManagement> T014_Map_RCSite_DataManagement { get; set; }
        public virtual DbSet<T015_Map_RCSite_Pharmacy> T015_Map_RCSite_Pharmacy { get; set; }
        public virtual DbSet<T016_Questionnaire> T016_Questionnaire { get; set; }
        public virtual DbSet<T017_Questionnaire_History> T017_Questionnaire_History { get; set; }
        public virtual DbSet<T018_UserRoleModules> T018_UserRoleModules { get; set; }
        public virtual DbSet<T019_Research_Site_History> T019_Research_Site_History { get; set; }
        public virtual DbSet<T020_Site_Address> T020_Site_Address { get; set; }
        public virtual DbSet<T021_RC_Application_Status_History> T021_RC_Application_Status_History { get; set; }
        public virtual DbSet<T022_Roaster_Roles> T022_Roaster_Roles { get; set; }
        public virtual DbSet<T023_Roaster_Map_Collaborator_Personnel> T023_Roaster_Map_Collaborator_Personnel { get; set; }
        public virtual DbSet<T024_Roaster_Statuses> T024_Roaster_Statuses { get; set; }
        public virtual DbSet<T025_Roster_Site_Status_History> T025_Roster_Site_Status_History { get; set; }
        public virtual DbSet<T027_Roaster_Pesonnel_Role_History> T027_Roaster_Pesonnel_Role_History { get; set; }
        public virtual DbSet<T028_Roaster_Collaborator_Site_Legel_History> T028_Roaster_Collaborator_Site_Legel_History { get; set; }
        public virtual DbSet<T029_Roaster_Collaborator_Site_Payment_History> T029_Roaster_Collaborator_Site_Payment_History { get; set; }
        public virtual DbSet<T030_Roaster_Site_Associations> T030_Roaster_Site_Associations { get; set; }
        public virtual DbSet<T031_RC_Site_Status_History> T031_RC_Site_Status_History { get; set; }
        public virtual DbSet<T032_SPAM_Protocols> T032_SPAM_Protocols { get; set; }
        public virtual DbSet<T034_SPAM_Protocol_Roles> T034_SPAM_Protocol_Roles { get; set; }
        public virtual DbSet<T035_SPAM_MAP_Protocol_Site> T035_SPAM_MAP_Protocol_Site { get; set; }
        public virtual DbSet<T039_SPAM_DrugAddress> T039_SPAM_DrugAddress { get; set; }
        public virtual DbSet<T040_SPAM_Protocol_Site_Person_Roles> T040_SPAM_Protocol_Site_Person_Roles { get; set; }
        public virtual DbSet<T041_SPAM_Protocol_external_partners> T041_SPAM_Protocol_external_partners { get; set; }
        public virtual DbSet<T042_SPAM_Protocol_Dealbreakers> T042_SPAM_Protocol_Dealbreakers { get; set; }
        public virtual DbSet<T044_SPAM_ExternalPartner_Emails> T044_SPAM_ExternalPartner_Emails { get; set; }
        public virtual DbSet<T200_Person_Types> T200_Person_Types { get; set; }
        public virtual DbSet<T201_RC_MedicalSpecialities> T201_RC_MedicalSpecialities { get; set; }
        public virtual DbSet<T202_Suffix_TYPES> T202_Suffix_TYPES { get; set; }
        public virtual DbSet<T203_RC_StudyPacrticipantActivities> T203_RC_StudyPacrticipantActivities { get; set; }
        public virtual DbSet<T204_RC_DataManagementActivities> T204_RC_DataManagementActivities { get; set; }
        public virtual DbSet<T205_RC_PharmacyActivities> T205_RC_PharmacyActivities { get; set; }
        public virtual DbSet<T206_RC_RegularActivities> T206_RC_RegularActivities { get; set; }
        public virtual DbSet<T207_RC_Degree> T207_RC_Degree { get; set; }
        public virtual DbSet<T209_Countries> T209_Countries { get; set; }
        public virtual DbSet<T210_States> T210_States { get; set; }
        public virtual DbSet<T211_User> T211_User { get; set; }
        public virtual DbSet<T212_Personnel> T212_Personnel { get; set; }
        public virtual DbSet<T213_Address> T213_Address { get; set; }
        public virtual DbSet<T214_RC_Application_Statuses> T214_RC_Application_Statuses { get; set; }
        public virtual DbSet<T215_TimeFrequency> T215_TimeFrequency { get; set; }
        public virtual DbSet<T216_Modules> T216_Modules { get; set; }
        public virtual DbSet<T217_Roles> T217_Roles { get; set; }
        public virtual DbSet<T219_SQA_Form_Status> T219_SQA_Form_Status { get; set; }
        public virtual DbSet<T901_Audit_Trial_Master> T901_Audit_Trial_Master { get; set; }
        public virtual DbSet<T008_ExternalPartner> T008_ExternalPartner { get; set; }
        public virtual DbSet<T010_Personnel_Statuses> T010_Personnel_Statuses { get; set; }
        public virtual DbSet<T026_Site_Main> T026_Site_Main { get; set; }
        public virtual DbSet<T033_SPAM_Roles> T033_SPAM_Roles { get; set; }
        public virtual DbSet<T036_SPAM_MAP_Protocol_Site_History> T036_SPAM_MAP_Protocol_Site_History { get; set; }
        public virtual DbSet<T037_SPam_protocol_Site_status_change_history> T037_SPam_protocol_Site_status_change_history { get; set; }
        public virtual DbSet<T038_SPam_protocol_Site_status_change_history_child> T038_SPam_protocol_Site_status_change_history_child { get; set; }
        public virtual DbSet<T043_SPAM_Site_DealBreaker_Answers> T043_SPAM_Site_DealBreaker_Answers { get; set; }
        public virtual DbSet<T208_User_Status> T208_User_Status { get; set; }
        public virtual DbSet<T218_Status_Matser> T218_Status_Matser { get; set; }
        public virtual DbSet<T902_Audit_Trial_Detail> T902_Audit_Trial_Detail { get; set; }
        public virtual DbSet<T045_SPAM_Protocol_Amendment> T045_SPAM_Protocol_Amendment { get; set; }
        public virtual DbSet<T220_SPAM_Status_Master> T220_SPAM_Status_Master { get; set; }
    
        public virtual int sp_alterdiagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_alterdiagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_creatediagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_creatediagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_dropdiagram(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_dropdiagram", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagramdefinition_Result> sp_helpdiagramdefinition(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagramdefinition_Result>("sp_helpdiagramdefinition", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagrams_Result> sp_helpdiagrams(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagrams_Result>("sp_helpdiagrams", diagramnameParameter, owner_idParameter);
        }
    
        public virtual int sp_renamediagram(string diagramname, Nullable<int> owner_id, string new_diagramname)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var new_diagramnameParameter = new_diagramname != null ?
                new ObjectParameter("new_diagramname", new_diagramname) :
                new ObjectParameter("new_diagramname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_renamediagram", diagramnameParameter, owner_idParameter, new_diagramnameParameter);
        }
    
        public virtual int sp_upgraddiagrams()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_upgraddiagrams");
        }
    
        public virtual ObjectResult<SP_GetRCPersonnelDetails_Result> SP_GetRCPersonnelDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetRCPersonnelDetails_Result>("SP_GetRCPersonnelDetails");
        }
    
        public virtual ObjectResult<USP3_GetPersonRoleAddressesByPersonId_Result> USP3_GetPersonRoleAddressesByPersonId(Nullable<int> personid)
        {
            var personidParameter = personid.HasValue ?
                new ObjectParameter("personid", personid) :
                new ObjectParameter("personid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<USP3_GetPersonRoleAddressesByPersonId_Result>("USP3_GetPersonRoleAddressesByPersonId", personidParameter);
        }
    
        public virtual ObjectResult<USP4_GetAddressesAssociationsByAddressId_Result> USP4_GetAddressesAssociationsByAddressId(Nullable<int> addressid)
        {
            var addressidParameter = addressid.HasValue ?
                new ObjectParameter("addressid", addressid) :
                new ObjectParameter("addressid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<USP4_GetAddressesAssociationsByAddressId_Result>("USP4_GetAddressesAssociationsByAddressId", addressidParameter);
        }
    
        public virtual ObjectResult<sp_GetStatesbyCountryID_Result> sp_GetStatesbyCountryID(string countryId)
        {
            var countryIdParameter = countryId != null ?
                new ObjectParameter("countryId", countryId) :
                new ObjectParameter("countryId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetStatesbyCountryID_Result>("sp_GetStatesbyCountryID", countryIdParameter);
        }
    
        public virtual ObjectResult<sp_GetAddressDetails_Result> sp_GetAddressDetails(Nullable<int> addressId)
        {
            var addressIdParameter = addressId.HasValue ?
                new ObjectParameter("AddressId", addressId) :
                new ObjectParameter("AddressId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAddressDetails_Result>("sp_GetAddressDetails", addressIdParameter);
        }
    
        public virtual ObjectResult<sp_IsAddressExist_Result> sp_IsAddressExist(string address_Line1, string address_Line2, string country_ID, string state_ID, string city, string zIP, string attention, string department, string internalOffice)
        {
            var address_Line1Parameter = address_Line1 != null ?
                new ObjectParameter("Address_Line1", address_Line1) :
                new ObjectParameter("Address_Line1", typeof(string));
    
            var address_Line2Parameter = address_Line2 != null ?
                new ObjectParameter("Address_Line2", address_Line2) :
                new ObjectParameter("Address_Line2", typeof(string));
    
            var country_IDParameter = country_ID != null ?
                new ObjectParameter("Country_ID", country_ID) :
                new ObjectParameter("Country_ID", typeof(string));
    
            var state_IDParameter = state_ID != null ?
                new ObjectParameter("State_ID", state_ID) :
                new ObjectParameter("State_ID", typeof(string));
    
            var cityParameter = city != null ?
                new ObjectParameter("City", city) :
                new ObjectParameter("City", typeof(string));
    
            var zIPParameter = zIP != null ?
                new ObjectParameter("ZIP", zIP) :
                new ObjectParameter("ZIP", typeof(string));
    
            var attentionParameter = attention != null ?
                new ObjectParameter("Attention", attention) :
                new ObjectParameter("Attention", typeof(string));
    
            var departmentParameter = department != null ?
                new ObjectParameter("Department", department) :
                new ObjectParameter("Department", typeof(string));
    
            var internalOfficeParameter = internalOffice != null ?
                new ObjectParameter("InternalOffice", internalOffice) :
                new ObjectParameter("InternalOffice", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_IsAddressExist_Result>("sp_IsAddressExist", address_Line1Parameter, address_Line2Parameter, country_IDParameter, state_IDParameter, cityParameter, zIPParameter, attentionParameter, departmentParameter, internalOfficeParameter);
        }
    
        public virtual ObjectResult<Procedure_Result> Procedure(Nullable<int> param1, Nullable<int> param2, Nullable<int> param3)
        {
            var param1Parameter = param1.HasValue ?
                new ObjectParameter("param1", param1) :
                new ObjectParameter("param1", typeof(int));
    
            var param2Parameter = param2.HasValue ?
                new ObjectParameter("param2", param2) :
                new ObjectParameter("param2", typeof(int));
    
            var param3Parameter = param3.HasValue ?
                new ObjectParameter("param3", param3) :
                new ObjectParameter("param3", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Procedure_Result>("Procedure", param1Parameter, param2Parameter, param3Parameter);
        }
    
        public virtual ObjectResult<sp_GetRCDetails_Result> sp_GetRCDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetRCDetails_Result>("sp_GetRCDetails");
        }
    
        public virtual ObjectResult<getSequentialIDs_Application_Result> getSequentialIDs_Application()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getSequentialIDs_Application_Result>("getSequentialIDs_Application");
        }
    
        public virtual ObjectResult<sp_GetUserDetails_Result> sp_GetUserDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetUserDetails_Result>("sp_GetUserDetails");
        }
    
        public virtual ObjectResult<sp_getApplicationOrSiteDetails_Result> sp_getApplicationOrSiteDetails(string siteName)
        {
            var siteNameParameter = siteName != null ?
                new ObjectParameter("SiteName", siteName) :
                new ObjectParameter("SiteName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getApplicationOrSiteDetails_Result>("sp_getApplicationOrSiteDetails", siteNameParameter);
        }
    
        public virtual ObjectResult<string> sp_getMaxHeaderDate_ForSelectedSite(string siteNumber)
        {
            var siteNumberParameter = siteNumber != null ?
                new ObjectParameter("siteNumber", siteNumber) :
                new ObjectParameter("siteNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("sp_getMaxHeaderDate_ForSelectedSite", siteNumberParameter);
        }
    
        public virtual ObjectResult<sp_GetSiteAddendumDetails_RCID_Result> sp_GetSiteAddendumDetails_RCID(string sid)
        {
            var sidParameter = sid != null ?
                new ObjectParameter("sid", sid) :
                new ObjectParameter("sid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetSiteAddendumDetails_RCID_Result>("sp_GetSiteAddendumDetails_RCID", sidParameter);
        }
    
        public virtual ObjectResult<sp_getApplicationHistoryStatus_Result> sp_getApplicationHistoryStatus(string sid)
        {
            var sidParameter = sid != null ?
                new ObjectParameter("sid", sid) :
                new ObjectParameter("sid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getApplicationHistoryStatus_Result>("sp_getApplicationHistoryStatus", sidParameter);
        }
    
        public virtual ObjectResult<sp_GetSiteAddendumDetailsWithAssessment_Result> sp_GetSiteAddendumDetailsWithAssessment(string sid)
        {
            var sidParameter = sid != null ?
                new ObjectParameter("sid", sid) :
                new ObjectParameter("sid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetSiteAddendumDetailsWithAssessment_Result>("sp_GetSiteAddendumDetailsWithAssessment", sidParameter);
        }
    
        public virtual ObjectResult<string> USP5_Update_PrimaryRole(string network, Nullable<int> role, Nullable<int> person, Nullable<int> modifiedBy)
        {
            var networkParameter = network != null ?
                new ObjectParameter("Network", network) :
                new ObjectParameter("Network", typeof(string));
    
            var roleParameter = role.HasValue ?
                new ObjectParameter("Role", role) :
                new ObjectParameter("Role", typeof(int));
    
            var personParameter = person.HasValue ?
                new ObjectParameter("Person", person) :
                new ObjectParameter("Person", typeof(int));
    
            var modifiedByParameter = modifiedBy.HasValue ?
                new ObjectParameter("ModifiedBy", modifiedBy) :
                new ObjectParameter("ModifiedBy", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("USP5_Update_PrimaryRole", networkParameter, roleParameter, personParameter, modifiedByParameter);
        }
    
        public virtual ObjectResult<sp_getPersonnelByNetworkID_Result> sp_getPersonnelByNetworkID(string collaborator_Site_ID)
        {
            var collaborator_Site_IDParameter = collaborator_Site_ID != null ?
                new ObjectParameter("Collaborator_Site_ID", collaborator_Site_ID) :
                new ObjectParameter("Collaborator_Site_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getPersonnelByNetworkID_Result>("sp_getPersonnelByNetworkID", collaborator_Site_IDParameter);
        }
    
        public virtual ObjectResult<sp_getNetworkDeailsByNetworkID_Result> sp_getNetworkDeailsByNetworkID(string networkID)
        {
            var networkIDParameter = networkID != null ?
                new ObjectParameter("NetworkID", networkID) :
                new ObjectParameter("NetworkID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getNetworkDeailsByNetworkID_Result>("sp_getNetworkDeailsByNetworkID", networkIDParameter);
        }
    
        public virtual ObjectResult<sp_getRoleHistoryBySiteID_Result> sp_getRoleHistoryBySiteID(string siteID)
        {
            var siteIDParameter = siteID != null ?
                new ObjectParameter("SiteID", siteID) :
                new ObjectParameter("SiteID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getRoleHistoryBySiteID_Result>("sp_getRoleHistoryBySiteID", siteIDParameter);
        }
    
        public virtual ObjectResult<sp_getCAAddressDetailsByNetworkID_Result> sp_getCAAddressDetailsByNetworkID(string networkID)
        {
            var networkIDParameter = networkID != null ?
                new ObjectParameter("NetworkID", networkID) :
                new ObjectParameter("NetworkID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getCAAddressDetailsByNetworkID_Result>("sp_getCAAddressDetailsByNetworkID", networkIDParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> sp_GetHeaderCount(string siteNumber)
        {
            var siteNumberParameter = siteNumber != null ?
                new ObjectParameter("siteNumber", siteNumber) :
                new ObjectParameter("siteNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("sp_GetHeaderCount", siteNumberParameter);
        }
    
        public virtual ObjectResult<sp_GetIRBTimes_Result> sp_GetIRBTimes()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetIRBTimes_Result>("sp_GetIRBTimes");
        }
    
        public virtual ObjectResult<sp_getAffirmationSiteDetails_Result> sp_getAffirmationSiteDetails(string siteId)
        {
            var siteIdParameter = siteId != null ?
                new ObjectParameter("siteId", siteId) :
                new ObjectParameter("siteId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getAffirmationSiteDetails_Result>("sp_getAffirmationSiteDetails", siteIdParameter);
        }
    
        public virtual ObjectResult<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result> sp_GetAddendumDetailsFortreatingSiteByNetworkID(string networkID)
        {
            var networkIDParameter = networkID != null ?
                new ObjectParameter("NetworkID", networkID) :
                new ObjectParameter("NetworkID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result>("sp_GetAddendumDetailsFortreatingSiteByNetworkID", networkIDParameter);
        }
    
        public virtual ObjectResult<sp_GetAllHeadersList_Result> sp_GetAllHeadersList(string userRole, Nullable<int> userID)
        {
            var userRoleParameter = userRole != null ?
                new ObjectParameter("userRole", userRole) :
                new ObjectParameter("userRole", typeof(string));
    
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("userID", userID) :
                new ObjectParameter("userID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllHeadersList_Result>("sp_GetAllHeadersList", userRoleParameter, userIDParameter);
        }
    
        public virtual ObjectResult<sp_GetPersonnelToNetworkDetails_Result> sp_GetPersonnelToNetworkDetails(Nullable<int> networkID)
        {
            var networkIDParameter = networkID.HasValue ?
                new ObjectParameter("NetworkID", networkID) :
                new ObjectParameter("NetworkID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetPersonnelToNetworkDetails_Result>("sp_GetPersonnelToNetworkDetails", networkIDParameter);
        }
    
        public virtual ObjectResult<sp_getPayments_HistoryBySiteID_Result> sp_getPayments_HistoryBySiteID(Nullable<int> t007_ID)
        {
            var t007_IDParameter = t007_ID.HasValue ?
                new ObjectParameter("T007_ID", t007_ID) :
                new ObjectParameter("T007_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getPayments_HistoryBySiteID_Result>("sp_getPayments_HistoryBySiteID", t007_IDParameter);
        }
    
        public virtual ObjectResult<sp_CAAddressDetailsByNetworkID_Result> sp_CAAddressDetailsByNetworkID(string networkID)
        {
            var networkIDParameter = networkID != null ?
                new ObjectParameter("NetworkID", networkID) :
                new ObjectParameter("NetworkID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_CAAddressDetailsByNetworkID_Result>("sp_CAAddressDetailsByNetworkID", networkIDParameter);
        }
    
        public virtual ObjectResult<sp_getContractsHistoryBySiteID_Result> sp_getContractsHistoryBySiteID(Nullable<int> t007_ID)
        {
            var t007_IDParameter = t007_ID.HasValue ?
                new ObjectParameter("T007_ID", t007_ID) :
                new ObjectParameter("T007_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_getContractsHistoryBySiteID_Result>("sp_getContractsHistoryBySiteID", t007_IDParameter);
        }
    
        public virtual ObjectResult<sp_PersonnelByNetwork_Result> sp_PersonnelByNetwork()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_PersonnelByNetwork_Result>("sp_PersonnelByNetwork");
        }
    
        public virtual ObjectResult<sp_IsHeaderUser_Result> sp_IsHeaderUser(Nullable<int> userID)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_IsHeaderUser_Result>("sp_IsHeaderUser", userIDParameter);
        }
    
        public virtual ObjectResult<sp_GetHeaderDetailsbyHeaderId_Result> sp_GetHeaderDetailsbyHeaderId(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetHeaderDetailsbyHeaderId_Result>("sp_GetHeaderDetailsbyHeaderId", idParameter);
        }
    
        public virtual ObjectResult<sp_GetIndustryPersonnelByNetwork_Result> sp_GetIndustryPersonnelByNetwork(string collaborator_Site_ID)
        {
            var collaborator_Site_IDParameter = collaborator_Site_ID != null ?
                new ObjectParameter("Collaborator_Site_ID", collaborator_Site_ID) :
                new ObjectParameter("Collaborator_Site_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetIndustryPersonnelByNetwork_Result>("sp_GetIndustryPersonnelByNetwork", collaborator_Site_IDParameter);
        }
    
        public virtual ObjectResult<sp_GetQuestionaryDetailsbyHeaderId_Result> sp_GetQuestionaryDetailsbyHeaderId(Nullable<int> headerID)
        {
            var headerIDParameter = headerID.HasValue ?
                new ObjectParameter("HeaderID", headerID) :
                new ObjectParameter("HeaderID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetQuestionaryDetailsbyHeaderId_Result>("sp_GetQuestionaryDetailsbyHeaderId", headerIDParameter);
        }
    
        public virtual ObjectResult<sp_GetAddressDetailsByNetworkId_Result> sp_GetAddressDetailsByNetworkId(string networkID)
        {
            var networkIDParameter = networkID != null ?
                new ObjectParameter("NetworkID", networkID) :
                new ObjectParameter("NetworkID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAddressDetailsByNetworkId_Result>("sp_GetAddressDetailsByNetworkId", networkIDParameter);
        }
    
        public virtual ObjectResult<IND_getSiteDetailsBySiteID_Result> IND_getSiteDetailsBySiteID(string collaborator_Site_ID)
        {
            var collaborator_Site_IDParameter = collaborator_Site_ID != null ?
                new ObjectParameter("Collaborator_Site_ID", collaborator_Site_ID) :
                new ObjectParameter("Collaborator_Site_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<IND_getSiteDetailsBySiteID_Result>("IND_getSiteDetailsBySiteID", collaborator_Site_IDParameter);
        }
    
        public virtual ObjectResult<IND_getSiteAssociations_Result> IND_getSiteAssociations(string siteID)
        {
            var siteIDParameter = siteID != null ?
                new ObjectParameter("SiteID", siteID) :
                new ObjectParameter("SiteID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<IND_getSiteAssociations_Result>("IND_getSiteAssociations", siteIDParameter);
        }
    
        public virtual ObjectResult<IND_PersonnelDetailsByPersonnelID_Result> IND_PersonnelDetailsByPersonnelID(string personnelID)
        {
            var personnelIDParameter = personnelID != null ?
                new ObjectParameter("PersonnelID", personnelID) :
                new ObjectParameter("PersonnelID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<IND_PersonnelDetailsByPersonnelID_Result>("IND_PersonnelDetailsByPersonnelID", personnelIDParameter);
        }
    
        public virtual ObjectResult<IND_GetAddressRolesBYPersonID_Result> IND_GetAddressRolesBYPersonID(string personnel_ID)
        {
            var personnel_IDParameter = personnel_ID != null ?
                new ObjectParameter("Personnel_ID", personnel_ID) :
                new ObjectParameter("Personnel_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<IND_GetAddressRolesBYPersonID_Result>("IND_GetAddressRolesBYPersonID", personnel_IDParameter);
        }
    
        public virtual ObjectResult<RC_RejectedApplications_Result> RC_RejectedApplications()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<RC_RejectedApplications_Result>("RC_RejectedApplications");
        }
    
        public virtual ObjectResult<PRSM_Get_RoleDetailsbyProtocol_Result> PRSM_Get_RoleDetailsbyProtocol(Nullable<int> protocolID)
        {
            var protocolIDParameter = protocolID.HasValue ?
                new ObjectParameter("ProtocolID", protocolID) :
                new ObjectParameter("ProtocolID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PRSM_Get_RoleDetailsbyProtocol_Result>("PRSM_Get_RoleDetailsbyProtocol", protocolIDParameter);
        }
    
        public virtual ObjectResult<PRSM_getDealBreaker_Questions_Result> PRSM_getDealBreaker_Questions(Nullable<int> protocol_ID)
        {
            var protocol_IDParameter = protocol_ID.HasValue ?
                new ObjectParameter("Protocol_ID", protocol_ID) :
                new ObjectParameter("Protocol_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PRSM_getDealBreaker_Questions_Result>("PRSM_getDealBreaker_Questions", protocol_IDParameter);
        }
    
        public virtual ObjectResult<as_getApplicaonOrAddendumStatus_Result> as_getApplicaonOrAddendumStatus(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<as_getApplicaonOrAddendumStatus_Result>("as_getApplicaonOrAddendumStatus", idParameter);
        }
    
        public virtual ObjectResult<getSPAM_IdentifiedSitesDetails_Result> getSPAM_IdentifiedSitesDetails(Nullable<bool> showRConly)
        {
            var showRConlyParameter = showRConly.HasValue ?
                new ObjectParameter("showRConly", showRConly) :
                new ObjectParameter("showRConly", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getSPAM_IdentifiedSitesDetails_Result>("getSPAM_IdentifiedSitesDetails", showRConlyParameter);
        }
    }
}
