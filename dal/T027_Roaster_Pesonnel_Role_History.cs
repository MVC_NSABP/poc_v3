//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class T027_Roaster_Pesonnel_Role_History
    {
        public int History_ID { get; set; }
        public Nullable<int> T007_Roaster_T001_Collaborator_Site_ID { get; set; }
        public Nullable<int> Roaster_T2_Personnel_ID { get; set; }
        public Nullable<int> T022_Roaster_Roles_ID { get; set; }
        public Nullable<System.DateTime> Effective_Date { get; set; }
        public Nullable<System.DateTime> End_Date { get; set; }
        public string Record_Status { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
    
        public virtual T007_Roaster_T001_Collaborator_Site T007_Roaster_T001_Collaborator_Site { get; set; }
    }
}
