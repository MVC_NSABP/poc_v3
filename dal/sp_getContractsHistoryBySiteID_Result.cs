//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dal
{
    using System;
    
    public partial class sp_getContractsHistoryBySiteID_Result
    {
        public int History_ID { get; set; }
        public Nullable<int> T007_Roaster_T001_Collaborator_Site_ID { get; set; }
        public string Legal_name { get; set; }
        public Nullable<System.DateTime> Effective_Date { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Legal_Address { get; set; }
    }
}
