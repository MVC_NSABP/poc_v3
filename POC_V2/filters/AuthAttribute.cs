﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace NSABP_POC.Filters
{
    public class AuthAttribute : ActionFilterAttribute, IActionFilter,  IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(filterContext.HttpContext.Session["UserName"]))
                || filterContext.ActionDescriptor.ActionName == "LoginPage")
            {
                // do nothing
            }
            else
            {
                filterContext.Result = new HttpUnauthorizedResult(); // mark unauthorized
            }
        }
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "Error",
                    ViewBag = { Message = "Your session has expired. Please login again.", Title = "Session Expired" }
                };
            }

        }
        //public  void OnException(ExceptionContext filterContext)
        //{
        //    httpContext.Response.Redirect("~/Error");

        //}

    }
}

