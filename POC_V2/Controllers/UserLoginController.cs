﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.Security;
using dal;
using dal.RC_DAL;

namespace POC_V2.Controllers
{

    public class UserLoginController : Controller
    {
        [HttpGet]
        public ActionResult LoginPage()
        {
            //T211.User_Password
            //  T211.User_Name
            //RCApp_DAL obj = new RCApp_DAL();
            //obj.getCountries();

            //if (UserName == "Admin" && PWD == "nsabp")
            //{

            //    return RedirectToAction("RC_Create", "RC");
            //}           
            //else
            //{
            //    ViewBag.Message = "Invalid Details Failed to Login";
            //}

            return View();
        }
        [HttpPost]
        public ActionResult LoginPage(T211_User T211)
        {
            if (!ModelState.IsValid)
            {
                return View(T211);
            }

            else
            {
                if (T211.User_Name.ToLower().Trim() == "admin" && T211.User_Password == "nsabp")
                {
                   
                    Session["UserName"] = "Admin";
                    Session["UserId"] = 1;
                    Session["RoleName"] = "Admin";
                    Session["RoleId"] = 1;
                    Session["Roles"] = "Admin,scf";
                    return RedirectToAction("ProtocolSetup", "Prsm", new { area = "PRSM" });
                }

                else
                {
                   
                    ViewBag.Message = "Invalid credentials";
                    return View(T211);
                }
            }


        }
        //else if (UserName.ToLower().Trim() == "rcadmin" && PWD == "nsabp")
        //{
        //    Session["username"] = "RC Admin";
        //    Session["role"] = "RC Admin";
        //    return RedirectToAction("RC_Create", "RC");
        //}
        //else if (UserName.ToLower().Trim() == "padmin" && PWD == "nsabp")
        //{
        //    Session["username"] = "padmin";
        //    Session["role"] = "Protocol Manager";
        //    return RedirectToAction("ProtocolSetup", "ProtocolSetup");
        //}

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("LoginPage", "UserLogin");
        }
        [HttpGet]
        public ActionResult Error(string msg)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            ViewBag.Message = msg;
            return View();
        }

    }


}
