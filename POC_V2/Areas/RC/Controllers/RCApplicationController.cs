﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dal.RC_DAL;
using dal.RC_BO;
using dal;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using POC_V2.Areas.RC.Models;
using NSABP_POC.Filters;

namespace POC_V2.Areas.RC.Controllers
{
    [Auth]
    public class RCApplicationController : Controller
    {
        // GET: RC/RCApplication
        #region  views
        [HttpGet]
        public ActionResult RCApp()
        {

            return View();
        }
        public PartialViewResult p_RCApp()
        {
            RCApp_DAL obj = new RCApp_DAL(); HeaderDAL obj1 = new HeaderDAL();
            Personal_DAL obj2 = new Personal_DAL();
            ViewBag.Countries = obj.getCountries();
            ViewBag.States = obj1.GetStates("0");
            ViewBag.GetRegularActivities = obj.GetRegularActivities();
            ViewBag.GetStudyActivities = obj.GetStudyActivities();
            ViewBag.GetDataManagementActivities = obj.GetDataManagementActivities();
            ViewBag.GetPharmacyActivities = obj.GetPharmacyActivities();
            ViewBag.GetMedicalSpecialties = obj.GetMedicalSpecialties();
            ViewBag.Persontype = obj2.GetPerson_Types();
            return PartialView("~/Areas/RC/Views/Shared/p_RCApp.cshtml");
        }
        public ActionResult getAddress()
        {

            return PartialView("~/Areas/RC/Views/Shared/LoadAddress.cshtml");
        }
        public JsonResult lstAddreses()
        {
            RCApp_DAL obj = new RCApp_DAL();

            var lst = obj.lstGetAddresses().ToList();
            return Json(new { data = lst }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult LoadPersonnelDetails()
        {

            return PartialView("/Areas/RC/Views/Shared/P_Load_PersonalDetails.cshtml");
        }
        public JsonResult GetPersonnelDetails()
        {
            Personal_DAL obj = new Personal_DAL();
            var data = obj.GetRCPersonnelDetails();
            return Json(new { data = data.ToList() }, JsonRequestBehavior.AllowGet);
            //return Json(new { data = result , RegularActivities = "'" + RegularActivitiesIDs + "'", StudyActivities = "'" + StudyActivitiesIDs + "'", DataManagementActivities = "'" + DataManagementActivitiesIDs + "'", PharmacyActivities = "'" + PharmacyActivitiesIDs + "'" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OpenaddAddresspopup()
        {
            HeaderDAL obj1 = new HeaderDAL();
            obj1.GetStates("usa");
            RCApp_DAL obj = new RCApp_DAL();
            ViewBag.Countries = obj.getCountries();
            ViewBag.States = obj1.GetStates("usa");
            return PartialView("~/Areas/RC/Views/Shared/P_AddAddress.cshtml");
        }
        public ActionResult RCAppDetails()
        {

            return View();
        }
       
        [HttpGet]
        public ActionResult openAddpersonPopup()
        {
            Personal_DAL objperson = new Personal_DAL();
            RCApp_DAL obj = new RCApp_DAL();
            ViewBag.SuffixTypes = obj.GetSuffix();

            ViewBag.Status = objperson.GetPersonnelStatus();

            ViewBag.Persontype = objperson.GetPerson_Types();
            ViewBag.Degrees = obj.GetDegree();
            ViewBag.MedicalSpeciality = objperson.GetMedicalSpecialties(1);

            return PartialView("~/Areas/RC/Views/Shared/P_AddPersonadetails.cshtml");
        }

        public PartialViewResult p_Addendums()
        {
            RCApp_DAL obj = new RCApp_DAL();
            HeaderDAL obj1 = new HeaderDAL();
            Personal_DAL obj2 = new Personal_DAL();
            ViewBag.Countries = obj.getCountries();
            ViewBag.States = obj1.GetStates("0");
            ViewBag.GetRegularActivities = obj.GetRegularActivities();
            ViewBag.GetStudyActivities = obj.GetStudyActivities();
            ViewBag.GetDataManagementActivities = obj.GetDataManagementActivities();
            ViewBag.GetPharmacyActivities = obj.GetPharmacyActivities();
            ViewBag.GetMedicalSpecialties = obj.GetMedicalSpecialties();
            ViewBag.Persontype = obj2.GetPerson_Types();
            return PartialView("~/Areas/RC/Views/RCApplication/p_Addendums.cshtml");
        }
        #endregion
        public JsonResult SaveSiteAddendumDetails(RCAppAddendumSave obj, bool isresubmit)
        {
            string res = "";
            try
            {
                RCApp_DAL dal = new RCApp_DAL();
                obj._T006_Research_Site.CreatedBy = Convert.ToInt16(Session["UserId"]);
                obj._T006_Research_Site.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                obj._T006_Research_Site.CreatedOn = DateTime.Today.Date;
                obj._T006_Research_Site.ModifiedOn = DateTime.Today.Date;
                if (dal.SaveSiteAddendumDetails(obj, isresubmit))
                {
                    res = "Addendum details created successfully.";
                }
                else
                {
                    res = "Invalid operation";
                }
            }
            catch (Exception ex)
            {
                res = "Invalid operation";
                throw;
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EditRCApp(int id)
        {
            RCApp_DAL obj = new RCApp_DAL();
            var res = obj.ViewRCDetails(id);
            return Json(new { data = res }, JsonRequestBehavior.AllowGet);
            //return Json(new { data = result , RegularActivities = "'" + RegularActivitiesIDs + "'", StudyActivities = "'" + StudyActivitiesIDs + "'", DataManagementActivities = "'" + DataManagementActivitiesIDs + "'", PharmacyActivities = "'" + PharmacyActivitiesIDs + "'" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRcAppDetails()
        {
            RCApp_DAL dal = new RCApp_DAL();
            var res = dal.getRCDetails();
            return Json(new { data = res });
        }
        public JsonResult SaveRcApp(RCAppSave obj)
        {
            string res = "";
            int id;
            try
            {
                RCApp_DAL dal = new RCApp_DAL();
                if(TempData["fileByte"]!=null)
                {
                    obj._T001_Research_Collaborator.Audits_Attachment = (byte[])TempData["fileByte"];
                }
                
                obj._T001_Research_Collaborator.CreatedBy = Convert.ToInt16(Session["UserId"]);
                obj._T001_Research_Collaborator.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                obj._T001_Research_Collaborator.CreatedOn = DateTime.Today.Date;
                obj._T001_Research_Collaborator.ModifiedOn = DateTime.Today.Date;
                id = dal.SaveRcApp(obj);
                if (id > 0)
                {
                    res = "Rc details saved successfully.";
                }
                else
                {
                    res = "Invalid operation";
                }
            }
            catch (Exception ex)
            {
                res = "Invalid operation";
                throw;
            }
            return Json(new { res = res, id = id }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpadteRcApp(RCAppSave obj)
        {
            int id = obj._T001_Research_Collaborator.ID;
            string res = "";
            try
            {
                RCApp_DAL dal = new RCApp_DAL();

                obj._T001_Research_Collaborator.ModifiedBy = Convert.ToInt16(Session["UserId"]);

                obj._T001_Research_Collaborator.ModifiedOn = DateTime.Today.Date;
                if (TempData["fileByte"] != null)
                {
                    obj._T001_Research_Collaborator.Audits_Attachment = (byte[])TempData["fileByte"];
                }
                if (dal.UpdateRCApp(obj))
                {
                    res = "Rc details updated successfully.";
                }
                else
                {
                    res = "Invalid operation";
                }
            }
            catch (Exception ex)
            {
                res = "Invalid operation";
                throw;
            }
            return Json(new { res = res, id = id }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveAddress(T213_Address obj)
        {
            string result = "";
            obj.CreatedBy = Convert.ToInt16(Session["UserId"]);
            obj.ModifiedBy = Convert.ToInt16(Session["UserId"]);
            obj.CreatedOn = DateTime.Today.Date;
            obj.ModifiedOn = DateTime.Today.Date;
            HeaderDAL objdal = new HeaderDAL();
            try
            {
                if (!objdal.isAddressExist(obj))
                {
                    if (objdal.SaveAddresses(obj))
                    {
                        result = "Success";
                    }
                    else
                    {
                        result = "invalid";
                    }
                }
                else
                {
                    result = "Address existed";
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { data = result });
        }
        public JsonResult UpdateAddressdetails(T213_Address obj)
        {
            string result = "";
            obj.ModifiedBy = Convert.ToInt16(Session["UserId"]);
            obj.ModifiedOn = DateTime.Today.Date;
            HeaderDAL objdal = new HeaderDAL();
            try
            {
                if (!objdal.isAddressExist(obj))
                {
                    if (objdal.UpdateAddress(obj))
                    {
                        result = "Success";
                    }
                    else
                    {
                        result = "invalid";
                    }
                }
                else
                {
                    result = "Address existed";
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { data = result });
        }
        public List<T210_States> ListStates(string Country_Code)
        {
            List<T210_States> ListStates = new List<T210_States>();
            RCApp_DAL obj = new RCApp_DAL();
            try
            {

                ListStates = obj.getStates(Country_Code);
            }
            catch (Exception)
            {

                throw;
            }
            return ListStates;
        }
        public JsonResult GetNxtRCID()
        {
            RCApp_DAL obj = new RCApp_DAL();
            string RCID = obj.GetNxtRCID();
            if (!string.IsNullOrEmpty(RCID))
                return Json(new { RCID = RCID, res = "Success" });
            else
                return Json(new { res = "failed" });

        }
        public JsonResult IsRCIDExists(string RCID)
        {
            RCApp_DAL obj = new RCApp_DAL();
            bool isExist = obj.IsRCIDExists(RCID);//t1 reaserch colla
            if (!isExist)
            {
                isExist = obj.roaster_IsRCIDExists(RCID);//rostert1 collasite
            }
            return Json(new { res = isExist });
        }
        public JsonResult IsRCSiteExists(string RCID)
        {
            RCApp_DAL obj = new RCApp_DAL();
            bool isExist = obj.IsRCSiteExists(RCID);//t006 reaserch site
            if (!isExist)
            {
                isExist = obj.IsRCIDExists(RCID); ;//t1 reaserch collaborator
            }
            else if (!isExist)
            {
                isExist = obj.roaster_IsRCIDExists(RCID);//rostert1 collasite
            }
            return Json(isExist);
        }
        public ActionResult GetAddressDetails()
        {
            Session["username"] = "Rama";
            Session["role"] = "Admin";
            RCApp_DAL obj = new RCApp_DAL();
            var address = obj.lstGetAddresses();
            return Json(address);
        }
        public ActionResult GetSiteAddendumDetails(string SID)
        {
            RCApp_DAL obj = new RCApp_DAL();
            return Json(obj.GetSiteAddendumDetails(SID));
        }
        public ActionResult GetApplicationHistoryStatus(string _SID)
        {
            RCApp_DAL obj = new RCApp_DAL();
            return Json(obj.GetApplicationHistoryStatus(_SID));
        }
        public ActionResult GetSiteAddendumDetailsWithAssessment(string id)
        {
            RCApp_DAL obj = new RCApp_DAL();
            return Json(obj.GetSiteAddendumDetailsWithAssessment(id));
        }

        public JsonResult addressChildDetails(int AddressId)
        {
            RCApp_DAL obj = new RCApp_DAL();

            return Json(new { data = obj.GetAddressMappingPersonSiteByAddressID(AddressId) });
        }

        public JsonResult PersonnelChildDetails(int PersonID)
        {
            Personal_DAL obj = new Personal_DAL();
            return Json(new { data = obj.GetPersonnelMappingAddressByPersonID(PersonID).ToList() });
        }
        public JsonResult GetStates(string CountryCode)
        {
            HeaderDAL obj = new HeaderDAL();
            List<SelectListItem> states = new List<SelectListItem>();
            states.Clear();
            //   ProjectsubTypes.Add(new SelectListItem { Text = "--Select Project sub-Type--", Value = "0" });
            if (obj.GetStates(CountryCode) != null)
            {
                foreach (var subtype in obj.GetStates(CountryCode))
                {
                    states.Add(new SelectListItem { Text = subtype.State_Name, Value = subtype.State_Code.ToString() });
                }
            }
            //ViewBag.States = new SelectList(obj.GetStates(CountryCode), "State_Code", "State_Name");
            return Json(states, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ISPersonnelNameExist(string Fname, string Lname)
        {

            var res = RCApp_DAL.ISPersonnelNameExist(Fname, Lname);

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SavePersonnelDetails(Getpersonals getpersons)

        {
            Personal_DAL obj = new Personal_DAL();

            var result = "";
            string personid = "";
            try
            {
                getpersons.Personel.CREATED_BY = Convert.ToInt16(Session["UserId"]);
                getpersons.Personel.CREATED_ON = DateTime.Now.Date;
                getpersons.Personel.MODIFIED_BY = Convert.ToInt16(Session["UserId"]);
                getpersons.Personel.MODIFIED_ON = DateTime.Now.Date;
                T212_Personnel objperson = new T212_Personnel();
                objperson = getpersons.Personel;
                List<T009_Map_Personnel_Degree> lstdegrees = new List<T009_Map_Personnel_Degree>();
                T009_Map_Personnel_Degree objdegree;
                if (getpersons.lstdegrees != null)
                {
                    foreach (var i in getpersons.lstdegrees)
                    {
                        objdegree = new T009_Map_Personnel_Degree();
                        objdegree.T207_Degree_ID = i;
                        objdegree.CreatedBy = Convert.ToInt16(Session["UserId"]);
                        objdegree.CreatedOn = DateTime.Now.Date;
                        objdegree.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                        objdegree.ModifiedOn = DateTime.Now.Date;
                        objdegree.Record_status = "Active";
                        lstdegrees.Add(objdegree);
                    }
                }
                personid = obj.SavePersonnelDetails(objperson, lstdegrees);
                if (personid == "")
                {
                    result = "invalid";
                }
                else
                {
                    result = "Success";
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { data = result, Personid = personid });
        }

        public JsonResult GetMedicalSpeciality(int Persontype)
        {
            Personal_DAL obj = new Personal_DAL();
            List<SelectListItem> lstmedical = new List<SelectListItem>();
            lstmedical.Clear();
            List<T201_RC_MedicalSpecialities> lst = obj.GetMedicalSpecialties(Persontype);
            if (lst.Count() != null)
            {
                foreach (var subtype in lst)
                {
                    lstmedical.Add(new SelectListItem { Text = subtype.Name, Value = subtype.ID.ToString() });
                }
            }
            //ViewBag.States = new SelectList(obj.GetStates(CountryCode), "State_Code", "State_Name");
            return Json(lstmedical, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdatePersonnelDetails(Getpersonals getpersons)
        {
            var result = "";
            try
            {
                Personal_DAL obj = new Personal_DAL();
                getpersons.Personel.MODIFIED_BY = Convert.ToInt16(Session["UserId"]);
                getpersons.Personel.MODIFIED_ON = DateTime.Now.Date;
                T212_Personnel objperson = new T212_Personnel();
                objperson = getpersons.Personel;
                List<T009_Map_Personnel_Degree> lstdegrees = new List<T009_Map_Personnel_Degree>();
                T009_Map_Personnel_Degree objdegree;
                if (getpersons.lstdegrees != null)
                {
                    foreach (var i in getpersons.lstdegrees)
                    {
                        objdegree = new T009_Map_Personnel_Degree();
                        objdegree.T207_Degree_ID = i;
                        objdegree.CreatedBy = Convert.ToInt16(Session["UserId"]);
                        objdegree.CreatedOn = DateTime.Now.Date;
                        objdegree.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                        objdegree.ModifiedOn = DateTime.Now.Date;
                        objdegree.Record_status = "Active";
                        lstdegrees.Add(objdegree);
                    }
                }
                if (obj.UpdatePersonnelDetails(objperson, lstdegrees))
                {
                    result = "Success";
                }
                else
                {
                    result = "Invalid";
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { data = result });
        }
        public JsonResult UpdateApplicationStatus()
        {
            var result = "";
            try
            {
                RCApp_DAL obj = new RCApp_DAL();
                T001_Research_Collaborator t001 = new T001_Research_Collaborator();
                List<T006_Research_Site> t006_table = new List<T006_Research_Site>();
                t001.CreatedBy = Convert.ToInt16(Session["UserId"]);
                t001.CreatedOn = DateTime.Today.Date;
                t001.ModifiedOn = DateTime.Today.Date;
                t001.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                if (obj.UpdateApplicationStatus(t001, t006_table))
                {
                    result = "Success";
                }
                else
                {
                    result = "Invalid";
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { data = result });
        }
        public PartialViewResult RejectedRcApp()
        {

            return PartialView("~/Areas/RC/Views/RCApplication/RejectedRcApp.cshtml");

        }
        public JsonResult getRejectedApplications()
        {
            RCApp_DAL obj = new RCApp_DAL();
            return Json(new { data = obj.getRejectedApplications().ToList() });
        }
        public JsonResult Uploadfile()
        {
            HttpFileCollectionBase files = Request.Files;
           HttpPostedFileBase file = files[0];
            byte[] imgbyte1 = new byte[file.ContentLength];
            HttpPostedFileBase img1 = file;
            img1.InputStream.Read(imgbyte1, 0, file.ContentLength);
            TempData["fileByte"] = imgbyte1;
            TempData.Keep();
            return Json("sucess", JsonRequestBehavior.AllowGet);
        }
        //int length1 = PI_AuditsNetworkInstitutionsFileUpapplicableCAPA.PostedFile.ContentLength;
        //byte[] imgbyte1 = new byte[length1];
        //HttpPostedFile img1 = PI_AuditsNetworkInstitutionsFileUpapplicableCAPA.PostedFile;
        //img1.InputStream.Read(imgbyte1, 0, length1);
        //        Session["image_Audits"] = imgbyte1;
        public PartialViewResult p_ChangeApplicationStatus(int RC_App_Status, string SID)
        {

            RCApp_DAL obj = new RCApp_DAL();
            List<T214_RC_Application_Statuses> lstStatus = obj.getApplicationStatuses();
            if (RC_App_Status == 3)
                //ViewBag.status = obj.getApplicationStatuses();
                ViewBag.status = lstStatus;
            else if (RC_App_Status == 7)
                ViewBag.status = lstStatus.Select(a => a).Where(b => b.Status_ID == 2 || b.Status_ID == 4 || b.Status_ID == 7 || b.Status_ID == 3).ToList();
            ViewBag.SID = SID;
            return PartialView("~/Areas/RC/Views/RCApplication/p_ChangeApplicationStatus.cshtml");
        }

        public JsonResult UpdateSentToRCRC(int _id, string _SID, DateTime _DateOfRCSubmittedToRCBoard, string hidden_ApplicationOrAddendum)
        //UpdateSentToRCRC(int _id, string _SID, DateTime _DateOfRCSubmittedToRCBoard, int _ModifiedBy)
        {
            try
            {
                //DateTime _DateOfRCSubmittedToRCBoard = DateTime.Now;
                int _ModifiedBy = Convert.ToInt16(Session["UserId"]);
                if (hidden_ApplicationOrAddendum.ToLower() == "application")
                {
                    bool _IsUpdate = IndustryRoster_DAL.UpdateSentToRCRC(_id, _SID, _DateOfRCSubmittedToRCBoard, _ModifiedBy);
                    if (_IsUpdate)
                    {
                        return Json(new { result = "Update", msg = "Application status changed successfully" });
                    }
                    else
                    {
                        return Json(new { result = "Fail" });
                    }
                }
                else if (hidden_ApplicationOrAddendum.ToLower() == "addendum")
                {
                    //UpdateSentToRCRC_Addendum
                    bool _IsUpdate = IndustryRoster_DAL.UpdateSentToRCRC_Addendum(_id, _SID, _DateOfRCSubmittedToRCBoard, _ModifiedBy);
                    if (_IsUpdate)
                    {
                        return Json(new { result = "Update", msg = "Addendum status changed successfully" });
                    }
                    else
                    {
                        return Json(new { result = "Fail" });
                    }
                }
                //if (Convert.ToString(ViewState["ApplicationOrAddendum"]) == "Addendum")
                //{
                //    ResearchSiteBO objResearchSite = new ResearchSiteBO();
                //    objResearchSite.ID = Convert.ToInt32(ViewState["SiteID"]);
                //    objResearchSite.SID = Convert.ToString(ViewState["SiteSID"]);
                //    objResearchSite.DateOfRCSubmittedToRCBoard = rdpDateOfRCSubmitted.SelectedDate.Value.Date;
                //    objResearchSite.ModifiedBy = Convert.ToInt32(Session["User ID"]);
                //    if (ResearchSiteBLL.UpdateSentToRCRC(objResearchSite))
                //    {
                //        gvSiteDetails.Rebind();
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert111", "javascript:alert('Addendum status changed successfully');", true);
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "cloasePopup11", "closeSentRCRCPanel()", true);
                //    }

                //}
            }
            catch (Exception ex)
            {
            }
            return Json(new { result = "error" });
        }

        //change 
        public JsonResult UpdateChangeStatus(IndustryRoasterBO objIndustryRoaster, List<IndustryRoasterBO> lstSiteReviewDetails)

        {
            try
            {
                objIndustryRoaster.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                // objIndustryRoaster.RC_Application_Status_Date = DateTime.Now;
                if (IndustryRoster_DAL.UpdateApplicationStatus(objIndustryRoaster, lstSiteReviewDetails))
                {
                    return Json(new { data = "success" });
                }
                else
                {
                    return Json(new { data = "invalid" });

                }
                //return Json(new { data = "invalid" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "error" });

            }

        }
        public JsonResult GetSiteAddendumDetailsWithAssessment1(string sid)
        {
            var obj = IndustryRoster_DAL.GetSiteAddendumDetailsWithAssessment(sid).ToList();
            TempData["ResearchSiteReviewDetais"] = obj;
            TempData.Keep();
            return Json(new { data = obj }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult BindApplicationSiteStatus(string SelectedSiteStatus, string oldSiteStatus)

        {
            try
            {

                List<SelectListItem> lstStatatus = new List<SelectListItem>();

                RCApp_DAL obj = new RCApp_DAL();
                List<T214_RC_Application_Statuses> lstStatus = obj.getApplicationStatuses();
                var res = lstStatus;
                if (SelectedSiteStatus == "4" && oldSiteStatus == "7")
                {
                    res = res.Select(a => a).Where(b => b.Status_ID == 4).ToList();
                }
                else if (SelectedSiteStatus == "2" && oldSiteStatus == "7")
                {
                    res = res.Select(a => a).Where(b => b.Status_ID == 2).ToList();

                }
                else if (SelectedSiteStatus == "3" && oldSiteStatus == "7")
                {
                    res = res.Select(a => a).Where(b => b.Status_ID == 2 || b.Status_ID == 4 || b.Status_ID == 7 || b.Status_ID == 3).ToList();

                }
                else if (oldSiteStatus == "7")
                {
                    res = res.Select(a => a).Where(b => b.Status_ID == 4 || b.Status_ID == 7).ToList();

                }
                if (res != null)
                {
                    foreach (var subtype in res)
                    {
                        //[SID],T1.[InstitutionName]
                        lstStatatus.Add(new SelectListItem { Text = subtype.Status_Name.ToString(), Value = subtype.Status_ID.ToString() });
                    }
                }
                return Json(lstStatatus, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result = "" });
        }
    }

}