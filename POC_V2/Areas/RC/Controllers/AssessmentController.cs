﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dal.RC_DAL;
using dal;
using System.IO;
using NSABP_POC.Filters;

namespace POC_V2.Areas.RC.Controllers
{
    [Auth]
    public class AssessmentController : Controller
    {
        //test12312
        // GET: RC/Assessment
        public ActionResult Assessment()
        {
            return View();
        }
        public PartialViewResult P_AssesmentForm()
        {
            RCApp_DAL obj = new RCApp_DAL();
            ViewBag.Users = HeaderDAL.GetUserDetails();
            ViewBag.Countries = obj.getCountries();
            ViewBag.timeandmonths = QuestionnarieDAL.getTimeFrequency_WEEKSandMONTHSWithOthers_DAL();
            ViewBag.Time = QuestionnarieDAL.getTimeFrequency_WeeksWithOthers_DAL();
            return PartialView("~/Areas/RC/Views/Shared/P_AssesmentForm.cshtml");
        }
        [HttpPost]
        public JsonResult getSedquentialIDs()//Applications
        {
            List<SelectListItem> lstSIds = new List<SelectListItem>();
            try
            {
                List<getSequentialIDs_Application_Result> lst = QuestionnarieDAL.getSequentialIDs();
                if (lst != null)
                {
                    foreach (var subtype in lst)
                    {
                        //[SID],T1.[InstitutionName]
                        lstSIds.Add(new SelectListItem { Text = subtype.SID, Value = subtype.InstitutionName.ToString() });
                    }
                }
                return Json(lstSIds, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

            }
            return Json(lstSIds, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getSiteNumbers()
        {
            List<SelectListItem> lstSIds = new List<SelectListItem>();
            List<String> lstsites = new List<String>();
            try
            {
                lstsites = HeaderDAL.getSiteNumbers();
                if (lstsites != null)
                {
                    foreach (var subtype in lstsites)
                    {
                        //[SID],T1.[InstitutionName]
                        lstSIds.Add(new SelectListItem { Text = subtype.ToString(), Value = subtype.ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Json(lstSIds, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getApplicationOrSiteDetails(string siteID)
        {

            try
            {
                var res = HeaderDAL.getApplicationOrSiteDetails(siteID);
                string site = res.InstitutionName;
                int id = Convert.ToInt32(res.ID);
                string rcStatus = res.RC_Status.ToString();
                return Json(new { sitename = site, Id = id, rcStatus = rcStatus }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
            }
            return Json(new { sitename = "", Id = "", rcStatus = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult getAffirmationorsiteDetails(string siteID)
        {
            try
            {
                var res = HeaderDAL.getAffirmationSiteDetails(siteID);
                string site = res.Select(a => a.Site_Name).FirstOrDefault().ToString();
                int id = Convert.ToInt32(res.Select(a => a.ID).FirstOrDefault());
                return Json(new { sitename = site, Id = id }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
            }
            return Json(new { sitename = "", Id = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveHeaderDetails(T011_Site_Assesment objsite)
        {
            string IsSave = "";
            string AssessmentId = "";
            int InsertedheaderId = 0;
            try
            {
                // objsite.T001_Research_Collabotator_ID = 1;
                T020_Site_Address objadd = new T020_Site_Address();
                objadd.Addr1 = "";
                objadd.Addr2 = "";
                objadd.Attention = "";
                objadd.City = "";
                objadd.Comments = "";
                objadd.Country = "";
                objadd.Dept_Name = "";
                objadd.fax = "";
                objadd.Record_status = "Active";
                objadd.CREATED_BY = Convert.ToInt32(Session["UserId"]);
                objadd.CREATED_ON = DateTime.Now.Date;
                bool _isAssessmentExists = HeaderDAL.IsAssessmentExists(objsite);
                if (!_isAssessmentExists)
                {
                    if (HeaderDAL.AddHeader(objsite, objadd))
                    {
                        IsSave = "Saved";
                        AssessmentId = objsite.HeaderID;
                        InsertedheaderId = objsite.ID;
                    }
                    else
                    {
                        IsSave = "invalid";
                    }
                }
                else
                {
                    return Json(new { result = "Exist" });
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { result = IsSave, AssessmentId = AssessmentId, headerId = InsertedheaderId, SiteID = objsite.SiteID });
        }

        public string EvalPartialSave(T016_Questionnaire objquegen)
        {
            string msg = "";
            try
            {
                if (QuestionnarieDAL.InsertEval_DAL(objquegen))
                {
                    msg = "save";
                }
                else
                {
                    msg = "error";
                }
            }
            catch (Exception ex)
            {

            }
            return msg;

        }

        public JsonResult GenEvalBtnPartialSave(T016_Questionnaire objquegen)
        {
            string msg = "";
            try
            {
                if (QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(objquegen.HeaderID)))
                {
                    objquegen.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    // objquegen.GenEval_AreInvReq4GCPTtrainingUploadCref=(byte[])Session["file"];
                    //objquegen.GenEval_AreInvReq4GCPTtrainingUploadCrefFileName=Session["filename"].ToString();
                    if (Session["file"] != null && Session["filename"] != null)
                    {
                        objquegen.GenEval_HaveFDAinspectedUrSiteUploadReportFile = (byte[])Session["file"];
                        objquegen.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName = Session["filename"].ToString();
                    }
                    if (QuestionnarieDAL.updateGenEvalDal(objquegen))
                    {
                        msg = "update";
                    }
                    else
                    {
                        msg = "error";
                    }
                }
                else
                {
                    objquegen.is_GenEval_inserted = true;
                    objquegen.CreatedOn = DateTime.Now.Date;
                    objquegen.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    msg = EvalPartialSave(objquegen);
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { data = msg });
        }

        public JsonResult FacEvalBtnPartialSave(T016_Questionnaire objquegen)
        {
            string msg = "";
            try
            {
                if (QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(objquegen.HeaderID)))
                {
                    objquegen.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    if (QuestionnarieDAL.UpdateFacEvalDal(objquegen))
                    {
                        msg = "update";
                    }
                    else
                    {
                        msg = "error";
                    }
                }
                else
                {
                    objquegen.CreatedOn = DateTime.Now.Date;
                    objquegen.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    objquegen.is_ContEval_inserted = false;
                    objquegen.is_GenEval_inserted = false;
                    objquegen.is_IRBEval_inserted = false;
                    objquegen.is_PhrmEval_inserted = false;
                    objquegen.is_FaciEval_inserted = true;
                    msg = EvalPartialSave(objquegen);
                }

            }
            catch (Exception ex)
            {

            }
            return Json(new { data = msg });
        }

        public JsonResult IRBEvalBtnPartialSave(T016_Questionnaire objquegen)
        {
            string msg = "";
            try
            {

                if (QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(objquegen.HeaderID)))
                {
                    objquegen.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    if (QuestionnarieDAL.UpdateIRBEvalDal(objquegen))
                    {
                        msg = "update";
                    }
                    else
                    {
                        msg = "error";
                    }
                }
                else
                {
                    objquegen.CreatedOn = DateTime.Now.Date;
                    objquegen.is_ContEval_inserted = false;
                    objquegen.is_GenEval_inserted = false;
                    objquegen.is_IRBEval_inserted = true;
                    objquegen.is_PhrmEval_inserted = false;
                    objquegen.is_FaciEval_inserted = false;
                    msg = EvalPartialSave(objquegen);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { data = msg });
        }

        public JsonResult PharmaEvalBtnPartialSave(T016_Questionnaire objquegen)
        {
            string msg = "";
            try
            {
                if (objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites == true)
                {
                    if (Session["Transportfile"] != null && Session["Transportfilename"] != null)
                    {
                        objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = (byte[])Session["Transportfile"];
                        objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName = Session["Transportfilename"].ToString();
                    }
                }
                if (objquegen.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction == true)
                {
                    if (Session["Destructfile"] != null && Session["Destructfilename"] != null)
                    {
                        objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = (byte[])Session["Destructfile"];
                        objquegen.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName = Session["Destructfilename"].ToString();
                    }
                }

                if (QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(objquegen.HeaderID)))
                {
                    objquegen.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    objquegen.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    if (QuestionnarieDAL.UpdatePharmaEvalDal(objquegen))
                    {
                        msg = "update";
                    }
                    else
                    {
                        msg = "error";
                    }
                }
                else
                {
                    objquegen.CreatedOn = DateTime.Now.Date;
                    objquegen.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    objquegen.is_ContEval_inserted = false;
                    objquegen.is_GenEval_inserted = false;
                    objquegen.is_IRBEval_inserted = false;
                    objquegen.is_PhrmEval_inserted = true;
                    objquegen.is_FaciEval_inserted = false;
                    msg = EvalPartialSave(objquegen);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { data = msg });
        }

        public JsonResult QuestionarySave(T016_Questionnaire objquegen)
        {
            string msg = "";
            try
            {
                if (objquegen.QCCompleteStatus == true)
                {
                    return Json(new { data = "Exist" });
                }
                if (Session["file"] != null && Session["filename"] != null)
                {
                    objquegen.GenEval_HaveFDAinspectedUrSiteUploadReportFile = (byte[])Session["file"];
                    objquegen.GenEval_HaveFDAinspectedUrSiteUploadReportFileFileName = Session["filename"].ToString();
                }

                if (objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites == true)
                {
                    if (Session["Transportfile"] != null && Session["Transportfilename"] != null)
                    {
                        objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = (byte[])Session["Transportfile"];
                        objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopyFileName = Session["Transportfilename"].ToString();
                    }
                }
                if (objquegen.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction == true)
                {
                    if (Session["Destructfile"] != null && Session["Destructfilename"] != null)
                    {
                        objquegen.PhrmEval_IsThrSOPDetailsDrugTransBwnSites_IfYesAtCopy = (byte[])Session["Destructfile"];
                        objquegen.PhrmEval_IfYesIsThrSOPDetailsDrugDestruction_IfYesAtCopyFileName = Session["Destructfilename"].ToString();
                    }
                }
                if (QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(objquegen.HeaderID)))
                {
                    objquegen.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    //ViewState["userRole"]="both" coming based on userid
                    objquegen.QCCompleteStatus = true;
                    if (QuestionnarieDAL.SubmitQuestionnarie(objquegen, "both"))
                    {
                        msg = "update";
                    }
                    else
                    {
                        msg = "error";
                    }
                }
                else
                {
                    objquegen.CreatedOn = DateTime.Now.Date;
                    objquegen.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    objquegen.QCCompleteStatus = true;
                    msg = EvalPartialSave(objquegen);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { data = msg });
        }

        #region HeaderDetails
        //GetAllHeaderDetails
        [HttpGet]
        public ActionResult HeaderDetails()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetHeaderList()
        {

            try
            {
                //Session["UserId"] = "1";
                //Session["RoleName"] = "Admin";
                var res = HeaderDAL.GetAllHeaderDetails("Both", Convert.ToInt32(Session["UserId"]));
                return Json(new { res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
            }
            return Json(new { res = "invalid" }, JsonRequestBehavior.AllowGet);
        }

        private bool ModuleAdmin_inProcees(dal.RC_BO.Questionnarie_Stattus obj, string sqaStatus)
        {
            bool isSave = false;
            try
            {
                if (QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(obj._T016_Questionnaire.HeaderID)))
                {
                    isSave = QuestionnarieDAL.UpdateNSABPHeadQt_DAL(obj._T016_Questionnaire, sqaStatus, obj.ApplicationorSiteStatus, obj.applicationorSiteId);
                }
                else
                {
                    isSave = QuestionnarieDAL.InsertEval_DAL(obj._T016_Questionnaire);

                }

            }
            catch (Exception ex)
            {
            }
            return isSave;
        }
        public JsonResult NSABPHeadQt_PartialSave(dal.RC_BO.Questionnarie_Stattus obj)
        {
            //dal.RC_BO.Questionnarie_Stattus obj
            try
            {
                if (ModuleAdmin_inProcees(obj, "3"))
                {
                    return Json(new { data = "save" });
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { res = "error" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult btnBackToInProcess(dal.RC_BO.Questionnarie_Stattus obj)
        {
            try
            {
                if (ModuleAdmin_inProcees(obj, "1"))
                {
                    return Json(new { data = "save" });
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { res = "error" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult btnUnSatisfactory(dal.RC_BO.Questionnarie_Stattus obj)
        {
            try
            {
                if (ModuleAdmin_inProcees(obj, "4"))
                {
                    return Json(new { data = "save" });
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { res = "error" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult getAssesmentDetilsbyId(int Id)
        {
            try
            {
                var header = HeaderDAL.GetHeaderDetails(Id);
                var Questionarrie = QuestionnarieDAL.GetQuestionnarieDetails(Id);
                return Json(new { header = header, Questionarrie = Questionarrie }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

            }
            return Json(new { header = "", Questionarrie = "" }, JsonRequestBehavior.AllowGet);
        }
        //fileupload
        [HttpPost]
        public JsonResult UploadFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER" || Request.Browser.Browser.ToUpper() == "CHROME")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string fname1 = fname;
                        bool folderExists = System.IO.Directory.Exists(Server.MapPath("~/Uploads/"));
                        if (!folderExists)
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Uploads/"));
                        //System.IO.Directory.CreateDirectory("~/Uploads/");
                        // Get the complete folder path and store the file inside it.  
                        fname = System.IO.Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        file.SaveAs(fname);
                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                        byte[] binaryData = binaryReader.ReadBytes((int)file.InputStream.Length);
                        ///string result = System.Text.Encoding.UTF8.GetString(binaryData);
                        Session["file"] = binaryData;
                        // Session.Keep("file");
                        Session["filename"] = fname1;

                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        public JsonResult UploadphevalTransportfile()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER" || Request.Browser.Browser.ToUpper() == "CHROME")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string fname1 = fname;
                        bool folderExists = System.IO.Directory.Exists(Server.MapPath("~/Uploads/"));
                        if (!folderExists)
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Uploads/"));
                        //System.IO.Directory.CreateDirectory("~/Uploads/");
                        // Get the complete folder path and store the file inside it.  
                        fname = System.IO.Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        file.SaveAs(fname);
                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                        byte[] binaryData = binaryReader.ReadBytes((int)file.InputStream.Length);
                        ///string result = System.Text.Encoding.UTF8.GetString(binaryData);
                        Session["Transportfile"] = binaryData;
                        //Session.Keep("Transportfile");
                        Session["Transportfilename"] = fname1;

                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        public JsonResult UploadphevaDestructfile()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER" || Request.Browser.Browser.ToUpper() == "CHROME")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string fname1 = fname;
                        bool folderExists = System.IO.Directory.Exists(Server.MapPath("~/Uploads/"));
                        if (!folderExists)
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Uploads/"));
                        //System.IO.Directory.CreateDirectory("~/Uploads/");
                        // Get the complete folder path and store the file inside it.  
                        fname = System.IO.Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        file.SaveAs(fname);
                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                        byte[] binaryData = binaryReader.ReadBytes((int)file.InputStream.Length);
                        ///string result = System.Text.Encoding.UTF8.GetString(binaryData);
                        Session["Destructfile"] = binaryData;
                        // Session.Keep("Destructfile");
                        Session["Destructfilename"] = fname1;

                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        public JsonResult Delete(int headerId)
        {
            bool _result = false, _isHeaderUser = false, _isQuestionnaireExists = false;
            // bool _isSignatureExist = false, _isImageExist = false;
            try
            {
                _isHeaderUser = HeaderDAL.IsHeaderUser(Convert.ToInt32(Session["UserId"]));
                _isQuestionnaireExists = QuestionnarieDAL.IsQuestionnaireExists(Convert.ToInt32(headerId));

                if (_isHeaderUser)
                {
                    if (_isQuestionnaireExists == true)
                    {
                        return Json(new { quest = "There is a Questionnaire form associated to the Header. Deleting this Header will also delete the associated Questionnaire as well. Are you sure you would like to Delete?", header = "", Questionnarie = "", _result = "" });

                    }
                    else
                    {
                        return Json(new { header = "Are you sure you want to delete this Header?", quest = "", Questionnarie = "", _result = "" });
                    }
                }
                else
                {
                    if (_isQuestionnaireExists)
                    {
                        return Json(new { Questionnarie = "There is a Questionnaire form associated with this Header. Hence it can not be deleted.", quest = "", header = "", _result = "" });

                    }

                    if (!_isQuestionnaireExists)
                    {
                        //_result = HeaderDAL.DeleteHeader(Convert.ToInt32(headerId));
                        return Json(new { result = _result, quest = "", header = "", Questionnarie = "" });
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Json("");
        }

        public JsonResult DeleteAllHeaderDetails(int headerId)
        {
            try
            {

                if (HeaderDAL.DeleteAllHeaderDetails(headerId))
                    return Json(new { res = "true" });
                else
                {
                    return Json(new { res = "false" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json("");
        }
        [HttpPost]
        public JsonResult getApplicaonOrAddendumStatus(int ID)
        {
            try
            {
                var res = HeaderDAL.getApplicaonOrAddendumStatus(ID);
                return Json(new { Result = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
            }
            return Json(new { Result = "" }, JsonRequestBehavior.AllowGet);
        }
    }




}