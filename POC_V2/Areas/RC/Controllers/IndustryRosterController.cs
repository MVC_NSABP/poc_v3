﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dal.RC_DAL;
using dal.RC_BO;
using dal;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using POC_V2.Areas.RC.Models;
using System.Text;
using NSABP_POC.Filters;

namespace POC_V2.Areas.RC.Controllers
{
    [Auth]
    public class IndustryRosterController : Controller
    {
        // GET: RC/IndustryRoster
        public ActionResult Network()
        {
            var lst_Roaster_T001_Collaborator_Site = IndustryRoster_DAL.lst_Roaster_T001_Collaborator_Site();
            ViewBag.rcbNetwork = lst_Roaster_T001_Collaborator_Site.Where(x => x.isCollaborator == true).OrderBy(c=>c.Parent_ID)
                .ToList()
                                           .Select(i => new SelectListItem()
                                           {
                                               Text = i.Parent_ID,
                                               Value = i.Collaborator_Site_Name

                                           });


            //ViewBag.Roles = IndustryRoster_DAL.T022_Roaster_Roles();


            return View();
        }
        public PartialViewResult P_IndustryNetwork(string NetworkID)
        {
            ViewBag.NetworkID = NetworkID;
            ViewBag.Roles = IndustryRoster_DAL.T022_Roaster_Roles();
            return PartialView("~/Areas/RC/Views/IndustryRoster/P_IndustryNetwork.cshtml");
        }

        public PartialViewResult P_IndustryPersonnels(string NetworkID)
        {
            ViewBag.NetworkID = NetworkID;

            return PartialView("~/Areas/RC/Views/IndustryRoster/P_IndustryPersonnels.cshtml");
        }
        public PartialViewResult P_IndustryNetworkByID(string SiteID)
        {
            ViewBag.SiteID = SiteID;

            return PartialView("~/Areas/RC/Views/IndustryRoster/P_IndustryNetworkByID.cshtml");
        }
        public JsonResult getSiteDetailsBySiteID(string SiteID)
        {
            try
            {
                var res = IndustryRoster_DAL.getSiteDetailsBySiteID(SiteID);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public List<Object> AllNetwork(List<T007_Roaster_T001_Collaborator_Site> lst_Roaster_T001_Collaborator_Site)
        {
            var ntwrk = (from res in lst_Roaster_T001_Collaborator_Site
                         where res.isCollaborator == true
                         orderby res.Parent_ID
                         select new
                         {
                             Network = res.Parent_ID + " " + res.Collaborator_Site_Name,
                             res.Parent_ID,
                             New_Network = res.Parent_ID.Split('-')[0] + " " + res.Collaborator_Site_Name
                         });
            return ntwrk.ToList<Object>();
        }
        public JsonResult LoadTreeView()
        {
            try
            {
                var lst_Roaster_T001_Collaborator_Site = IndustryRoster_DAL.lst_Roaster_T001_Collaborator_Site();
                var AllParent_Networks = AllNetwork(lst_Roaster_T001_Collaborator_Site);
                var AllChild_Networks = ((from T007 in lst_Roaster_T001_Collaborator_Site
                                          where T007.isCollaborator == true
                                          select new
                                          {
                                              T007.Parent_ID,
                                              rownumber = 1,
                                              SiteName = "*" + T007.Collaborator_Site_ID + " " + T007.Collaborator_Site_Name,
                                              T007.Collaborator_Site_ID
                                          }).ToList()

                       .Union(

                       (from T007 in lst_Roaster_T001_Collaborator_Site
                        where T007.isCollaborator == false
                        select new
                        {
                            T007.Parent_ID,
                            rownumber = 2,
                            SiteName = " " + T007.Collaborator_Site_ID + " " + T007.Collaborator_Site_Name,
                            T007.Collaborator_Site_ID

                        }).ToList())).OrderBy(x => x.rownumber).ThenBy(x => x.Collaborator_Site_ID);

                var AllPersonnels = IndustryRoster_DAL.sp_T212_Personnel().Distinct().ToList();


                var All_Roaster_Roles = (from lst_T212 in IndustryRoster_DAL.T022_Roaster_Roles()
                                         select lst_T212).ToList();
                var All_Address = (from add in IndustryRoster_DAL.lst_Address()
                                   from T001 in lst_Roaster_T001_Collaborator_Site
                                   where add.Address_ID == T001.AddressID ||
                                   add.Address_ID == T001.Pharmacy_Address_ID ||
                                    add.Address_ID == T001.LeagalAddressID
                                   select new
                                   {
                                       T001.Parent_ID,
                                       add.Address_ID,
                                       Address = add.Address_ID.ToString("ADD" + "000000")
                                   }
                                   ).Distinct().ToList();


                return Json(new { parents = AllParent_Networks, childs = AllChild_Networks, AllPersonnels = AllPersonnels, All_Roaster_Roles = All_Roaster_Roles, All_Address = All_Address }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult getPersonnelByNetworkID(string NetworkID)
        {
            try
            {
                var lst_getPersonnelByNetworkID_Result = IndustryRoster_DAL.lst_getPersonnelByNetworkID_Result(NetworkID);

                return Json(lst_getPersonnelByNetworkID_Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult getRoleHistoryBySiteID(int SiteID)
        {
            try
            {
                var data = IndustryRoster_DAL.lst_getRoleHistoryBySiteID(SiteID).ToList();

                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult getNetworkDeailsByNetworkID(string NetworkID)
        {
            try
            {
                var lst_getPersonnelByNetworkID_Result = IndustryRoster_DAL.lst_getNetworkDeailsByNetworkID(NetworkID);

                return Json(lst_getPersonnelByNetworkID_Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult GetPersonnelToNetworkDetails(int NetworkID, bool isShowAll)
        {
            try
            {
                List<sp_GetPersonnelToNetworkDetails_Result> ob = new List<sp_GetPersonnelToNetworkDetails_Result>();
                if (!isShowAll)
                    ob = IndustryRoster_DAL.GetPersonnelToNetworkDetails_Result(NetworkID).Where(x => x.StatusID == 1).ToList();
                else
                    ob = IndustryRoster_DAL.GetPersonnelToNetworkDetails_Result(NetworkID);
                return Json(new { data = ob }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult GetIndustryPersonnelByNetwork(string NetworkID)
        {
            try
            {
                List<sp_GetIndustryPersonnelByNetwork_Result> ob = new List<sp_GetIndustryPersonnelByNetwork_Result>();

                ob = IndustryRoster_DAL.GetIndustryPersonnelByNetwork(NetworkID);
                return Json(new { data = ob }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public PartialViewResult GetIndustryAddressByNetwork_AddressID(string NetworkID, int AddressID)
        {
            ViewBag.NetworkID = NetworkID;
            ViewBag.AddressID = AddressID;
            return PartialView("~/Areas/RC/Views/IndustryRoster/P_IndustryAddresses.cshtml");

        }
        public JsonResult lstIndustryAddressByNetwork_AddressID(string NetworkID, int AddressID)
        {

            var lst_NetworkId = IndustryRoster_DAL.GetAddressDetailsByNetworkId(NetworkID);
            if (AddressID > 0)
            {
                lst_NetworkId = lst_NetworkId.Where(x => x.Address_ID == AddressID).ToList();
            }

            return Json(new { data = lst_NetworkId }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAddendumDetailsFortreatingSiteByNetworkID(string NetworkID)
        {
            try
            {
                List<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result> ob = new List<sp_GetAddendumDetailsFortreatingSiteByNetworkID_Result>();

                ob = IndustryRoster_DAL.GetAddendumDetailsFortreatingSiteByNetworkID(NetworkID);

                return Json(ob, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult getContractsHistoryBySiteID(int SiteID)
        {
            try
            {
                List<sp_getContractsHistoryBySiteID_Result> ob = new List<sp_getContractsHistoryBySiteID_Result>();
                ob = IndustryRoster_DAL.getContractsHistoryBySiteID(SiteID);
                return Json(ob, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult getPayments_HistoryBySiteID(int SiteID)
        {
            try
            {
                List<sp_getPayments_HistoryBySiteID_Result> ob = new List<sp_getPayments_HistoryBySiteID_Result>();
                ob = IndustryRoster_DAL.getPayments_HistoryBySiteID(SiteID);
                return Json(ob, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult getCAAddressDetailsByNetworkID(string NetworkID)
        {
            try
            {
                List<sp_CAAddressDetailsByNetworkID_Result> ob = new List<sp_CAAddressDetailsByNetworkID_Result>();
                ob = IndustryRoster_DAL.getCAAddressDetailsByNetworkID(NetworkID);
                return Json(ob, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult USP5_Update_PrimaryRole(string network, int role, int person)
        {
            try
            {
                int modifiedBy = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.USP5_Update_PrimaryRole(network, role, person, modifiedBy);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult Update_Roaster_Collaborator_Legal_Details(T007_Roaster_T001_Collaborator_Site obj)
        {
            try
            {
                obj.ModifiedOn = DateTime.Now.Date;
                obj.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.Update_Roaster_Collaborator_Legal_Details(obj);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult Update_Roaster_Collaborator_Details(T007_Roaster_T001_Collaborator_Site obj)
        {
            try
            {
                obj.ModifiedOn = DateTime.Now.Date;
                obj.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.Update_Roaster_Collaborator_Details(obj);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult SaveAddendumDetailsForTreatingSite(T007_Roaster_T001_Collaborator_Site objRC)
        {
            try
            {
                objRC.ModifiedOn = DateTime.Now.Date;
                objRC.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                objRC.Roaster_Status_Date = DateTime.Now.Date;
                objRC.CreatedBy = Convert.ToInt16(Session["UserId"]);
                objRC.CreatedOn = DateTime.Now.Date;

                bool res = IndustryRoster_DAL.SaveAddendumDetailsForTreatingSite(objRC);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public PartialViewResult p_AddendumsForTreatingSite()
        {
            RCApp_DAL obj = new RCApp_DAL();
            HeaderDAL obj1 = new HeaderDAL();
            Personal_DAL obj2 = new Personal_DAL();
            ViewBag.Countries = obj.getCountries();
            ViewBag.States = obj1.GetStates("0");
            ViewBag.Persontype = obj2.GetPerson_Types();
            return PartialView("~/Areas/RC/Views/IndustryRoster/p_addendum_TreatingSite.cshtml");
        }

        public JsonResult UpdatePersonnelStatus(T023_Roaster_Map_Collaborator_Personnel objRC)
        {
            try
            {
                objRC.ModifiedOn = DateTime.Now.Date;
                objRC.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.UpdatePersonnelStatus(objRC);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult UpdateSiteDetails(T007_Roaster_T001_Collaborator_Site Collaborator)
        {
            try
            {
                Collaborator.ModifiedOn = DateTime.Now.Date;
                Collaborator.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.UpdateSiteDetails(Collaborator);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JsonResult UpdateSitePharmacyDetails(T007_Roaster_T001_Collaborator_Site Collaborator)
        {
            try
            {
                Collaborator.ModifiedOn = DateTime.Now.Date;

                Collaborator.ModifiedBy = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.UpdateSitePharmacyDetails(Collaborator);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JsonResult getSiteAssociations(string SiteID)
        {
            try
            {
                var res = IndustryRoster_DAL.getSiteAssociations(SiteID.Trim()).ToList();
                return Json(new { data = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public PartialViewResult P_IndustryPersonnelById(string Personnel_ID)
        {
            var lst_Roaster_T001_Collaborator_Site = IndustryRoster_DAL.lst_Roaster_T001_Collaborator_Site();
            var AllParent_Networks = AllNetwork(lst_Roaster_T001_Collaborator_Site);
            ViewBag.Personnel_ID = Personnel_ID;
            Personal_DAL objperson = new Personal_DAL();
            RCApp_DAL obj = new RCApp_DAL();
            ViewBag.SuffixTypes = obj.GetSuffix();

            ViewBag.Status = objperson.GetPersonnelStatus();

            ViewBag.Persontype = objperson.GetPerson_Types();
            ViewBag.Degrees = obj.GetDegree();
            ViewBag.MedicalSpeciality = objperson.GetMedicalSpecialties(1);
            ViewBag.AllNetworks = AllParent_Networks;
            return PartialView("~/Areas/RC/Views/IndustryRoster/P_IndustryPersonnelById.cshtml");
        }
        public JsonResult PersonnelDetailsByPersonnelID(string Personnel_ID)
        {
            try
            {
                var res = IndustryRoster_DAL.PersonnelDetailsByPersonnelID(Personnel_ID);
                var degrees = IndustryRoster_DAL.getPersonnelDegreesByT212_ID(res.ID);


                return Json(new { res = res, degrees = degrees }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult Update_Roaster_PersonnelDetails(T212_Personnel obj, List<T009_Map_Personnel_Degree> lst_T009)
        {
            try
            {
                obj.MODIFIED_ON = DateTime.Now.Date;
                obj.MODIFIED_BY = Convert.ToInt16(Session["UserId"]);
                bool res = IndustryRoster_DAL.Update_Roaster_PersonnelDetails(obj, lst_T009);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public JsonResult GetAddressRolesBYPersonID(string Personnel_ID)
        {
            try
            {
                var res = IndustryRoster_DAL.GetAddressRolesBYPersonID(Personnel_ID);
                return Json(new { data = res.ToList() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}