﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_POC.Filters;
using dal.PRSM_DAL;
using dal;
namespace POC_V2.Areas.PRSM.Controllers
{
    [Auth]
    public class PrsmController : Controller
    {
        // GET: PRSM/PRSM
        public ActionResult PersonRoleAssociation()
        {
            return View();
        }
        public ActionResult ProtocolSetup()
        {
            return View();
        }
        public ActionResult SiteSelection()
        {
            return View();
        }
        public ActionResult ProtocolSetupDetails()
        {
            return View();
        }

        public JsonResult Save_Protocol_Setup_info(T032_SPAM_Protocols obj)
        {
            obj.Record_status = "Active";
            obj.ModifiedBy = Convert.ToInt16(Session["UserId"]);
            obj.CreatedBy = Convert.ToInt16(Session["UserId"]);
            obj.ModifiedOn = DateTime.Today.Date;
            obj.CreatedOn = DateTime.Today.Date;
            var ID = PRSM_DAL.Save_Protocol_Setup_info(obj);
            return Json(new { data = ID }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProtocolSetupDetails()
        {
            var listProtocolSetupDetails = PRSM_DAL.GetProtocolSetupDetails();
            return Json(new { data = listProtocolSetupDetails }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult isProtocolCodeNotExists(string ProtocolCode)
        {
            var listCount = PRSM_DAL.GetProtocolSetupDetails().Where(x => x.Protocol_Code == ProtocolCode.Trim() && x.Record_status == "Active").ToList().Count;
            if (listCount > 0)
                return Json(new { isexist = "true" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { isexist = "false" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDealBreaker_Questions(int ProtocolID)
        {
            var data = PRSM_DAL.getDealBreaker_Questions(ProtocolID).ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_RoleDetailsbyProtocol(int ProtocolID)
        {
            var data = PRSM_DAL.Get_RoleDetailsbyProtocol(ProtocolID).ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult is_External_Partners_exists(T041_SPAM_Protocol_external_partners T041)
        {
            var bl = PRSM_DAL.is_External_Partners_exists(T041);
            return Json(new { isexist = bl }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult is_External_Partners_Email_exists(T044_SPAM_ExternalPartner_Emails T044)
        {
            var bl = PRSM_DAL.is_External_Partners_Email_exists(T044);
            return Json(new { isexist = bl }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Save_ExternalPartnerName_Email(T041_SPAM_Protocol_external_partners T041, T044_SPAM_ExternalPartner_Emails T044)
        {
            T041.Record_status = "Active";
            T041.ModifiedBy = Convert.ToInt16(Session["UserId"]);
            T041.CreatedBy = Convert.ToInt16(Session["UserId"]);
            T041.ModifiedOn = DateTime.Today.Date;
            T041.CreatedOn = DateTime.Today.Date;
            int externalpartnerid;
            var bl = PRSM_DAL.Save_ExternalPartnerName_Email(T041, T044, out externalpartnerid);
            return Json(new { isexist = bl, externalpartnerid = externalpartnerid }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Update_ExternalPartnerName_Email(T041_SPAM_Protocol_external_partners T041, T044_SPAM_ExternalPartner_Emails T044)
        {
            T041.ModifiedBy = Convert.ToInt16(Session["UserId"]);            
            T041.ModifiedOn = DateTime.Today.Date;
        
            var bl = PRSM_DAL.Update_ExternalPartnerName_Email(T041, T044);
            return Json(new { isexist = bl }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult get_External_Partners(int _ProtocolID)
        {

            var res = PRSM_DAL.get_External_Partners(_ProtocolID);
            return Json(res, JsonRequestBehavior.AllowGet);

        }
        public JsonResult IsAmendment_Exists(string Amendment_Number,int ProtocolID)
        {

            var res = PRSM_DAL.IsAmendment_Exists(Amendment_Number, ProtocolID);
            return Json(res, JsonRequestBehavior.AllowGet);

        }
        public JsonResult Save_Protocol_Amendment(T045_SPAM_Protocol_Amendment T045)
        {
            T045.Record_status = "Active";
            T045.ModifiedBy = Convert.ToInt16(Session["UserId"]);
            T045.CreatedBy = Convert.ToInt16(Session["UserId"]);
            T045.ModifiedOn = DateTime.Today.Date;
            T045.CreatedOn = DateTime.Today.Date;
            var res = PRSM_DAL.Save_Protocol_Amendment(T045);
            return Json(res, JsonRequestBehavior.AllowGet);

        }
        public JsonResult get_External_Partners_Emails_by_Name(int _ProtocolExternalPartnerID)
        {            
            var res = PRSM_DAL.get_External_Partners_Emails_by_Name(_ProtocolExternalPartnerID);
            return Json(res, JsonRequestBehavior.AllowGet);

        }
        public JsonResult get_External_Partners_Name_Emails(int _protocol)
        {
            //var res = PRSM_DAL.get_External_Partners_Name_Emails(_protocol);
            //var t =  new ({ ExternalPartnerName = "wsd" , Emails = "sdcx" },{ ExternalPartnerName = "wsd" , Emails = "sdcx" });
            return Json(0, JsonRequestBehavior.AllowGet);

        }
    }
}