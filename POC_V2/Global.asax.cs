﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;

namespace POC_V2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            Server.ClearError();
            var msg = Server.UrlEncode(exception.Message.ToString());
            Response.Redirect(String.Format("~/UserLogin/Error?msg=" + msg + ""));
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}
